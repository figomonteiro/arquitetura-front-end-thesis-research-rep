suite('example', function () {

  setup(function (done) {
    // phantomjs seems to keep session data between runs,
    // so clear before running tests
    localStorage.clear();
    hoodie.account.signOut().done(function () {
      done();
    });
  });

  // test('say hello', function (done) {
  //   this.timeout(10000);
  //   var task = hoodie.directMessages.hello('world')
  //     .fail(function(err) {
  //       assert.ok(false, err.message);
  //     })
  //     .done(function(doc) {
  //       assert.equal(doc.msg, 'Hello, world');
  //       done();
  //     });
  // });

  test('direct message', function (done) {
    this.timeout(10000);

    hoodie.account.signUp("test_user", "test_password")
      .fail(function(err) {
        assert.ok(false, err.message);
      })
      .done(function() {
        return hoodie.directMessages.send({
            'to': 'test_user',
            'body': 'Direct message example.'
        });
      })
      .fail(function(err) {
        assert.ok(false, err.message);
      })
      .done(function(doc) {
        assert.ok(true, 'Direct message sent');
        done();
      });
  });

});
