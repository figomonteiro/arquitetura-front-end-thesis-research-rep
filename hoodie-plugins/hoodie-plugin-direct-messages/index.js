/**
 * Hoodie plugin template
 * An example plugin worker, this is where you put your backend code (if any)
 */
module.exports = function (hoodie, done) {

  // hello world example
  // hoodie.task.on('directmessage:add', function (db, task) {
  //   task.msg = 'Hello, ' + task.name;
  //   hoodie.task.success(db, task);
  // });

  hoodie.task.on('directmessage:add', handleNewMessage);

  function handleNewMessage(originDb, message) {
    var recipient = message.to;
    hoodie.account.find('user', recipient, function(error, user) {
      if (error) {
        return hoodie.task.error(originDb, message, error);
      };

			delete message._rev;
      delete message._id;

      hoodie.database(user.database).revokePublicWriteAccess(function(cenas) {console.log(cenas)});

      hoodie.database(user.database).add('directmessage', message, function(error, message) {
          return addMessageCallback(error, message, originDb);
      });
    });
  };

  function addMessageCallback(error, message, originDb) {
    if(error){
        return hoodie.task.error(originDb, message, error);
    }
    return hoodie.task.success(originDb, message);
  };

  done();

};
