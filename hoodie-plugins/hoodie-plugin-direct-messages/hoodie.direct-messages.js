/**
 * Hoodie plugin template
 * An example plugin, this is where you put your frontend code (if any)
 */

/* global Hoodie */

Hoodie.extend(function (hoodie) {
  'use strict';

  // extend the hoodie.js API
  hoodie.directMessages = {
    // hello: hello,
    send: send,
    findAll: findAll
  };

  // function hello(name) {
  //   return hoodie.task.start('directmessage', { name: name });
  // };

  function send(messageData) {
    return hoodie.task.start('directmessage', messageData);
  }
  
  function findAll(){
    return hoodie.store.findAll('directmessage')
  }

});
