hoodie-plugin-static-resources
===
Extends the hoodie front-end API with methods for storing files both in a public and a private database.
