// creates a database with a given name and optionally grant access
module.exports = function (hoodie, doneCallback) {

	hoodie.task.on('createdb:add', function(dbName, createDbTask) {

		hoodie.database.add(createDbTask.name, function(error, db) {
			if (error) {
				hoodie.task.error(dbName, createDbTask);
				return;
			}

			var grantMethod;
			if (createDbTask.read && !createDbTask.write) {
				grantMethod = db.grantPublicReadAccess;
			} else if (createDbTask.write) {
				grantMethod = db.grantPublicWriteAccess;
			}

			// ADDS WRITE PERMISSIONS
			db.addPermission("mypermission", 'function (newDoc, oldDoc, userCtx) {\n' +
				'for (var i = 0; i < userCtx.roles.length; i++) {\n' +
				'var r = userCtx.roles[i];\n' +
				'if (r === ' + 'pissascaralhofodasse' + ' || r === "_admin") {\n' +
				'return;\n' +
				'}\n' +
				'}\n' +
				'throw {unauthorized: "You are not authorized to write ' +
				'to this database"};\n' +
				'}', function(cenas) {console.log(cenas)});



			// ADDS READ PERMISSIONS
			var sec_url = db._resolve('_security');
			console.log(sec_url);
			var sec = {
				admins : {
					names : [],
					roles : []
				},
				members : {
					names : [],
					roles : [
					'hoodie:read:' + "pissascaralhofodasse",
					'hoodie:write:' + "pissascaralhofodasse"
					]
				}
			};

			hoodie.request('GET', sec_url, function(err, body, response) {
				body.members.roles.push('hoodie:read:pissascaralhofodasse');
				body.members.roles.push('hoodie:write:pissascaralhofodasse');

				hoodie.request('PUT', sec_url, {data: body}, function(err, body, response) {});
			});

			if (grantMethod) {
				grantMethod.call(db, function(error) {
					if (error) {
						hoodie.task.error(dbName, createDbTask);
						return;
					}
					hoodie.task.success(dbName, createDbTask);
				});
			} else {
				hoodie.task.success(dbName, createDbTask);
			}
		});
});

doneCallback();
};
