    /**
     * Created by cristianoalves on 31/03/15.
     */


 var SocketIO = require("cloud/socketIO.js")    
    
    Parse.Cloud.beforeSave("OrderStateUpdate", function(request, response) {
    
        Parse.Config.get().then(function (config) {
    
            var ORDER_STATE_GO_TO_RESTAURANT = config.get("ORDER_STATE_GO_TO_RESTAURANT");
            var ORDER_STATE_IN_RESTAURANT = config.get("ORDER_STATE_IN_RESTAURANT");
            var ORDER_STATE_GO_TO_CLIENT = config.get("ORDER_STATE_GO_TO_CLIENT");
            var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
    
            var Order = Parse.Object.extend("Order");
            var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
    
            var queryOrder = new Parse.Query("Order");
    
            queryOrder.include("courier");
    
            queryOrder.get(request.object.get("order").id).then(
                function (order) {
    
    
                    if (request.object.get("state") == ORDER_STATE_IN_RESTAURANT) {
    
                        var queryOrderUpdateStateGoToRestaurant = new Parse.Query("OrderStateUpdate");
    
                        queryOrderUpdateStateGoToRestaurant.equalTo("order", order);
    
                        queryOrderUpdateStateGoToRestaurant.equalTo("state", ORDER_STATE_GO_TO_RESTAURANT);
    
                        queryOrderUpdateStateGoToRestaurant.find().then(
                            function (orderUpdateStatesResult) {
                                if (orderUpdateStatesResult.length > 0) {
    
                                    var orderUpdateStateGoToRestaurant = orderUpdateStatesResult[0];
    
    
                                    var startTime = orderUpdateStateGoToRestaurant.get("date");
                                    var finishTime = request.object.get("date");
                                    var diffHr = ((finishTime - startTime) * 1.0) / (1000 * 60 * 60);
    
                                    request.object.set("velocityTime", diffHr);
                                    var distKm = (order.get("distanceCourierRestaurant") * 1.0) / 1000;
                                    request.object.set("velocityDistance", distKm);
    
                                    response.success();
    
                                }
                                else {
    
                                    response.success();
                                }
                            }
                        );
    
                    } else if (request.object.get("state") == ORDER_STATE_ARRIVED) {
                        var queryOrderUpdateStateGoToRestaurant = new Parse.Query("OrderStateUpdate");
    
                        queryOrderUpdateStateGoToRestaurant.equalTo("order", order);
    
                        queryOrderUpdateStateGoToRestaurant.equalTo("state", ORDER_STATE_GO_TO_CLIENT);
    
                        queryOrderUpdateStateGoToRestaurant.find().then(
                            function (orderUpdateStatesResult) {
                                if (orderUpdateStatesResult.length > 0) {
    
                                    var orderUpdateStateGoToRestaurant = orderUpdateStatesResult[0];
    
                                    var startTime = orderUpdateStateGoToRestaurant.get("date");
                                    var finishTime = request.object.get("date");
                                    var diffHr = ((finishTime - startTime) * 1.0) / (1000 * 60 * 60);
    
                                    request.object.set("velocityTime", diffHr);
                                    var distKm = (order.get("distanceCourierRestaurant") * 1.0) / 1000;
                                    request.object.set("velocityDistance", distKm);
    
                                    response.success();
    
                                }
                                else {
    
                                    response.success();
                                }
                            }
                        );
    
                    } else if (request.object.get("state") == ORDER_STATE_GO_TO_CLIENT) {
    
                        var queryOrderUpdateStateGoToRestaurant = new Parse.Query("OrderStateUpdate");
    
                        console.log("aqui");
    
    
                        queryOrderUpdateStateGoToRestaurant.equalTo("order", order);
    
                        queryOrderUpdateStateGoToRestaurant.equalTo("state", ORDER_STATE_IN_RESTAURANT);
    
                        queryOrderUpdateStateGoToRestaurant.find().then(
                            function (orderUpdateStatesResult) {
                                if (orderUpdateStatesResult.length > 0) {
    
                                    var orderUpdateStateGoToRestaurant = orderUpdateStatesResult[0];
                                    var startTime = orderUpdateStateGoToRestaurant.get("date");
                                    var finishTime = request.object.get("date");
    
                                    var diffM = ((finishTime - startTime) * 1.0) / (1000 * 60);
    
    
                                    if (order.get("timeWaitRestaurant") == undefined || order.get("timeWaitRestaurant") == 0) {
    
                                        order.set("timeWaitRestaurant", diffM);
                                        order.save();
                                        response.success();
    
                                    } else {
                                        response.success();
                                    }
    
                                } else {
    
                                    response.success();
                                }
                            }
                        );
    
                    } else {
    
                        response.success();
                    }
                }, function(error) {
                    response.success();
                }
            );
        }, function(error) {
            response.success();
        });
    
    });
    
    
    Parse.Cloud.afterSave("OrderStateUpdate", function(request) {
        // Publish change event to socketIO server
        SocketIO.publishUpdate('OrderProduct')
    
    
        Parse.Config.get().then(function (config) {
    
    
            var ORDER_STATE_IN_RESTAURANT = config.get("ORDER_STATE_IN_RESTAURANT");
            var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
    
            var Order = Parse.Object.extend("Order");
            var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
    
            var queryOrder = new Parse.Query("Order");
    
            var courier;
            queryOrder.include("courier");
            queryOrder.include("courier.courier");
    
    
            queryOrder.get(request.object.get("order").id).then(
                function (order) {
    
    
                    var queryOrderStateUpdate1 = new Parse.Query(OrderStateUpdate);
                    var queryOrderStateUpdate2 = new Parse.Query(OrderStateUpdate);
    
    
    
                    var queryOrderByCourier = new Parse.Query(Order);
    
                    queryOrderByCourier.include("courier");
                    queryOrderByCourier.include("courier.courier");
    
                    queryOrderByCourier.equalTo("courier", order.get("courier"));
    
                    queryOrderStateUpdate1.equalTo("state",ORDER_STATE_IN_RESTAURANT);
    
                    queryOrderStateUpdate1.matchesQuery("order", queryOrderByCourier);
    
                    queryOrderStateUpdate2.equalTo("state",ORDER_STATE_ARRIVED);
    
                    queryOrderStateUpdate2.matchesQuery("order", queryOrderByCourier);
    
                    courier = order.get("courier").get("courier");
    
                    var mainQuery = Parse.Query.or(queryOrderStateUpdate1, queryOrderStateUpdate2);
    
                    return mainQuery.find();
    
                }).then(
    
                function(orderStateUpdateStateResult) {
    
                    if(orderStateUpdateStateResult.length > 0) {
    
                        var timeSum = 0;
                        var distanceSum = 0;
    
                        for (var i = 0; i < orderStateUpdateStateResult.length; i++) {
    
                            var orderStateUpdate = orderStateUpdateStateResult[i];
                            timeSum = orderStateUpdate.get("velocityTime");
                            distanceSum = orderStateUpdate.get("velocityDistance");
    
                        }
    
    
    
                        var velocity = 1.0 * (distanceSum / timeSum);
    
                        courier.set("velocity", velocity);
    
                        courier.save();
    
                    }
    
    
                }, function(error) {
                   console.log(error);
                })
    
    
        });

    });
    
    
    Parse.Cloud.afterSave("Order", function(request) {
        // Publish change event to socketIO server
        SocketIO.publishUpdate('OrderProduct')
    
        Parse.Config.get().then(function (config) {
    
            var ORDER_STATE_CANCEL_BY_CLIENT = config.get("ORDER_STATE_CANCEL_BY_CLIENT");
    
    
    
            var ORDER_STATE_DELIVERY_FAIL_CLIENT = config.get("ORDER_STATE_DELIVERY_FAIL_CLIENT");
            var ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER = config.get("ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER");
            var ORDER_STATE_ORDER_DELIVERY = config.get("ORDER_STATE_ORDER_DELIVERY");
    
    
            var Order = Parse.Object.extend("Order");
            var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
            var queryOrder = new Parse.Query("Order");
            queryOrder.include("restaurant");
            queryOrder.include("client");
            queryOrder.include("client.client");
            queryOrder.include("client.client.privateData");
            queryOrder.include("client.client.privateData.cardMaster");
            queryOrder.include("commission");
            Parse.Cloud.useMasterKey();
    
            queryOrder.get(request.object.id).then(
                function (order) {
    
    
                    if (order.get("state") == ORDER_STATE_CANCEL_BY_CLIENT) {
    
    
    
                        var EligibleCourier = Parse.Object.extend("EligibleCourier");
                        var queryEligible = new Parse.Query("EligibleCourier");
    
                        queryEligible.equalTo("order", order);
    
    
                        queryEligible.find( {useMasterKey: true}).then(
                            function(eligibles) {
    
                                for(var i =0; i<eligibles.length; i++) {
    
                                    eligibles[i].set("history",true);
                                }
                                Parse.Cloud.useMasterKey();
                                Parse.Object.saveAll(eligibles);
                            }
                        );
    
    
                    } else {
    
    ///payment
    
    
                       if(order.get("pay")==false &&
                            (order.get("state") == ORDER_STATE_DELIVERY_FAIL_CLIENT ||
                            order.get("state") == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER ||
                            order.get("state") == ORDER_STATE_ORDER_DELIVERY))
                        {
    
                            console.log("Aqui");
    
                            var SP = require("cloud/switchPayments.js");
                            SP.initialize('15e66865060db45b19a8160803ba817dca8af44f553a109e', '5663297ec447953a5a4b38098d642be959a4ba0b553a109e','api-test.switchpayments.com/v1/');
    
                            var CardMaster = Parse.Object.extend("CardMaster");
    
                            var cardMaster = new CardMaster();
    
                            cardMaster = order.get("client").get("client").get("privateData").get("cardMaster");
    
                            SP.createPayment(order.get("totalPrice")+order.get("commission").get("value"), "EUR",cardMaster.get("CardId")).then(
                                function(data) {
    
                                  console.log(data);
    
                                    order.set("pay",true);
                                    order.save();
    
                                },function(error) {
    
                                    console.log(error);
    
                                });
    
                        }
    
                        var queryOrders = new Parse.Query("Order");
    
                        queryOrders.include("restaurant");
                        queryOrders.include("products");
    
                        queryOrders.equalTo("restaurant", order.get("restaurant"));
    
                        queryOrders.find({useMasterKey: true}).then(
                            function (orders) {
    
                                var timeSum = 0;
                                var nProduct = 0;
    
                                for (var i = 0; i < orders.length; i++) {
    
                                    if (orders[i].get("timeWaitRestaurant") != 0 && orders[i].get("timeWaitRestaurant") != undefined) {
    
    
                                        timeSum = timeSum + orders[i].get("timeWaitRestaurant");
    
                                        for (var j = 0; j < orders[i].get("products").length; j++) {
    
                                            nProduct = nProduct + orders[i].get("products")[j].get("quantity");
                                        }
    
                                    }
                                }
                                var waitTime = (1.0 * timeSum) / nProduct;
    
                                order.get("restaurant").set("waitTime", waitTime);
                                order.get("restaurant").save(null, {useMasterKey: true});
                            },
                            function (error) {
                                order.get("restaurant").set("waitTime", 0);
                                order.get("restaurant").save(null, {useMasterKey: true});
                            }
                        );
    
                    }
    
    
                }
            );
        });
    });
    
    
    Parse.Cloud.afterSave("Rating", function(request) {
        // Publish change event to socketIO server
        SocketIO.publishUpdate('OrderProduct')
    
        var Rating = Parse.Object.extend("Rating");
    
        var queryRating = new Parse.Query("Rating");
    
        queryRating.include("reviewer");
    
        queryRating.include("rated");
        queryRating.include("rated.client");
        queryRating.include("rated.courier");
        Parse.Cloud.useMasterKey();
        queryRating.get(request.object.id).then(
    
            function(rating) {
    
                var queryRatings = new Parse.Query("Rating");
    
                queryRatings.equalTo("rated", rating.get("rated"));
    
                queryRatings.find().then(
                    function(ratings){
    
                        var ratingSum = 0;
                        for (var i = 0; i < ratings.length; i++) {
    
                            ratingSum+=ratings[i].get("value");
    
                        }
    
                        var ratingAVG = (1.0*ratingSum)/ratings.length;
    
                        var rated = rating.get("rated");
    
                        if(rated.get("client")!=undefined) {
    
                            rated.get("client").set("classification", ratingAVG);
    
                            rated.get("client").save();
                        } else {
    
                            rated.get("courier").set("classification", ratingAVG);
                            rated.get("courier").save();
                        }
    
    
                    }
                );
    
            }
        );
    
    });
    
    Parse.Cloud.afterSave("RatingRestaurant", function(request) {
        // Publish change event to socketIO server
        SocketIO.publishUpdate('OrderProduct')
    
        var Rating = Parse.Object.extend("Rating");
    
        var queryRating = new Parse.Query("RatingRestaurant");
    
        queryRating.include("reviewer");
    
        queryRating.include("restaurant");
        Parse.Cloud.useMasterKey();
        queryRating.get(request.object.id).then(
    
            function(rating) {
    
                var queryRatings = new Parse.Query("RatingRestaurant");
    
                queryRatings.equalTo("restaurant", rating.get("restaurant"));
    
                queryRatings.find().then(
                    function(ratings){
    
                        var ratingSum = 0;
                        for (var i = 0; i < ratings.length; i++) {
    
                            ratingSum+=ratings[i].get("value");
    
                        }
    
                        var ratingAVG = (1.0*ratingSum)/ratings.length;
    
                        var restaurant = rating.get("restaurant");
    
                        restaurant.set("rating", ratingAVG);
                        restaurant.save();
                        
   
    
                    }
                );
    
            }
        );

        // Publish change event to socketIO server
        SocketIO.publishUpdate('OrderProduct')
    
    });
    
    
    Parse.Cloud.beforeSave("Order", function(request, response) {
    
        Parse.Config.get().then(function (config) {
    
            var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
            var ORDER_STATE_ARRIVED_WAIT_CLIENT = config.get("ORDER_STATE_ARRIVED_WAIT_CLIENT");
            var ORDER_STATE_CLIENT_ACCEPT = config.get("ORDER_STATE_CLIENT_ACCEPT");
            var ORDER_STATE_CLIENT_ACCEPT_IN_RESTAURANT = config.get("ORDER_STATE_CLIENT_ACCEPT_IN_RESTAURANT");
            var ORDER_STATE_DELIVERY_FAIL = config.get("ORDER_STATE_DELIVERY_FAIL");
            var ORDER_STATE_DELIVERY_FAIL_ANALYZE = config.get("ORDER_STATE_DELIVERY_FAIL_ANALYZE");
            var ORDER_STATE_DELIVERY_FAIL_CLIENT = config.get("ORDER_STATE_DELIVERY_FAIL_CLIENT");
            var ORDER_STATE_DELIVERY_FAIL_COURIER = config.get("ORDER_STATE_DELIVERY_FAIL_COURIER");
            var ORDER_STATE_FAIL_NOT_HAVE_COURIER = config.get("ORDER_STATE_FAIL_NOT_HAVE_COURIER");
            var ORDER_STATE_GO_TO_CLIENT = config.get("ORDER_STATE_GO_TO_CLIENT");
            var ORDER_STATE_GO_TO_RESTAURANT = config.get("ORDER_STATE_GO_TO_RESTAURANT");
            var ORDER_STATE_HAVE_COURIER = config.get("ORDER_STATE_HAVE_COURIER");
            var ORDER_STATE_IN_RESTAURANT = config.get("ORDER_STATE_IN_RESTAURANT");
            var ORDER_STATE_ORDER_DELIVERY = config.get("ORDER_STATE_ORDER_DELIVERY");
            var ORDER_STATE_PAY = config.get("ORDER_STATE_PAY");
            var ORDER_STATE_PROOF_PAY = config.get("ORDER_STATE_PROOF_PAY");
            var ORDER_STATE_WAIT_CLIENT_ACCEPT = config.get("ORDER_STATE_WAIT_CLIENT_ACCEPT");
            var ORDER_STATE_WAIT_CLIENT_ACCEPT_IN_RESTAURANT = config.get("ORDER_STATE_WAIT_CLIENT_ACCEPT_IN_RESTAURANT");
            var ORDER_STATE_WAIT_COURIER = config.get("ORDER_STATE_WAIT_COURIER");
            var ORDER_STATE_INIT = config.get("ORDER_STATE_INIT");
            var ORDER_STATE_CANCEL_BY_CLIENT = config.get("ORDER_STATE_CANCEL_BY_CLIENT");
            var ORDER_STATE_FAIL_CLIENT_NOT_ACCEPT = config.get("ORDER_STATE_FAIL_CLIENT_NOT_ACCEPT");
            var ORDER_STATE_CANCEL_RESTAURANT_CLOSE = config.get("ORDER_STATE_CANCEL_RESTAURANT_CLOSE");
            var ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER = config.get("ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER");
            var Order = Parse.Object.extend("Order");
    
            var newOrder= request.object;
    
            var newState = newOrder.get("state");
    
            var queryOrder = new Parse.Query("Order");
    
            queryOrder.equalTo("objectId",newOrder.id);
    
            queryOrder.find().then(
                function(oldOrder) {
    
                    if(oldOrder.length>0) {
                        var oldState = oldOrder[0].get("state");
                        console.log("old: "+ oldState + " new:" + newState);
                        switch(oldOrder[0].get("state")) {
    
                            case ORDER_STATE_INIT:
                                if( newState == ORDER_STATE_INIT || newState == ORDER_STATE_WAIT_COURIER || newState == ORDER_STATE_CANCEL_BY_CLIENT ) {
    
                                    response.success();
    
                                }  else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
    
                                }
                                break;
    
                            case ORDER_STATE_WAIT_COURIER:
                                if( newState == ORDER_STATE_HAVE_COURIER || newState == ORDER_STATE_WAIT_COURIER || newState == ORDER_STATE_CANCEL_BY_CLIENT || newState == ORDER_STATE_FAIL_NOT_HAVE_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_HAVE_COURIER:
    
                                if( newState == ORDER_STATE_HAVE_COURIER || newState == ORDER_STATE_CLIENT_ACCEPT || newState == ORDER_STATE_WAIT_CLIENT_ACCEPT || newState == ORDER_STATE_GO_TO_RESTAURANT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
    
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_WAIT_CLIENT_ACCEPT:
    
                                if(newState == ORDER_STATE_WAIT_CLIENT_ACCEPT || newState == ORDER_STATE_CLIENT_ACCEPT || newState == ORDER_STATE_FAIL_CLIENT_NOT_ACCEPT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_CLIENT_ACCEPT:
    
                                if(newState == ORDER_STATE_CLIENT_ACCEPT || newState == ORDER_STATE_GO_TO_RESTAURANT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
    
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_GO_TO_RESTAURANT:
    
                                if(newState == ORDER_STATE_GO_TO_RESTAURANT || newState == ORDER_STATE_IN_RESTAURANT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
    
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_IN_RESTAURANT:
    
                                if(newState == ORDER_STATE_IN_RESTAURANT || newState == ORDER_STATE_CLIENT_ACCEPT_IN_RESTAURANT || newState == ORDER_STATE_WAIT_CLIENT_ACCEPT_IN_RESTAURANT || newState == ORDER_STATE_CANCEL_RESTAURANT_CLOSE || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
    
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_WAIT_CLIENT_ACCEPT_IN_RESTAURANT:
    
                                if(newState == ORDER_STATE_WAIT_CLIENT_ACCEPT_IN_RESTAURANT || newState == ORDER_STATE_CLIENT_ACCEPT_IN_RESTAURANT || newState == ORDER_STATE_FAIL_CLIENT_NOT_ACCEPT  || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_CLIENT_ACCEPT_IN_RESTAURANT :
    
                                if(newState == ORDER_STATE_CLIENT_ACCEPT_IN_RESTAURANT || newState == ORDER_STATE_PAY || newState == ORDER_STATE_PROOF_PAY || newState ==ORDER_STATE_GO_TO_CLIENT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_PAY:
    
                                if( newState == ORDER_STATE_PAY || newState == ORDER_STATE_GO_TO_CLIENT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_PROOF_PAY:
    
                                if( newState == ORDER_STATE_PROOF_PAY || newState ==ORDER_STATE_GO_TO_CLIENT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_GO_TO_CLIENT:
    
                                if(newState == ORDER_STATE_GO_TO_CLIENT || newState==ORDER_STATE_ARRIVED || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_ARRIVED:
    
                                if(newState == ORDER_STATE_ARRIVED || newState == ORDER_STATE_ORDER_DELIVERY || newState == ORDER_STATE_ARRIVED_WAIT_CLIENT || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                } else {
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
    
                                break;
    
                            case ORDER_STATE_ARRIVED_WAIT_CLIENT:
    
                                if(newState == ORDER_STATE_ARRIVED_WAIT_CLIENT || newState == ORDER_STATE_ORDER_DELIVERY || newState == ORDER_STATE_DELIVERY_FAIL || newState == ORDER_STATE_CANCEL_BY_CLIENT_WITH_COURIER) {
    
                                    response.success();
                                }  else {
    
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_DELIVERY_FAIL:
    
                                if(newState == ORDER_STATE_DELIVERY_FAIL || newState == ORDER_STATE_DELIVERY_FAIL_CLIENT || newState == ORDER_STATE_DELIVERY_FAIL_ANALYZE) {
    
                                    response.success();
                                }  else {
    
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            case ORDER_STATE_DELIVERY_FAIL_ANALYZE:
    
                                if( newState = ORDER_STATE_DELIVERY_FAIL_COURIER || newState == ORDER_STATE_DELIVERY_FAIL_ANALYZE || newState == ORDER_STATE_DELIVERY_FAIL_CLIENT ) {
    
                                    response.success();
                                }  else {
    
    
                                    response.error("Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                                }
                                break;
    
                            default :
    
                                response.error("Default Incorrect state. OldState: " + oldOrder[0].get("state") + " newState: "+ newState);
                        }
                    } else {
    
                        response.success();
                    }
    
    
    
                }
            );
        });
    });
    
    
    
    Parse.Cloud.beforeSave("EligibleCourier", function(request, response) {
    
        Parse.Config.get().then(function (config) {
    
            var EligibleCourier = Parse.Object.extend("EligibleCourier");
    
            var ELIIGIBLE_COURIERS_ACCEPT = config.get("ELIIGIBLE_COURIERS_ACCEPT");
            var ELIIGIBLE_COURIERS_LAST_TRY = config.get("ELIIGIBLE_COURIERS_LAST_TRY");
            var ELIIGIBLE_COURIERS_NOT_CONTACT = config.get("ELIIGIBLE_COURIERS_NOT_CONTACT");
            var ELIIGIBLE_COURIERS_NOT_RESPONSE = config.get("ELIIGIBLE_COURIERS_NOT_RESPONSE");
            var ELIIGIBLE_COURIERS_REFUSE = config.get("ELIIGIBLE_COURIERS_REFUSE");
            var ELIIGIBLE_COURIERS_WAIT_RESPONSE = config.get("ELIIGIBLE_COURIERS_WAIT_RESPONSE");
            var ORDER_STATE_WAIT_COURIER = config.get("ORDER_STATE_WAIT_COURIER");
    
            var newEligibleCourier = request.object;
    
            var newState = newEligibleCourier.get("state");
    
            var queryOrder = new Parse.Query("EligibleCourier");
    
            queryOrder.include("order");
    
            queryOrder.equalTo("objectId",newEligibleCourier.id);
    
            queryOrder.find().then(
                function(oldEligibleCourier) {
    
                    if(oldEligibleCourier.length>0) {
    
                        if(oldEligibleCourier[0].get("history")==true || oldEligibleCourier[0].get("order").get("state") != ORDER_STATE_WAIT_COURIER) {
    
                            response.error("Incorrect state");
    
                        } else {
    
                            switch(oldEligibleCourier[0].get("state")) {
    
                                case ELIIGIBLE_COURIERS_NOT_CONTACT :
    
                                    if(newState == ELIIGIBLE_COURIERS_WAIT_RESPONSE || newState == ELIIGIBLE_COURIERS_NOT_CONTACT) {
    
                                        response.success();
    
                                    }  else {
    
                                        response.error("Incorrect state");
                                    }
    
                                    break;
    
                                case ELIIGIBLE_COURIERS_WAIT_RESPONSE:
    
                                    if(newState == ELIIGIBLE_COURIERS_WAIT_RESPONSE || newState == ELIIGIBLE_COURIERS_NOT_RESPONSE || newState == ELIIGIBLE_COURIERS_ACCEPT || newState == ELIIGIBLE_COURIERS_REFUSE) {
    
                                        response.success();
    
                                    }  else {
    
                                        response.error("Incorrect state");
                                    }
    
                                    break;
    
                                case ELIIGIBLE_COURIERS_LAST_TRY:
    
                                    if(newState == ELIIGIBLE_COURIERS_LAST_TRY || newState == ELIIGIBLE_COURIERS_NOT_RESPONSE || newState == ELIIGIBLE_COURIERS_ACCEPT || newState == ELIIGIBLE_COURIERS_REFUSE) {
    
                                        response.success();
    
                                    }  else {
    
                                        response.error("Incorrect state");
                                    }
    
                                    break;
    
    
    
                                default:
    
                                    response.error("Incorrect state");
    
                            }
    
                        }
    
                    } else {
    
                        response.success();
    
                    }
    
    
    
                }
            );
    
        });
    });
    
Parse.Cloud.beforeSave("OrderProduct", function(request, response) {
   var queryOrderProduct = new Parse.Query("OrderProduct");
   
    var OrderProduct = Parse.Object.extend("OrderProduct");
    var OrderProductUpdate = Parse.Object.extend("OrderProductUpdate");
 
    var newOrderProduct = new OrderProduct();
   
   newOrderProduct =  request.object;
   
   queryOrderProduct.equalTo("objectId",newOrderProduct.id);
   
   queryOrderProduct.find().then(
       function (objects) {
 
          
          if(objects.length>0) {
             
              var oldOrderProduct = new OrderProduct();
              oldOrderProduct = objects[0];
              var orderProductUpdate = new OrderProductUpdate();
              orderProductUpdate.set("oldQuantity", oldOrderProduct.get("quantity"));
              orderProductUpdate.set("oldPrice",oldOrderProduct.get("realPrice"));
              orderProductUpdate.set("newQuantity", newOrderProduct.get("quantity"));
              orderProductUpdate.set("newPrice",newOrderProduct.get("realPrice"));
              orderProductUpdate.set("orderProduct", oldOrderProduct);
              orderProductUpdate.set("updateBy", Parse.User.current());
                
             orderProductUpdate.save().then(
                 
                 function (orderP) {
                
                       response.success();
             }, function (error) {
               
                  response.success();
            });
            
          }else {
             
                response.success();
          }
          
          
       }, function (error) {
             
             response.success();
       });
     
 }); 
    


Parse.Cloud.afterSave("OrderProduct", function(request) {
    // Publish change event to socketIO server
    SocketIO.publishUpdate('OrderProduct')
    
   var OrderProduct = Parse.Object.extend("OrderProduct");
   var Order = Parse.Object.extend("Order");
   var orderProduct = new OrderProduct();
   orderProduct = request.object;
   
   var queryOrderProducts = new Parse.Query("OrderProduct");
   
   queryOrderProducts.equalTo("order",orderProduct.get("order"));
   
   queryOrderProducts.find().then(
       function (objects) {
           var count = 0;
           for(var i = 0; i< objects.length; i++) {
               
              var orderP = new OrderProduct();
              orderP= objects[i];
              count = count + (orderP.get("realPrice") * orderP.get("quantity"));   
           }
           
          var queryOrder = new Parse.Query("Order"); 
          console.log(orderProduct.get("order").id);
          queryOrder.equalTo("objectId",orderProduct.get("order").id);
          queryOrder.include("commission");
           
          queryOrder.find().then(
              function (objects) {
                  var order = new Order();
                  order = objects[0];
                  if(order.get("commission")!=undefined) {
                       count = count + order.get("commission").get("value");
                  }
      
                  order.set("totalPrice", count);
                  
                  order.save();
              }
          );
       }
   );
    
});


Parse.Cloud.afterSave("Product", function(request) {
    // Publish change event to socketIO server
    SocketIO.publishUpdate('Product')
})

Parse.Cloud.afterSave("Restaurant", function(request) {
    // Publish change event to socketIO server
    SocketIO.publishUpdate('Restaurant')
})

