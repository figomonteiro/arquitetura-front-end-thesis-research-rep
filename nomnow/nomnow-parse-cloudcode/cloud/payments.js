/**
 * Created by cristianoalves on 28/04/15.
 */


Parse.Cloud.define("createCard", function (request, response) {

    var token = request.params.token;
    var CardMaster = Parse.Object.extend("CardMaster");
    var Card = Parse.Object.extend("Card");
    var SP = require("cloud/switchPayments.js");
    SP.initialize('15e66865060db45b19a8160803ba817dca8af44f553a109e', '5663297ec447953a5a4b38098d642be959a4ba0b553a109e','api-test.switchpayments.com/v1/');

    var currentUser = Parse.User.current();

    if(!currentUser) {

        response.error();
        return;
    }

    SP.createCard(token).then(
        function(data){

            var cardMaster = new CardMaster();
            var card = new Card();

            cardMaster.set("brand", data["brand"]);
            card.set("brand", data["brand"]);

            cardMaster.set("expirationMonth", data["expiration_month"]);
            card.set("expirationMonth", data["expiration_month"]);

            cardMaster.set("fingerprint", data["fingerprint"]);
            cardMaster.set("active", data["active"]);
            cardMaster.set("CardId", data["id"]);

            cardMaster.set("last4Digits", data["last_4_digits"]);
            card.set("last4Digits", data["last_4_digits"]);

            cardMaster.set("name", data["name"]);
            card.set("name", data["name"]);

            cardMaster.set("expirationYear", data["expiration_year"]);
            card.set("expirationYear", data["expiration_year"]);

            cardMaster.set("country", data["country"]);
            card.set("country", data["country"]);

            cardMaster.set("token", token);
            var acl = new Parse.ACL();
            var aclCard = new Parse.ACL();

            acl.setPublicReadAccess(false);
            acl.setPublicWriteAccess(false);

            aclCard.setPublicReadAccess(false);
            aclCard.setPublicWriteAccess(false);
            aclCard.setReadAccess(currentUser,true);
            aclCard.setWriteAccess(currentUser,true);

            cardMaster.setACL(acl);
            card.setACL(aclCard);


            Parse.Cloud.useMasterKey();
              console.log(data);
            cardMaster.save().then(
              function() {

                  card.save().then(
                      function(card_){



                          var queryUser = new Parse.Query(Parse.User);

                          queryUser.equalTo("objectId", currentUser.id);
                          queryUser.include("client");
                          queryUser.include("client.privateData");

                          queryUser.find().then(
                              function (users) {
                                  var user = users[0];

                                  user.get("client").get("privateData").set("card", card_);
                                  user.get("client").get("privateData").set("cardMaster",cardMaster);
                                  user.save();

                                  response.success(user);
                              }
                          );




                      }, function(error) {

                          response.error(error);
                      }
                  );

              }, function(error) {

                    response.error(error);
                }
            );

        },function(error) {

            response.error(error);

        });

});