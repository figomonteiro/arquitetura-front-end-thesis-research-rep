/**
 * SwitchPayment Module
 * @name SwitchPayment
 * @namespace SP
 *
 *
 * <ul><li>Module Version: 1.0.0</li>
 *
 *
 */

(function() {

    const SANDBOX = 'api-test.switchpayments.com/v1/';
    const LIVE = 'api.switchpayments.com/v1/';
    var environment;
    var merchant_id;
    var private_key;

    module.exports = {

        initialize: function(merchant_key,key, environmentType) {
            environment = environmentType;
            private_key = key;
            merchant_id = merchant_key;
            return this;
        },

        createCard: function(oneTimeCardToken) {
            var promise = new Parse.Promise();
            Parse.Cloud.httpRequest({
                method: "POST",
                url: "https://" + merchant_id +":"+private_key+ "@" + environment+"/cards",
                body: '{"card":"' + oneTimeCardToken +'"}'
            }).then(function(httpResponse) {
                promise.resolve(httpResponse.data);

            }, function(httpResponse) {


                promise.reject(httpResponse.data);
            });

            return promise;
        },

        createPayment: function(amount,currency, card) {
            var promise = new Parse.Promise();
            Parse.Cloud.httpRequest({
                method: "POST",
                url: "https://" + merchant_id +":"+private_key+ "@" + environment+"/payments",
                body: '{"amount":'+ amount +', "capture_on_creation": true, "currency": "EUR" , "card": "' + card + '"}'
            }).then(function(httpResponse) {
                promise.resolve(httpResponse.data);

            }, function(httpResponse) {

                promise.reject(httpResponse.data);
            });

            return promise;

        }, capturePayment: function(amount, transationId) {
            var promise = new Parse.Promise();
            Parse.Cloud.httpRequest({
                method: "POST",
                url: "https://" + merchant_id + ":" + private_key + "@" + environment + "/payments/"+transationId+"/capture/",
                body: '{"amount":' + amount +'}'
            }).then(function (httpResponse) {
                promise.resolve(httpResponse.data);

            }, function (httpResponse) {
                //     promise.resolve();

                promise.reject(httpResponse.data);
            });

            return promise;
        }
    }
}());
