/**
 * Created by cristianoalves on 20/04/15.
 */


Parse.Cloud.define("cron", function (request, response) {


    Parse.Config.get().then(function (config) {

        var ORDER_STATE_WAIT_COURIER = config.get("ORDER_STATE_WAIT_COURIER");
        var queryOrders = new Parse.Query("Order");
        queryOrders.equalTo("state",ORDER_STATE_WAIT_COURIER);

        var Order = Parse.Object.extend("Order");


        Parse.Cloud.useMasterKey();
        queryOrders.find({ useMasterKey: true }).then(
        function (orders) {

            var count=0;
            var size = orders.length;

            console.log("count: "+count);

           if(count == size) {

                response.success();

            } else {
                for(var i =0;i<orders.length; i++) {

                    var order = new Order();
                    order = orders[i];


                    Parse.Cloud.run('checkOrderState', {"order": order.id}, {
                        success: function (result) {
                            console.log(result);
                            count++;
                            if(count == size) {
                                response.success();

                            }
                        },
                        error: function (error) {
                            console.log(error);
                            count++;
                            if(count == size) {
                                response.success();

                            }
                        }
                    });
                }
            }

        }, function (error) {
                console.log("aqui2");
                response.error(error);

            }
        );
    });
});
