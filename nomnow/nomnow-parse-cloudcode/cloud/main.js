require("cloud/restaurant.js")
require("cloud/order.js")
require("cloud/updateOrder.js")
require("cloud/triggers.js")
require("cloud/job.js")
require("cloud/cron.js")
require("cloud/payments.js")
require("cloud/switchTest.js")
require("cloud/changeEvents.js")