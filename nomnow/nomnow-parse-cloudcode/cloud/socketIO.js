/**
 * SocketIO REST API Module
 * @name SocketIO
 * @namespace SocketIO
 *
 *
 * <ul><li>Module Version: 1.0.0</li>
 *
 *
 */

 (function() {
    var SOCKET_IO_SERVER_URL = null

    Parse.Config.get().then(function (config) {
        SOCKET_IO_SERVER_URL = config.get('SOCKET_IO_SERVER_URL')
    })

    module.exports = {
        publishUpdate: function(collectionName) {
            if (!SOCKET_IO_SERVER_URL) {
                throw new Error('No \'SOCKET_IO_SERVER_URL\' config defined in Parse.')
            }
            // Call socketIO HTTP REST API
            Parse.Cloud.httpRequest({
                url: SOCKET_IO_SERVER_URL + 'update/' + collectionName,
                success: function(httpResponse) {
                    console.log(httpResponse.text)
                },
                error: function(httpResponse) {
                    console.error('Request failed ' + httpResponse.status)
                }
            })
        }
    }
}())
