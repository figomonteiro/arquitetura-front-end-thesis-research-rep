/**
 * Created by cristianoalves on 16/03/15.
 */

Parse.Cloud.define("updateProducts", function (request, response) {

    Parse.Config.get().then(function (config) {

        var ORDER_STATE_WAIT_CLIENT_ACCEPT = config.get("ORDER_STATE_WAIT_CLIENT_ACCEPT");
        var ORDER_STATE_IN_RESTAURANT = config.get("ORDER_STATE_IN_RESTAURANT");
        var Order = Parse.Object.extend("Order");
        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");

        var order = new Order();

        order.id = request.params.order;

        var position = request.params.position;

        var queryOrder = new Parse.Query(Order);

        queryOrder.include("client");
        queryOrder.include("courier");

        queryOrder.get(order.id).then(

            function(orderResult) {
                order = orderResult;


                if(order.get("courier").id != request.user.id) {
                    throw "Not a Courier";

                } else if ( order.get("state") != ORDER_STATE_IN_RESTAURANT) {

                    throw "Incorrect state";
                }

                order.set("state", ORDER_STATE_WAIT_CLIENT_ACCEPT);
                order.set("acceptedByClient", false);

                return order.save();

            }).then(

            function (orderSave) {

                var update = new OrderStateUpdate();

                update.set("state", ORDER_STATE_WAIT_CLIENT_ACCEPT);
                update.set("location", position);
                update.set("date", new Date());
                update.set("order", order);
                update.set("seen", false);
                update.save();

                var queryInstallation = new Parse.Query(Parse.Installation);

                queryInstallation.equalTo("user", order.get("client"));
                Parse.Push.send({
                    where: queryInstallation,
                    data: {
                        alert: "Update products"
                    }
                }, {
                    success: function () {

                    },
                    error: function (error) {

                        console.log(error);
                    }

                });

                response.success(order);


            }, function(error) {

                response.error(error);

            });

    });

});



Parse.Cloud.define("clientAccepted", function (request, response) {

    Parse.Config.get().then(function (config) {

        var ORDER_STATE_CLIENT_ACCEPT = config.get("ORDER_STATE_CLIENT_ACCEPT");
        var ORDER_STATE_WAIT_CLIENT_ACCEPT = config.get("ORDER_STATE_WAIT_CLIENT_ACCEPT");

        var Order = Parse.Object.extend("Order");

        var order = new Order();

        order.id = request.params.order;

        var queryOrder = new Parse.Query(Order);

        queryOrder.include("courier");
        queryOrder.include("client");

        queryOrder.get(order.id).then(
            function (orderResult) {

                order = orderResult;

                if(order.get("client").id != request.user.id) {
                    throw "Not a client";

                } else if ( order.get("state") != ORDER_STATE_WAIT_CLIENT_ACCEPT) {

                    throw "Incorrect state";
                }



                order.set("state", ORDER_STATE_CLIENT_ACCEPT);

                order.set("acceptedByClient", true);
                return order.save();

            }).then(
            function (orderSave) {

                var queryInstallation = new Parse.Query(Parse.Installation);

                queryInstallation.equalTo("user", order.get("courier"));

                Parse.Push.send({
                    where: queryInstallation,
                    data: {
                        alert: "Client acepted"
                    }
                }, {
                    success: function () {

                    },
                    error: function (error) {

                        console.log(error);
                    }

                });

                response.success(orderSave);

            }, function(error) {

                response.error(error);

            });
    });

});



Parse.Cloud.define("courierArrived", function (request, response) {

    Parse.Config.get().then(function (config) {

        var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
        var ORDER_STATE_GO_TO_CLIENT = config.get("ORDER_STATE_GO_TO_CLIENT");
        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");

        var Order = Parse.Object.extend("Order");

        var order = new Order();

        var position = request.params.position;
        order.id = request.params.order;

        var queryOrder = new Parse.Query(Order);

        queryOrder.include("client");
        queryOrder.include("courier");

        queryOrder.get(order.id).then(

            function (orderResult) {

                order = orderResult;
                if(order.get("courier").id != request.user.id) {
                    throw "Not a courier";

                } else if ( order.get("state") != ORDER_STATE_GO_TO_CLIENT) {

                    throw "Incorrect state";
                }



                order.set("state", ORDER_STATE_ARRIVED);

                return order.save();
            }).then(

            function(orderSave) {

                var update = new OrderStateUpdate();

                update.set("state", ORDER_STATE_ARRIVED);
                update.set("location", position);
                update.set("date", new Date());
                update.set("order", order);
                update.set("seen", false);
                update.save();



                var queryInstallation = new Parse.Query(Parse.Installation);

                queryInstallation.equalTo("user", order.get("client"));


                Parse.Push.send({
                    where: queryInstallation,
                    data: {
                        alert: "Courier arrived"
                    }
                }, {
                    success: function () {

                    },
                    error: function (error) {

                        console.log(error);
                    }

                });

                response.success(order);

            }, function(error) {

                response.error(error);

            });
    });

});

Parse.Cloud.define("courierUploadPhoto", function (request, response) {

    Parse.Config.get().then(function (config) {

        var ORDER_STATE_ARRIVED_WAIT_CLIENT = config.get("ORDER_STATE_ARRIVED_WAIT_CLIENT");
        var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");

        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
        var Order = Parse.Object.extend("Order");

        var order = new Order();

        var position = request.params.position;
        order.id = request.params.order;
        var photoFile = request.params["photoFile"];

        var queryOrder = new Parse.Query(Order);
        queryOrder.include("client");
        queryOrder.include("courier");

        queryOrder.get(order.id).then(
            function(orderResult) {
                order = orderResult;

                if(order.get("courier").id != request.user.id) {
                    throw "Not a courier";

                } else if ( order.get("state") != ORDER_STATE_ARRIVED) {

                    throw "Incorrect state";
                }



                order.set("state", ORDER_STATE_ARRIVED_WAIT_CLIENT);



                order.set("courierPhoto", photoFile);
                return order.save();

            }).then(

            function(orderSave) {


                var update = new OrderStateUpdate();

                update.set("state", ORDER_STATE_ARRIVED_WAIT_CLIENT);
                update.set("location", position);
                update.set("date", new Date());
                update.set("order", order);
                update.set("seen", false);

                update.save();

                var queryInstallation = new Parse.Query(Parse.Installation);

                queryInstallation.equalTo("user", order.get("client"));


                Parse.Push.send({
                    where: queryInstallation,
                    data: {
                        alert: "Courier upload photo"
                    }
                }, {
                    success: function () {

                    },
                    error: function (error) {

                        console.log(error);
                    }

                });

                response.success(order);

            }, function(error) {

                response.error(error);

            });
    });

});



Parse.Cloud.define("courierArrivedConfirm", function (request, response) {

    Parse.Config.get().then(function (config) {

        Parse.Cloud.useMasterKey();
        var ORDER_STATE_ARRIVED_WAIT_CLIENT = config.get("ORDER_STATE_ARRIVED_WAIT_CLIENT");
        var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
        var ORDER_STATE_ORDER_DELIVERY = config.get("ORDER_STATE_ORDER_DELIVERY");
        var COURIER_STATE_OFF = config.get("COURIER_STATE_OFF");
        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
        var Order = Parse.Object.extend("Order");

        var order = new Order();

        var position = request.params.position;
        order.id = request.params.order;
        var pin = request.params["pin"];


        var queryOrder = new Parse.Query(Order);

        queryOrder.include("client");
        queryOrder.include("courier");
        queryOrder.include("courier.courier");
        queryOrder.include("courier.courier.privateData");
        queryOrder.include("client.client");
        queryOrder.include("clientData");
        queryOrder.include("client.client.courierData");

        queryOrder.get(order.id).then(
            function (orderResult) {
                order = orderResult;

                if (order.get("courier").id != request.user.id) {
                    throw "Not a courier";

                } else if (order.get("state") != ORDER_STATE_ARRIVED && order.get("state") != ORDER_STATE_ARRIVED_WAIT_CLIENT) {

                    throw "Incorrect state";
                }

                if (order.get("clientData").get("validationPIN") != pin) {

                    throw "Incorrect PIN";
                }

                order.set("state", ORDER_STATE_ORDER_DELIVERY);
                order.set("deliveryDate", new Date());

                return order.save();

            }).then(
            function (orderSave) {

                var update = new OrderStateUpdate();

                update.set("state", ORDER_STATE_ORDER_DELIVERY);
                update.set("location", position);
                update.set("date", new Date());
                update.set("order", order);
                update.set("seen", false);
                update.save();

                var userClient = order.get("client");
                var client = order.get("client").get("client");
                var courierClient = order.get("client").get("client").get("courierData");
                var courier = order.get("courier");

                var aclUserClient = userClient.getACL();

                aclUserClient.setReadAccess(courier, false);
                aclUserClient.setWriteAccess(courier, false);
                userClient.setACL(aclUserClient);

                var aclClient = client.getACL();

                aclClient.setReadAccess(courier, false);
                aclClient.setWriteAccess(courier, false);
                client.setACL(aclClient);

                var aclCourierClient = courierClient.getACL();

                aclCourierClient.setReadAccess(courier, false);
                aclCourierClient.setWriteAccess(courier, false);
                courierClient.setACL(aclCourierClient);


                courier.get("courier").get("privateData").set("state", COURIER_STATE_OFF);
                courier.get("courier").get("privateData").save();

                courierClient.save();
                client.save();
                userClient.save();

                courier.save();

                var queryInstallation = new Parse.Query(Parse.Installation);

                queryInstallation.equalTo("user", order.get("client"));

                Parse.Push.send({
                    where: queryInstallation,
                    data: {
                        alert: "Courier arrived confirm"
                    }
                }, {
                    success: function () {

                    },
                    error: function (error) {

                        response.error(error);
                    }

                });

                response.success(order);


            }, function (error) {

                response.error(error);

            });
    });

});

Parse.Cloud.define("clientNotAppear", function (request, response) {

    Parse.Config.get().then(function (config) {

        var ORDER_STATE_ARRIVED_WAIT_CLIENT = config.get("ORDER_STATE_ARRIVED_WAIT_CLIENT");
        var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
        var ORDER_STATE_ORDER_DELIVERY = config.get("ORDER_STATE_ORDER_DELIVERY");
        var TIME_TO_WAIT_CLIENT = config.get("TIME_TO_WAIT_CLIENT");
        var ORDER_STATE_DELIVERY_FAIL = config.get("ORDER_STATE_DELIVERY_FAIL");
        var COURIER_STATE_OFF = config.get("COURIER_STATE_OFF");
        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
        var Order = Parse.Object.extend("Order");

        var order = new Order();

        var position = request.params.position;
        order.id = request.params.order;

        var queryOrder = new Parse.Query(Order);

        queryOrder.include("client");
        queryOrder.include("courier");
        queryOrder.include("courier.courier");
        queryOrder.include("courier.courier.privateData");
        queryOrder.include("client.client");
        queryOrder.include("clientData");
        queryOrder.include("client.client.courierData");

        queryOrder.get(order.id).then(
            function (orderResult) {
                order = orderResult;

                if (order.get("courier").id != request.user.id) {
                    throw "Not a courier";

                } else if (order.get("state") != ORDER_STATE_ARRIVED_WAIT_CLIENT) {

                    throw "Incorrect state";
                }

                var queryUpdate = new Parse.Query(OrderStateUpdate);

                queryUpdate.equalTo("order", order);
                queryUpdate.ascending("date");

                return queryUpdate.first();
            }).then(
            function (update) {

                var actualDate = new Date();
                var dateSendPhoto = update.get("date");
                var diffMs = (actualDate - dateSendPhoto);

                if (diffMs < TIME_TO_WAIT_CLIENT) {

                    throw "Wait more time";
                }

                order.set("state", ORDER_STATE_DELIVERY_FAIL);

                return order.save();
            }).then(
            function (orderSaved) {

                var update = new OrderStateUpdate();

                update.set("state", ORDER_STATE_DELIVERY_FAIL);
                update.set("location", position);
                update.set("date", new Date());
                update.set("order", order);
                update.set("seen", false);

                update.save();

                var userClient = order.get("client");
                var client = order.get("client").get("client");
                var courierClient = order.get("client").get("client").get("courierData");
                var courier = order.get("courier");

                var aclUserClient = userClient.getACL();

                aclUserClient.setReadAccess(courier, false);
                aclUserClient.setWriteAccess(courier, false);
                userClient.setACL(aclUserClient);

                var aclClient = client.getACL();

                aclClient.setReadAccess(courier, false);
                aclClient.setWriteAccess(courier, false);
                client.setACL(aclClient);

                var aclCourierClient = courierClient.getACL();

                aclCourierClient.setReadAccess(courier, false);
                aclCourierClient.setWriteAccess(courier, false);
                courierClient.setACL(aclCourierClient);

                courier.get("courier").get("privateData").set("state", COURIER_STATE_OFF);
                courier.get("courier").get("privateData").save();

                courierClient.save();
                client.save();
                userClient.save();

                courier.save();

                var queryInstallation = new Parse.Query(Parse.Installation);

                queryInstallation.equalTo("user", order.get("client"));

                Parse.Push.send({
                    where: queryInstallation,
                    data: {
                        alert: "Courier cancel"
                    }
                }, {
                    success: function () {

                    },
                    error: function (error) {

                        response.error(error);
                    }

                });

                response.success(order);


            }, function (error) {

                response.error(error);

            });
    });

});


Parse.Cloud.define("clientNotAppearClientResponse", function (request, response) {


    Parse.Config.get().then(function (config) {

        var ORDER_STATE_ARRIVED_WAIT_CLIENT = config.get("ORDER_STATE_ARRIVED_WAIT_CLIENT");
        var ORDER_STATE_ARRIVED = config.get("ORDER_STATE_ARRIVED");
        var ORDER_STATE_ORDER_DELIVERY = config.get("ORDER_STATE_ORDER_DELIVERY");
        var TIME_TO_WAIT_CLIENT = config.get("TIME_TO_WAIT_CLIENT");
        var ORDER_STATE_DELIVERY_FAIL = config.get("ORDER_STATE_DELIVERY_FAIL");
        var ORDER_STATE_DELIVERY_FAIL_CLIENT = config.get("ORDER_STATE_DELIVERY_FAIL_CLIENT");
        var ORDER_STATE_DELIVERY_FAIL_ANALYZE = config.get("ORDER_STATE_DELIVERY_FAIL_ANALYZE");
        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");
        var Order = Parse.Object.extend("Order");

        var order = new Order();

        order.id = request.params.order;
        var clientResponse = request.params.clientResponse;

        var queryOrder = new Parse.Query(Order);

        queryOrder.include("client");
        queryOrder.include("courier");
        queryOrder.include("client.client");
        queryOrder.include("clientData");

        queryOrder.get(order.id).then(
            function (orderResult) {
                order = orderResult;

                if (order.get("client").id != request.user.id) {
                    throw "Not a client";

                } else if (order.get("state") != ORDER_STATE_DELIVERY_FAIL) {

                    throw "Incorrect state";
                }

                if (clientResponse == undefined) {

                    order.set("state", ORDER_STATE_DELIVERY_FAIL_CLIENT);

                } else {

                    order.set("state", ORDER_STATE_DELIVERY_FAIL_ANALYZE);
                    order.get("clientData").set("clientJustification", clientResponse);
                    order.get("clientData").save();
                }

                return order.save();

            }).then(
            function(orderSave) {


                response.success(orderSave);

            }, function (error) {

                response.error(error);

            });

    });

});