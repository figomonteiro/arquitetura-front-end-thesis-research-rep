/**
 *
 * Registration of polymer add-to-chart-view custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('add-to-chart-view', {
    attached() {
        if (this.itemToEdit != undefined) {
            this.product = this.itemToEdit.product
            this.$.quantityInput.value = this.itemToEdit.quantity
            this.$.observationsInput.value = this.itemToEdit.observations
        }
    },
    closePopupClicked() {
        this.asyncFire('close-popup-clicked')
    },
    addProductClicked() {
        if (this.itemToEdit != undefined) {
            this.itemToEdit.quantity = this.$.quantityInput.value
            this.itemToEdit.observations = this.$.observationsInput.value

            this.asyncFire('close-popup-clicked')
        } else {
            this.collections.chartCollection.createObject({
                product: {
                    objectId: this.product.objectId,
                    name: this.product.name,
                    price: this.product.price,
                    restaurant: this.product.restaurant,
                    IVA: this.product.IVA,
                    category: this.product.category
                },
                quantity: this.$.quantityInput.value,
                observations: this.$.observationsInput.value
            }).upload()

            this.asyncFire('close-popup-clicked')
        }
    }
})