/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer add-to-chart-view custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('add-to-chart-view', {
	    attached: function attached() {
	        if (this.itemToEdit != undefined) {
	            this.product = this.itemToEdit.product;
	            this.$.quantityInput.value = this.itemToEdit.quantity;
	            this.$.observationsInput.value = this.itemToEdit.observations;
	        }
	    },
	    closePopupClicked: function closePopupClicked() {
	        this.asyncFire('close-popup-clicked');
	    },
	    addProductClicked: function addProductClicked() {
	        if (this.itemToEdit != undefined) {
	            this.itemToEdit.quantity = this.$.quantityInput.value;
	            this.itemToEdit.observations = this.$.observationsInput.value;
	
	            this.asyncFire('close-popup-clicked');
	        } else {
	            this.collections.chartCollection.createObject({
	                product: {
	                    objectId: this.product.objectId,
	                    name: this.product.name,
	                    price: this.product.price,
	                    restaurant: this.product.restaurant,
	                    IVA: this.product.IVA,
	                    category: this.product.category
	                },
	                quantity: this.$.quantityInput.value,
	                observations: this.$.observationsInput.value
	            }).upload();
	
	            this.asyncFire('close-popup-clicked');
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map