/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer order-status custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	var orderStatusArray = ["Waiting for a courier.", "We found a courier for your order.", "The courier has changed the order. Accept or reject the changes.", "Your order has been corfirmed.", "The courier is now heading to the restaurant.", "The courier has arrived the restaurant.", "The courier has changed the order. He's now at the restaurant waiting for your confirmation.", "Order confirmed at the restaurant.", "Order confirmed at the restaurant.", "The courier is heading the delivery location.", "Courier has arrived the delivery location. Your meal is waiting for you!", "The courier is not seeing you. Check the photo of the location he sent you and go meet him.", "The order has been delivered!", "The order has failed.", "Order failure is being investigated.", "Order failure was your responsibility. We sill have to charge you its cost.", "Order failure was your courier's fault. You won't be charged for any costs.", "We couldn't find a courier for your delivery.", "You rejected the changes made to the order, so it was canceled.", "Waiting for a courier...", "Order canceled.", "The restaurant was closed. Your order was canceled.", "You have canceled this order."];
	Polymer("order-status", {
	    statusChanged: function statusChanged() {
	        this.statusMessage = orderStatusArray[parseInt(this.status)];
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map