/**
 *
 * Registration of polymer restaurant-explorer-view custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('restaurant-explorer-view', {
    selectedRestaurant: null,
    selectedProduct: null,
    attached() {
        this.restaurantClicked = (event) => {
            this.selectedRestaurant = event.detail
            this.$.pagerTile.selectedPage = 1
        }
        //this.productClicked = (event) => {
        //    this.selectedProduct = event.detail
        //    this.$.pagerTile.selectedPage = 2
        //}
        this.viewMoreOfCategoryClicked = (event) => {
            this.selectedCategory = event.detail
            this.$.pagerTile.selectedPage = 2
        }
    }
})