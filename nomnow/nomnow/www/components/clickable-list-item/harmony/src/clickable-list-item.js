/**
 *
 * Registration of polymer clickable-list-item custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('clickable-list-item', {
    fireClickEvent() {
        this.asyncFire('list-item-clicked', this.model)
    }
})