/**
 *
 * Registration of polymer nomnow-message custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('nomnow-message', {
    close() {
        this.asyncFire('popup-message-close')
    }
})