/**
 *
 * Registration of polymer order-explorer-view custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('order-explorer-view', {
    attached() {
        this.orderClicked = (event) => {
            this.selectedOrder = event.detail
            this.$.pagerTile.selectedPage = 1
        }
        this.viewOrderProductsClicked = (event) => {
            this.$.pagerTile.selectedPage = 2
        }
    },
    setPricePrecision: function(value) {
        if (value) {
            return parseFloat(value).toFixed(2)
        }
    }
})