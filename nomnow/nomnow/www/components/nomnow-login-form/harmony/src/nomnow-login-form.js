/**
 *
 * Registration of polymer nomnow-login-form custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('nomnow-login-form', {
    signInWithFacebook() {
        this.fire('facebook-sign-in-tap')
    },
    skipSignIn() {
        this.fire('skip-sign-in-tap')
    }
})