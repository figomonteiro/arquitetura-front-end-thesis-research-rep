/**
 *
 * Registration of polymer pager-tile custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('pager-tile', {
    selectedPage: 0,
    currentSection: {},
    currentTileTitle: "placeholder",
    attached() {
        this.currentTileTitle = this.children[this.selectedPage].title
    },
    observe: {
        'currentSection.title': 'updateTileTitle'
    },
    selectedPageChanged() {
        this.currentSection = this.children[this.selectedPage]
    },
    updateTileTitle() {
        this.currentTileTitle = this.currentSection.title
    },
    back() {
        if (this.selectedPage != 0) {
            this.selectedPage--
        }
    },
    closeButtonClicked() {
        this.asyncFire('close-button-clicked')
    },
    nextButtonClicked() {
        this.asyncFire('next-button-clicked')
    }
})