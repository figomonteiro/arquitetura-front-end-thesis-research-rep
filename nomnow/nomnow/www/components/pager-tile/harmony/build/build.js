/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer pager-tile custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('pager-tile', {
	    selectedPage: 0,
	    currentSection: {},
	    currentTileTitle: 'placeholder',
	    attached: function attached() {
	        this.currentTileTitle = this.children[this.selectedPage].title;
	    },
	    observe: {
	        'currentSection.title': 'updateTileTitle'
	    },
	    selectedPageChanged: function selectedPageChanged() {
	        this.currentSection = this.children[this.selectedPage];
	    },
	    updateTileTitle: function updateTileTitle() {
	        this.currentTileTitle = this.currentSection.title;
	    },
	    back: function back() {
	        if (this.selectedPage != 0) {
	            this.selectedPage--;
	        }
	    },
	    closeButtonClicked: function closeButtonClicked() {
	        this.asyncFire('close-button-clicked');
	    },
	    nextButtonClicked: function nextButtonClicked() {
	        this.asyncFire('next-button-clicked');
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map