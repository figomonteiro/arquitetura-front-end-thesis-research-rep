/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer restaurant-category-preview custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('restaurant-category-preview', {
	    attached: function attached() {
	        var _this = this;
	
	        this.productClicked = function (event) {
	            _this.asyncFire('add-product-to-chart', event.detail);
	        };
	    },
	    productsChanged: function productsChanged() {
	        if (this.condensed != undefined) {
	            this.filteredProducts = this.products.slice(0, 3);
	        } else {
	            this.filteredProducts = this.products;
	        }
	    },
	    viewMoreClicked: function viewMoreClicked() {
	        this.asyncFire('category-view-more-clicked', this.category);
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map