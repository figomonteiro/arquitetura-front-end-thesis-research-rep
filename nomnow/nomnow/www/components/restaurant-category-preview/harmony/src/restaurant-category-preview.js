/**
 *
 * Registration of polymer restaurant-category-preview custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('restaurant-category-preview', {
    attached() {
        this.productClicked = (event) => {
            this.asyncFire('add-product-to-chart', event.detail)
        }
    },
    productsChanged() {
        if (this.condensed != undefined) {
            this.filteredProducts = this.products.slice(0, 3)
        } else {
            this.filteredProducts = this.products
        }
    },
    viewMoreClicked() {
        this.asyncFire('category-view-more-clicked', this.category)
    }
})