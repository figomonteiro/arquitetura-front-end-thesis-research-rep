/**
 *
 * Registration of polymer nomnow-data-layer custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let collections
Polymer('nomnow-data-layer', {
    attached() {
        this.adapter = this.$.hoodieParseAdapterPlugin
    },
    collectionsArrayChanged() {
        if (!collections) {
            collections = {}
            for (let index = 0; index < this.collectionsArray.length; index++) {
                collections[this.collectionsArray[index].id] = this.collectionsArray[index]
            }
        }
        this.collections = collections
    }
})