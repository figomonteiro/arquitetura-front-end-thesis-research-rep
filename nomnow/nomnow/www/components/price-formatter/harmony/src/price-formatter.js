/**
 *
 * Registration of polymer price-formatter custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('price-formatter', {
    setPricePrecision: function(value) {
        if (value) {
            return parseFloat(value).toFixed(2)
        }
    }
})