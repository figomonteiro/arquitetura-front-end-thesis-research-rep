/**
 *
 * Registration of polymer global-theme-values custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
const valueTypesArray = ['color', 'url', 'src', 'dimension']
let themeValues = {}

function getNameAttribute(element) {
    if (!element.attributes || !element.attributes.name) {
        throw new Error('global-theme-color must have a name attribute.')
    }
    return element.attributes.name.value
}

Polymer('global-theme-values', {
	created() {
        let children, nameAttribute

        for (let valueType of valueTypesArray) {
            if ((children = this.getElementsByTagName(`global-theme-${valueType}`)).length > 0) {
                // Parse child nodes for style values
                themeValues[valueType] = {}

                for (let index = 0; index < children.length; index++) {
                    if (themeValues.hasOwnProperty((nameAttribute = getNameAttribute(children[index])))) {
                        throw new Error(`Duplicated value ${nameAttribute} in global-theme-values.`)
                    }
                    themeValues[valueType][nameAttribute] = (valueType == 'url')
                        ? `url(${children[index].textContent})`
                        : children[index].textContent
                }
            }
        }
        this.themeValues = themeValues
    }
})