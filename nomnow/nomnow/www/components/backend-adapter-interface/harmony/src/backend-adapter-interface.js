/**
 *
 * Registration of polymer backend-adapter-interface custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 *
 * Interface for the backend-adapter custom element
 */
Polymer('backend-adapter-interface', {
    store: {
        add() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        find() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        findOrAdd() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        findAll() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        update() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        updateAll() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        remove() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        removeAll() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },

        onAdd() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        onItemUpdated() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        onUpdate() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        onItemRemoved() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        },
        onRemove() {
            throw new Error("Method from backend-adapter-interface not implemented.")
        }
    }
})
