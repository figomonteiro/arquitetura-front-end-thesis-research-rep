/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer backend-adapter-interface custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 *
	 * Interface for the backend-adapter custom element
	 */
	"use strict";
	
	Polymer("backend-adapter-interface", {
	    store: {
	        add: function add() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        find: function find() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        findOrAdd: function findOrAdd() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        findAll: function findAll() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        update: function update() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        updateAll: function updateAll() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        remove: function remove() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        removeAll: function removeAll() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	
	        onAdd: function onAdd() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        onItemUpdated: function onItemUpdated() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        onUpdate: function onUpdate() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        onItemRemoved: function onItemRemoved() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        },
	        onRemove: function onRemove() {
	            throw new Error("Method from backend-adapter-interface not implemented.");
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map