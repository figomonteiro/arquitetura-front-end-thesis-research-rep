/**
 *
 * Registration of polymer nomnow-content-container custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('nomnow-content-container', {
	desktopScreen: false
})