/**
 *
 * Registration of polymer restaurant-schedule custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let twoDigits = (n) => {
    return n > 9 ? "" + n: "0" + n;
}
Polymer('restaurant-schedule', {
    openingDateISOChanged() {
        this.openingDate = new Date(this.openingDateISO)
        if (this.closingDate) {
            this.updateStatusText()
        }
    },
    closingDateISOChanged() {
        this.closingDate = new Date(this.closingDateISO)
        if (this.openingDate) {
            this.updateStatusText()
        }
    },
    updateStatusText() {
        this.openingHour = `${twoDigits(this.openingDate.getHours())}:${twoDigits(this.openingDate.getMinutes())}`
        this.closingHour = `${twoDigits(this.closingDate.getHours())}:${twoDigits(this.closingDate.getMinutes())}`
    }
})