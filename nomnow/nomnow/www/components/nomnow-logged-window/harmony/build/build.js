/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer nomnow-logged-window custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	var Parse = undefined;
	
	function generateValidationPIN() {
	    var lowerBound = 1000,
	        upperBound = 9999;
	    return lowerBound + Math.random() * 4294967296 % (upperBound - lowerBound);
	}
	
	Polymer('nomnow-logged-window', {
	    attached: function attached() {
	        this.chartView = this.$.chartView;
	    },
	    adapterChanged: function adapterChanged() {
	        if (!Parse) {
	            Parse = this.adapter.hoodieAdapter.adapter.parse.Parse;
	        }
	    },
	    addProductToChartPopup: function addProductToChartPopup(event) {
	        if (this.chartItemsList.length > 0 && this.chartItemsList[0].product.restaurant.objectId != event.detail.restaurant.objectId) {
	            // Show error message on trying to add a product to the chart from a different
	            // restaurant than the other product already added to the chart
	            return console.log('Different restaurants.');
	        }
	        this.addProductToChart = event.detail;
	    },
	    closeAddProductPopup: function closeAddProductPopup() {
	        this.addProductToChart = undefined;
	        this.editChartItem = undefined;
	    },
	    editChartItemPopup: function editChartItemPopup(event) {
	        this.editChartItem = event.detail;
	    },
	    checkoutPopup: function checkoutPopup(event) {
	        this.chartItems = event.detail;
	        this.showCheckoutPopup = true;
	    },
	    closeCheckoutPopup: function closeCheckoutPopup() {
	        this.showCheckoutPopup = false;
	    },
	    appMessagePopup: function appMessagePopup(event) {
	        this.appMessage = event.detail.message;
	    },
	    closePopupMessage: function closePopupMessage() {
	        this.appMessage = undefined;
	    },
	    estimateOrderParams: function estimateOrderParams(event) {
	        var self = this,
	            checkoutView = event.target;
	
	        Parse.Cloud.run('calcEstimatedPriceAndTime', event.detail, {
	            success: function success(result) {
	                checkoutView.showConfirmationPage(result[0], result[1], result[2], result[3]);
	            },
	            error: function error(_error) {
	                self.closeCheckoutPopup();
	                self.appMessagePopup({
	                    detail: {
	                        message: 'No couriers available for the selected restaurant'
	                    }
	                });
	            }
	        });
	    },
	    processOrder: function processOrder(event) {
	        var checkoutView, orderParams, deliveryLocation, aclPermissions, Restaurant, restaurant, OrderClientData, orderClientData, Order, order, Product, OrderProduct, orderProductsArray, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, chartItem, product, orderProduct;
	
	        return regeneratorRuntime.async(function processOrder$(context$1$0) {
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    checkoutView = event.target;
	                    orderParams = event.detail;
	                    deliveryLocation = new Parse.GeoPoint(orderParams.location);
	                    aclPermissions = new Parse.ACL(Parse.User.current());
	                    Restaurant = Parse.Object.extend('Restaurant');
	                    restaurant = new Restaurant();
	
	                    restaurant.id = orderParams.restaurant.objectId;
	
	                    OrderClientData = Parse.Object.extend('OrderClientData');
	                    orderClientData = new OrderClientData();
	
	                    orderClientData.setACL(aclPermissions);
	                    orderClientData.set('validationPIN', generateValidationPIN());
	
	                    Order = Parse.Object.extend('Order');
	                    order = new Order();
	
	                    order.setACL(aclPermissions);
	                    order.set('state', 19); // Use other element to store constant values
	                    order.set('orderDate', new Date());
	                    order.set('deliveryAddress', orderParams.address.street);
	                    order.set('client', Parse.User.current());
	                    order.set('aceptedByClient', true);
	                    order.set('deliveryLocation', deliveryLocation);
	                    order.set('deliveryDoorNumber', orderParams.address.doorNumber);
	                    order.set('deliveryZipCode', orderParams.address.zipCode);
	                    order.set('pay', false);
	                    order.set('deliveryObservations', orderParams.observations);
	                    order.set('deliveryFloor', orderParams.address.floor);
	                    order.set('clientData', orderClientData);
	                    order.set('restaurant', restaurant);
	
	                    context$1$0.prev = 27;
	                    context$1$0.next = 30;
	                    return order.save();
	
	                case 30:
	                    Product = Parse.Object.extend('Product'), OrderProduct = Parse.Object.extend('OrderProduct'), orderProductsArray = [];
	                    _iteratorNormalCompletion = true;
	                    _didIteratorError = false;
	                    _iteratorError = undefined;
	                    context$1$0.prev = 34;
	
	                    for (_iterator = orderParams.orderItems[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                        chartItem = _step.value;
	                        product = new Product(), orderProduct = new OrderProduct();
	
	                        product.id = chartItem.product.objectId;
	                        orderProduct.setACL(aclPermissions);
	                        orderProduct.set('order', order);
	                        orderProduct.set('quantity', parseInt(chartItem.quantity));
	                        orderProduct.set('observations', chartItem.observations);
	                        orderProduct.set('realPrice', parseInt(chartItem.product.price));
	                        orderProduct.set('product', product);
	
	                        orderProductsArray.push(orderProduct);
	                    }
	
	                    context$1$0.next = 42;
	                    break;
	
	                case 38:
	                    context$1$0.prev = 38;
	                    context$1$0.t0 = context$1$0['catch'](34);
	                    _didIteratorError = true;
	                    _iteratorError = context$1$0.t0;
	
	                case 42:
	                    context$1$0.prev = 42;
	                    context$1$0.prev = 43;
	
	                    if (!_iteratorNormalCompletion && _iterator['return']) {
	                        _iterator['return']();
	                    }
	
	                case 45:
	                    context$1$0.prev = 45;
	
	                    if (!_didIteratorError) {
	                        context$1$0.next = 48;
	                        break;
	                    }
	
	                    throw _iteratorError;
	
	                case 48:
	                    return context$1$0.finish(45);
	
	                case 49:
	                    return context$1$0.finish(42);
	
	                case 50:
	                    context$1$0.next = 52;
	                    return Parse.Object.saveAll(orderProductsArray);
	
	                case 52:
	                    order.set('products', orderProductsArray);
	                    context$1$0.next = 55;
	                    return order.save();
	
	                case 55:
	                    context$1$0.next = 57;
	                    return Parse.Cloud.run('findEligibleCouriers', { order: order.id });
	
	                case 57:
	
	                    this.closeCheckoutPopup();
	                    this.chartView.emptyChart();
	                    this.appMessagePopup({
	                        detail: {
	                            message: 'Order confirmed! You can check its status on the left column in the app.'
	                        }
	                    });
	                    context$1$0.next = 65;
	                    break;
	
	                case 62:
	                    context$1$0.prev = 62;
	                    context$1$0.t1 = context$1$0['catch'](27);
	
	                    this.appMessagePopup({
	                        detail: {
	                            message: 'An error occurred trying to register your order.'
	                        }
	                    });
	
	                case 65:
	                case 'end':
	                    return context$1$0.stop();
	            }
	        }, null, this, [[27, 62], [34, 38, 42, 50], [43,, 45, 49]]);
	    }
	});
	
	// Create order remotely

	// After order is successfully saved

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map