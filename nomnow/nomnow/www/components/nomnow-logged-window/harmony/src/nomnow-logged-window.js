/**
 *
 * Registration of polymer nomnow-logged-window custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let Parse

function generateValidationPIN() {
    let lowerBound = 1000,
        upperBound = 9999
    return lowerBound + Math.random() * 4294967296 % (upperBound - lowerBound)
}

Polymer('nomnow-logged-window', {
    attached() {
        this.chartView = this.$.chartView
    },
    adapterChanged() {
        if (!Parse) {
            Parse = this.adapter.hoodieAdapter.adapter.parse.Parse
        }
    },
    addProductToChartPopup(event) {
        if (this.chartItemsList.length > 0
            && this.chartItemsList[0].product.restaurant.objectId != event.detail.restaurant.objectId) {
            // Show error message on trying to add a product to the chart from a different
            // restaurant than the other product already added to the chart
            return console.log("Different restaurants.")
        }
        this.addProductToChart = event.detail
    },
    closeAddProductPopup() {
        this.addProductToChart = undefined
        this.editChartItem = undefined
    },
    editChartItemPopup(event) {
        this.editChartItem = event.detail
    },
    checkoutPopup(event) {
        this.chartItems = event.detail
        this.showCheckoutPopup = true
    },
    closeCheckoutPopup() {
        this.showCheckoutPopup = false
    },
    appMessagePopup(event) {
        this.appMessage = event.detail.message
    },
    closePopupMessage() {
        this.appMessage = undefined
    },
    estimateOrderParams(event) {
        let self = this,
            checkoutView = event.target

        Parse.Cloud.run('calcEstimatedPriceAndTime', event.detail, {
            success: (result) => {
                checkoutView.showConfirmationPage(result[0], result[1], result[2], result[3])
            },
            error: (error) => {
                self.closeCheckoutPopup()
                self.appMessagePopup({
                    detail: {
                        message: 'No couriers available for the selected restaurant'
                    }
                })
            }
        })
    },
    async processOrder(event) {
        let checkoutView = event.target
        let orderParams = event.detail
        let deliveryLocation = new Parse.GeoPoint(orderParams.location)

        // Create order remotely
        let aclPermissions = new Parse.ACL(Parse.User.current())

        let Restaurant = Parse.Object.extend("Restaurant")
        let restaurant = new Restaurant()
        restaurant.id = orderParams.restaurant.objectId

        let OrderClientData = Parse.Object.extend("OrderClientData")
        let orderClientData = new OrderClientData()
        orderClientData.setACL(aclPermissions)
        orderClientData.set('validationPIN', generateValidationPIN())

        let Order = Parse.Object.extend("Order")
        let order = new Order()
        order.setACL(aclPermissions)
        order.set('state', 19) // Use other element to store constant values
        order.set('orderDate', new Date())
        order.set('deliveryAddress', orderParams.address.street)
        order.set('client', Parse.User.current())
        order.set('aceptedByClient', true)
        order.set('deliveryLocation', deliveryLocation)
        order.set('deliveryDoorNumber', orderParams.address.doorNumber)
        order.set('deliveryZipCode', orderParams.address.zipCode)
        order.set('pay', false)
        order.set('deliveryObservations', orderParams.observations)
        order.set('deliveryFloor', orderParams.address.floor)
        order.set('clientData', orderClientData)
        order.set('restaurant', restaurant)

        try {
            await order.save()

            // After order is successfully saved
            let Product = Parse.Object.extend("Product"),
                OrderProduct = Parse.Object.extend("OrderProduct"),
                orderProductsArray = []
            for (let chartItem of orderParams.orderItems) {
                let product = new Product(),
                    orderProduct = new OrderProduct()

                product.id = chartItem.product.objectId
                orderProduct.setACL(aclPermissions)
                orderProduct.set('order', order)
                orderProduct.set('quantity', parseInt(chartItem.quantity))
                orderProduct.set('observations', chartItem.observations)
                orderProduct.set('realPrice', parseInt(chartItem.product.price))
                orderProduct.set('product', product)

                orderProductsArray.push(orderProduct)
            }

            await Parse.Object.saveAll(orderProductsArray)
            order.set('products', orderProductsArray)
            await order.save()
            await Parse.Cloud.run('findEligibleCouriers', {order: order.id})

            this.closeCheckoutPopup()
            this.chartView.emptyChart()
            this.appMessagePopup({
                detail: {
                    message: 'Order confirmed! You can check its status on the left column in the app.'
                }
            })
        } catch(error) {
            this.appMessagePopup({
                detail: {
                    message: 'An error occurred trying to register your order.'
                }
            })
        }
    }
})