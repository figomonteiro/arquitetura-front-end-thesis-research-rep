/**
 *
 * Registration of polymer chart-view custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('chart-view', {
    attached() {
        this.productClicked = (event) => {
            this.asyncFire('edit-chart-item', event.detail)
        }
        this.updateTotalPrice = () => {
            this.totalPrice = 0
            for (let item of this.chartItems) {
                this.totalPrice += item.product.price * item.quantity
            }
        }
        this.checkout = () => {
            this.asyncFire('chart-checkout', this.chartItems)
        }
    },
    removeProduct(event) {
        event.detail.delete()
        event.stopImmediatePropagation()
    },
    chartItemsChanged() {
        this.updateTotalPrice()
    },
    emptyChart() {
        this.collections.chartCollection.removeAll()
    }
})