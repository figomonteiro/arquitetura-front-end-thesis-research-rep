/**
 *
 * Registration of polymer ripple-button custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('ripple-button', {
    publish: {
        text: "BUTTON"
    }
})