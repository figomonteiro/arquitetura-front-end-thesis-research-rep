/**
 *
 * Registration of polymer nomnow-header custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('nomnow-header', {
    fireSignOutEvent() {
        this.asyncFire('sign-out')
    }
})