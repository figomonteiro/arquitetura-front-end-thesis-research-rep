import Model from "./models/Model"
/**
 * Registration of polymer backend-collection custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */

// Let every collection be a singleton
let registeredCollections = {}

let checkRequiredAttributes = (name, backendAdapter, backendAdapterInterface) => {
    if (!name)
        throw new Error("backend-collection must receive a name attribute.")
    if (!backendAdapter)
        throw new Error("backend-collection must receive a backendAdapter attribute.")
    else if (backendAdapter.__proto__.__proto__ != backendAdapterInterface.__proto__)
        throw new Error("backendAdapter must implement BackendAdapterInterface")
}
// Auxiliary function used to get nested values on an object (Used to create query functions)
function getNestedFieldValue(field, object) {
    let nestedPropsArray = field.split('.'),
        value = object
    for (let prop of nestedPropsArray) {
        value = value[prop]
    }
    return value
}
// Operator helpers to use in query function construction
let queryOperators = {
    greaterThan(a, b) {
        return a > b
    },
    lessThan(a, b) {
        return a < b
    },
    equals(a, b) {
        return a == b
    },
    different(a, b) {
        return a != b
    }
}
// backend-collection element registration
Polymer('backend-collection', {
    collection: "model",
    requiredProperties: [],
    modelClass: {},
    attached() {
        checkRequiredAttributes(this.name, this.backendAdapter, this.$.backendAdapterInterface)
        this.collection = this.name
        // Check if collection is already registered
        if (registeredCollections[this.collection]
            && registeredCollections[this.collection].backendAdapter.isEqualNode(this.backendAdapter)) {
            this.modelClass = registeredCollections[this.collection].modelClass
            return
        }

        // If not already registered
        if (this.requiredProperties.length > 0) {
            this.requiredProperties = this.requiredProperties.split(' ')
        }
        let self = this
        @Model.Required(self.requiredProperties)
        @Model.Collection(self.collection)
        class GenericModel extends Model {
            constructor(backendAdapter, object) {
                super(backendAdapter, object)
            }
        }
        this.modelClass = GenericModel
        // Register collection as a singleton
        registeredCollections[this.collection] = {
            backendAdapter: this.backendAdapter,
            modelClass: this.modelClass
        }
    },
    createObject(object) {
        return new this.modelClass(this.backendAdapter, object)
    },
    onItemAddedHandler(event) {
        this.createObject(event.detail).upload()
    },
    onItemDeletedHandler(event) {
        event.detail.delete()
    },
    removeAll() {
        this.modelClass.removeAll(this.backendAdapter)
    }
})

// backend-collection-query element registration
Polymer('backend-collection-query', {
    created() {
        this.queryParams = []
    },
    async collectionChanged() {
        if (this.findById != undefined || this.children.length > 0) {
            this.queuedQuery && this.queuedQuery()
            return this.queuedQuery = undefined
        }
        // Query all if this element is not given an objectId attribute
        this.queryFindMany()
    },
    async queryFindMany(queryFunction) {
        this.result = await this.collection.modelClass.findAll(this.collection.backendAdapter, queryFunction)
        this.orderResults()

        let self = this
        // Set add and remove event callbacks
        if (this.noPersistence == undefined) {
            this.collection.modelClass.onAddToCollection(
                this.collection.backendAdapter, object => {
                    if (!self.getObjectFromList(object) && (!self.queryFunction || self.queryFunction(object))) {
                        self.result.push(object)
                        self.orderResults()
                        //console.log("ADDED:", object)
                    }
                })
            this.collection.modelClass.onRemoveFromCollection(
                this.collection.backendAdapter, object => {
                    self.removeObjectFromList(object)
                    //console.log("REMOVED", object)
                })
            this.collection.modelClass.onUpdateInCollection(
                this.collection.backendAdapter, object => {
                    if (!self.queryFunction || self.queryFunction(object)) {
                        if (!self.getObjectFromList(object)) {
                            self.result.push(object)
                            self.orderResults()
                        }
                    } else {
                        self.removeObjectFromList(object)
                    }
                }
            )
        }
    },
    orderResults() {
        if (this.orderBy != undefined) {
            let orderBy = this.orderBy,
                descendant = this.descendant
            this.result.sort((a, b) => {
                if (a[orderBy] > b[orderBy]) {
                    return (descendant != undefined) ? -1 : 1
                } else if (a[orderBy] < b[orderBy]) {
                    return (descendant != undefined) ? 1 : -1
                }
                return 0
            })
        }
    },
    removeObjectFromList(object) {
        for (let index = 0; index < this.result.length; index++) {
            if (this.result[index].id == object.id) {
                this.result.splice(index, 1)
            }
        }
        this.orderResults()
    },
    getObjectFromList(object) {
        for (let instance of this.result)
            if (instance.id == object.id)
                return instance;
        return null;
    },
    async objectIdChanged() {
        if (this.findById != undefined) {
            let query = async () => {
                return this.result = await this.collection.modelClass.find(
                    this.collection.backendAdapter, this.objectId
                )
            }

            if (!this.collection) {
                this.queuedQuery = query
            } else {
                this.result = await query()
            }
        }
    },
    queryParamsChanged() {
        // Perform query after all the parameters are loaded
        if (this.queryParams.length == this.children.length) {
            let queryParams = this.queryParams
            // Method used to filter the results from the backend adapter method findAll
            this.queryFunction = (object) => {
                let value = true
                for (let param of queryParams) {
                    value = param(value, object)
                }
                return value
            }

            if (!this.collection) {
                this.queuedQuery = () => {
                    this.queryFindMany(this.queryFunction)
                }
            } else {
                this.queryFindMany(this.queryFunction)
            }
            this.queryParams = []
        }
    }
})
// backend-collection-query-param element registration
Polymer('backend-collection-query-param', {
    ready() {
        this.queryParams = []
        this.queryElement = this.parentElement
        if (!this.queryElement || (this.queryElement.tagName != 'BACKEND-COLLECTION-QUERY'
            && this.queryElement.tagName != 'BACKEND-COLLECTION-QUERY-PARAM')) {
            throw new Error('backend-collection-query-param element must be ' +
                'used nested with a backend-collection-query element or with' +
                'a backend-collection-query-param element')
        }
    },
    valueChanged() {
        let self = this,
            queryFunctionPart = (value, object, or) => {
                return (or == undefined)
                    ? value && queryOperators[self.operator](getNestedFieldValue(self.field, object), self.value)
                    : value || queryOperators[self.operator](getNestedFieldValue(self.field, object), self.value)
            }
        this.queryElement.queryParams.push(queryFunctionPart)
    },
    queryParamsChanged() {
        // Perform query after all the parameters are loaded
        if (this.children.length > 0 && this.queryParams.length == this.children.length) {
            let queryParams = this.queryParams
            // Method used to filter the results from the backend adapter method findAll
            let self = this,
                queryFunctionPart = (value, object) => {
                    let queryValue = self.or == undefined
                    for (let param of queryParams) {
                        queryValue = param(queryValue, object, self.or)
                    }
                    return queryValue
                }
            this.queryElement.queryParams.push(queryFunctionPart)
        }
    }
})
