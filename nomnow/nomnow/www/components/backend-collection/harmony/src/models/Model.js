/**
 *
 * Decorator for class Model.
 * @properties Array with all the required properties of a valid object.
 */
let Required = (properties) => {
    return (target) => {
        Object.defineProperty(target, "requiredProperties", {
            get: function() {
                return properties;
            }
        });
    }
}
let Collection = (name) => {
    return (target) => {
        Object.defineProperty(target, "collection", {
            get: function() {
                return name;
            }
        });
    }
}
/**
 *
 * Class Model
 * @required Must be called with an array containing all the required properties of a valid object.
 */
@Required(/* required properties */)
@Collection()
class Model {
    // Initializing the collection name with the name of the class.
    static get collection() {
        return this.name.toLowerCase()
    }
    get collection() {
        return this.constructor.collection
    }

    // Must be called with an object of a class that implements the BackendAdapter interface.
    constructor(backendAdapter, object) {
        this.constructor.validateProperties(object)
        this.backendAdapter = backendAdapter

        this.objectProperties = Object.keys(object)
        for (let key of this.objectProperties) {
            this["_" + key] = object[key]

            Object.defineProperty(this, key, {
                get() {
                    return this["_" + key]
                },
                async set(value) {
                    try {
                        let object = await this.update({[(() => key)() ]: value})
                        this["_" + key] = object[key]
                    } catch (error) {
                        console.log(error)
                    }
                }
            })
        }

        if (object.id) {
            this.onUpdate(object => {
                for (let key of this.objectProperties)
                    this["_" + key] = object[key]
                //console.log("UPDATED:", this)
            })
        }
    }

    static validateProperties(object) {
        for (let prop of this.requiredProperties) {
            if (!object.hasOwnProperty(prop))
                throw new Error("Property " + prop + " missing on creating an instance of " + this.collection)
        }
    }

    // Method used to upload an object to both local and remote databases.
    async upload() {
        let object = {}
        for (let prop of this.objectProperties)
            object[prop] = this[prop]

        return await this.backendAdapter.store.add(this.collection, object)
    }

    update(changes) {
        return this.backendAdapter.store.update(this.collection, this.id, changes)
    }
    delete() {
        return this.backendAdapter.store.remove(this.collection, this.id)
    }


    onUpdate(callback) {
        return this.backendAdapter.store.onItemUpdated(this.collection, this.id, callback)
    }

    onRemove(callback) {
        return this.backendAdapter.store.onItemRemoved(this.collection, this.id, callback)
    }

    static async find(backendAdapter, id) {
        let result = await backendAdapter.store.find(this.collection, id)
        return new this(backendAdapter, result)
    }

    static async findAll(backendAdapter, queryFunction) {
        let results = await backendAdapter.store.findAll(this.collection, queryFunction)
        return [
            for(result of results)
            new this(backendAdapter, result)
            ]
    }

    static updateAll(backendAdapter, changes) {
        return backendAdapter.store.updateAll(this.collection, changes)
    }

    static removeAll(backendAdapter) {
        return backendAdapter.store.removeAll(this.collection)
    }

    //Hoodie database events
    static onAddToCollection(backendAdapter, callback) {
        return backendAdapter.store.onAdd(this.collection,
                result => callback(new this(backendAdapter, result)))
    }

    static onUpdateInCollection(backendAdapter, callback) {
        return backendAdapter.store.onUpdate(this.collection, callback)
    }

    static onRemoveFromCollection(backendAdapter, callback) {
        return backendAdapter.store.onRemove(this.collection, callback)
    }

    static cast(backendAdapter, object) {
        let castedObject = {}
        for (let prop of this.requiredProperties)
            if (object.hasOwnProperty(prop))
                castedObject[prop] = object[prop]
            else
                throw new Error("Property " + prop + " missing on casting an object to " + this.name)

        if (object.id)
            castedObject.id = object.id

        return new this(backendAdapter, castedObject)
    }
}

Model.Required = Required
Model.Collection = Collection

export default Model