/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }
	
	var _modelsModel = __webpack_require__(2);
	
	var _modelsModel2 = _interopRequireDefault(_modelsModel);
	
	/**
	 * Registration of polymer backend-collection custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	
	// Let every collection be a singleton
	var registeredCollections = {};
	
	var checkRequiredAttributes = function checkRequiredAttributes(name, backendAdapter, backendAdapterInterface) {
	    if (!name) throw new Error("backend-collection must receive a name attribute.");
	    if (!backendAdapter) throw new Error("backend-collection must receive a backendAdapter attribute.");else if (backendAdapter.__proto__.__proto__ != backendAdapterInterface.__proto__) throw new Error("backendAdapter must implement BackendAdapterInterface");
	};
	// Auxiliary function used to get nested values on an object (Used to create query functions)
	function getNestedFieldValue(field, object) {
	    var nestedPropsArray = field.split("."),
	        value = object;
	    var _iteratorNormalCompletion = true;
	    var _didIteratorError = false;
	    var _iteratorError = undefined;
	
	    try {
	        for (var _iterator = nestedPropsArray[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var prop = _step.value;
	
	            value = value[prop];
	        }
	    } catch (err) {
	        _didIteratorError = true;
	        _iteratorError = err;
	    } finally {
	        try {
	            if (!_iteratorNormalCompletion && _iterator["return"]) {
	                _iterator["return"]();
	            }
	        } finally {
	            if (_didIteratorError) {
	                throw _iteratorError;
	            }
	        }
	    }
	
	    return value;
	}
	// Operator helpers to use in query function construction
	var queryOperators = {
	    greaterThan: function greaterThan(a, b) {
	        return a > b;
	    },
	    lessThan: function lessThan(a, b) {
	        return a < b;
	    },
	    equals: function equals(a, b) {
	        return a == b;
	    },
	    different: function different(a, b) {
	        return a != b;
	    }
	};
	// backend-collection element registration
	Polymer("backend-collection", {
	    collection: "model",
	    requiredProperties: [],
	    modelClass: {},
	    attached: function attached() {
	        checkRequiredAttributes(this.name, this.backendAdapter, this.$.backendAdapterInterface);
	        this.collection = this.name;
	        // Check if collection is already registered
	        if (registeredCollections[this.collection] && registeredCollections[this.collection].backendAdapter.isEqualNode(this.backendAdapter)) {
	            this.modelClass = registeredCollections[this.collection].modelClass;
	            return;
	        }
	
	        // If not already registered
	        if (this.requiredProperties.length > 0) {
	            this.requiredProperties = this.requiredProperties.split(" ");
	        }
	        var self = this;
	
	        var GenericModel = (function (_Model) {
	            function GenericModel(backendAdapter, object) {
	                _classCallCheck(this, _GenericModel);
	
	                _get(Object.getPrototypeOf(_GenericModel.prototype), "constructor", this).call(this, backendAdapter, object);
	            }
	
	            _inherits(GenericModel, _Model);
	
	            var _GenericModel = GenericModel;
	            GenericModel = _modelsModel2["default"].Collection(self.collection)(GenericModel) || GenericModel;
	            GenericModel = _modelsModel2["default"].Required(self.requiredProperties)(GenericModel) || GenericModel;
	            return GenericModel;
	        })(_modelsModel2["default"]);
	
	        this.modelClass = GenericModel;
	        // Register collection as a singleton
	        registeredCollections[this.collection] = {
	            backendAdapter: this.backendAdapter,
	            modelClass: this.modelClass
	        };
	    },
	    createObject: function createObject(object) {
	        return new this.modelClass(this.backendAdapter, object);
	    },
	    onItemAddedHandler: function onItemAddedHandler(event) {
	        this.createObject(event.detail).upload();
	    },
	    onItemDeletedHandler: function onItemDeletedHandler(event) {
	        event.detail["delete"]();
	    },
	    removeAll: function removeAll() {
	        this.modelClass.removeAll(this.backendAdapter);
	    }
	});
	
	// backend-collection-query element registration
	Polymer("backend-collection-query", {
	    created: function created() {
	        this.queryParams = [];
	    },
	    collectionChanged: function collectionChanged() {
	        return regeneratorRuntime.async(function collectionChanged$(context$1$0) {
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    if (!(this.findById != undefined || this.children.length > 0)) {
	                        context$1$0.next = 3;
	                        break;
	                    }
	
	                    this.queuedQuery && this.queuedQuery();
	                    return context$1$0.abrupt("return", this.queuedQuery = undefined);
	
	                case 3:
	                    // Query all if this element is not given an objectId attribute
	                    this.queryFindMany();
	
	                case 4:
	                case "end":
	                    return context$1$0.stop();
	            }
	        }, null, this);
	    },
	    queryFindMany: function queryFindMany(queryFunction) {
	        var self;
	        return regeneratorRuntime.async(function queryFindMany$(context$1$0) {
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    context$1$0.next = 2;
	                    return this.collection.modelClass.findAll(this.collection.backendAdapter, queryFunction);
	
	                case 2:
	                    this.result = context$1$0.sent;
	
	                    this.orderResults();
	
	                    self = this;
	
	                    // Set add and remove event callbacks
	                    if (this.noPersistence == undefined) {
	                        this.collection.modelClass.onAddToCollection(this.collection.backendAdapter, function (object) {
	                            if (!self.getObjectFromList(object) && (!self.queryFunction || self.queryFunction(object))) {
	                                self.result.push(object);
	                                self.orderResults();
	                            }
	                        });
	                        this.collection.modelClass.onRemoveFromCollection(this.collection.backendAdapter, function (object) {
	                            self.removeObjectFromList(object);
	                        });
	                        this.collection.modelClass.onUpdateInCollection(this.collection.backendAdapter, function (object) {
	                            if (!self.queryFunction || self.queryFunction(object)) {
	                                if (!self.getObjectFromList(object)) {
	                                    self.result.push(object);
	                                    self.orderResults();
	                                }
	                            } else {
	                                self.removeObjectFromList(object);
	                            }
	                        });
	                    }
	
	                case 6:
	                case "end":
	                    return context$1$0.stop();
	            }
	        }, null, this);
	    },
	    orderResults: function orderResults() {
	        var _this = this;
	
	        if (this.orderBy != undefined) {
	            (function () {
	                var orderBy = _this.orderBy,
	                    descendant = _this.descendant;
	                _this.result.sort(function (a, b) {
	                    if (a[orderBy] > b[orderBy]) {
	                        return descendant != undefined ? -1 : 1;
	                    } else if (a[orderBy] < b[orderBy]) {
	                        return descendant != undefined ? 1 : -1;
	                    }
	                    return 0;
	                });
	            })();
	        }
	    },
	    removeObjectFromList: function removeObjectFromList(object) {
	        for (var index = 0; index < this.result.length; index++) {
	            if (this.result[index].id == object.id) {
	                this.result.splice(index, 1);
	            }
	        }
	        this.orderResults();
	    },
	    getObjectFromList: function getObjectFromList(object) {
	        var _iteratorNormalCompletion2 = true;
	        var _didIteratorError2 = false;
	        var _iteratorError2 = undefined;
	
	        try {
	            for (var _iterator2 = this.result[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                var instance = _step2.value;
	
	                if (instance.id == object.id) return instance;
	            }
	        } catch (err) {
	            _didIteratorError2 = true;
	            _iteratorError2 = err;
	        } finally {
	            try {
	                if (!_iteratorNormalCompletion2 && _iterator2["return"]) {
	                    _iterator2["return"]();
	                }
	            } finally {
	                if (_didIteratorError2) {
	                    throw _iteratorError2;
	                }
	            }
	        }
	
	        return null;
	    },
	    objectIdChanged: function objectIdChanged() {
	        var query;
	        return regeneratorRuntime.async(function objectIdChanged$(context$1$0) {
	            var _this2 = this;
	
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    if (!(this.findById != undefined)) {
	                        context$1$0.next = 9;
	                        break;
	                    }
	
	                    query = function query() {
	                        return regeneratorRuntime.async(function query$(context$2$0) {
	                            while (1) switch (context$2$0.prev = context$2$0.next) {
	                                case 0:
	                                    context$2$0.next = 2;
	                                    return this.collection.modelClass.find(this.collection.backendAdapter, this.objectId);
	
	                                case 2:
	                                    return context$2$0.abrupt("return", this.result = context$2$0.sent);
	
	                                case 3:
	                                case "end":
	                                    return context$2$0.stop();
	                            }
	                        }, null, _this2);
	                    };
	
	                    if (this.collection) {
	                        context$1$0.next = 6;
	                        break;
	                    }
	
	                    this.queuedQuery = query;
	                    context$1$0.next = 9;
	                    break;
	
	                case 6:
	                    context$1$0.next = 8;
	                    return query();
	
	                case 8:
	                    this.result = context$1$0.sent;
	
	                case 9:
	                case "end":
	                    return context$1$0.stop();
	            }
	        }, null, this);
	    },
	    queryParamsChanged: function queryParamsChanged() {
	        var _this3 = this;
	
	        // Perform query after all the parameters are loaded
	        if (this.queryParams.length == this.children.length) {
	            (function () {
	                var queryParams = _this3.queryParams;
	                // Method used to filter the results from the backend adapter method findAll
	                _this3.queryFunction = function (object) {
	                    var value = true;
	                    var _iteratorNormalCompletion3 = true;
	                    var _didIteratorError3 = false;
	                    var _iteratorError3 = undefined;
	
	                    try {
	                        for (var _iterator3 = queryParams[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
	                            var param = _step3.value;
	
	                            value = param(value, object);
	                        }
	                    } catch (err) {
	                        _didIteratorError3 = true;
	                        _iteratorError3 = err;
	                    } finally {
	                        try {
	                            if (!_iteratorNormalCompletion3 && _iterator3["return"]) {
	                                _iterator3["return"]();
	                            }
	                        } finally {
	                            if (_didIteratorError3) {
	                                throw _iteratorError3;
	                            }
	                        }
	                    }
	
	                    return value;
	                };
	
	                if (!_this3.collection) {
	                    _this3.queuedQuery = function () {
	                        _this3.queryFindMany(_this3.queryFunction);
	                    };
	                } else {
	                    _this3.queryFindMany(_this3.queryFunction);
	                }
	                _this3.queryParams = [];
	            })();
	        }
	    }
	});
	// backend-collection-query-param element registration
	Polymer("backend-collection-query-param", {
	    ready: function ready() {
	        this.queryParams = [];
	        this.queryElement = this.parentElement;
	        if (!this.queryElement || this.queryElement.tagName != "BACKEND-COLLECTION-QUERY" && this.queryElement.tagName != "BACKEND-COLLECTION-QUERY-PARAM") {
	            throw new Error("backend-collection-query-param element must be " + "used nested with a backend-collection-query element or with" + "a backend-collection-query-param element");
	        }
	    },
	    valueChanged: function valueChanged() {
	        var self = this,
	            queryFunctionPart = function queryFunctionPart(value, object, or) {
	            return or == undefined ? value && queryOperators[self.operator](getNestedFieldValue(self.field, object), self.value) : value || queryOperators[self.operator](getNestedFieldValue(self.field, object), self.value);
	        };
	        this.queryElement.queryParams.push(queryFunctionPart);
	    },
	    queryParamsChanged: function queryParamsChanged() {
	        var _this4 = this;
	
	        // Perform query after all the parameters are loaded
	        if (this.children.length > 0 && this.queryParams.length == this.children.length) {
	            (function () {
	                var queryParams = _this4.queryParams;
	                // Method used to filter the results from the backend adapter method findAll
	                var self = _this4,
	                    queryFunctionPart = function queryFunctionPart(value, object) {
	                    var queryValue = self.or == undefined;
	                    var _iteratorNormalCompletion4 = true;
	                    var _didIteratorError4 = false;
	                    var _iteratorError4 = undefined;
	
	                    try {
	                        for (var _iterator4 = queryParams[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
	                            var param = _step4.value;
	
	                            queryValue = param(queryValue, object, self.or);
	                        }
	                    } catch (err) {
	                        _didIteratorError4 = true;
	                        _iteratorError4 = err;
	                    } finally {
	                        try {
	                            if (!_iteratorNormalCompletion4 && _iterator4["return"]) {
	                                _iterator4["return"]();
	                            }
	                        } finally {
	                            if (_didIteratorError4) {
	                                throw _iteratorError4;
	                            }
	                        }
	                    }
	
	                    return queryValue;
	                };
	                _this4.queryElement.queryParams.push(queryFunctionPart);
	            })();
	        }
	    }
	});
	//console.log("ADDED:", object)
	//console.log("REMOVED", object)

/***/ },
/* 1 */,
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Decorator for class Model.
	 * @properties Array with all the required properties of a valid object.
	 */
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _defineProperty(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Required = function Required(properties) {
	    return function (target) {
	        Object.defineProperty(target, "requiredProperties", {
	            get: function get() {
	                return properties;
	            }
	        });
	    };
	};
	var Collection = function Collection(name) {
	    return function (target) {
	        Object.defineProperty(target, "collection", {
	            get: function get() {
	                return name;
	            }
	        });
	    };
	}
	/**
	 *
	 * Class Model
	 * @required Must be called with an array containing all the required properties of a valid object.
	 */
	;
	var Model = (function () {
	
	    // Must be called with an object of a class that implements the BackendAdapter interface.
	
	    function Model(backendAdapter, object) {
	        var _this = this;
	
	        _classCallCheck(this, _Model);
	
	        this.constructor.validateProperties(object);
	        this.backendAdapter = backendAdapter;
	
	        this.objectProperties = Object.keys(object);
	        var _iteratorNormalCompletion = true;
	        var _didIteratorError = false;
	        var _iteratorError = undefined;
	
	        try {
	            var _loop = function () {
	                var key = _step.value;
	
	                _this["_" + key] = object[key];
	
	                Object.defineProperty(_this, key, {
	                    get: function get() {
	                        return this["_" + key];
	                    },
	                    set: function set(value) {
	                        var _object;
	
	                        return regeneratorRuntime.async(function set$(context$4$0) {
	                            while (1) switch (context$4$0.prev = context$4$0.next) {
	                                case 0:
	                                    context$4$0.prev = 0;
	                                    context$4$0.next = 3;
	                                    return this.update(_defineProperty({}, (function () {
	                                        return key;
	                                    })(), value));
	
	                                case 3:
	                                    _object = context$4$0.sent;
	
	                                    this["_" + key] = _object[key];
	                                    context$4$0.next = 10;
	                                    break;
	
	                                case 7:
	                                    context$4$0.prev = 7;
	                                    context$4$0.t0 = context$4$0["catch"](0);
	
	                                    console.log(context$4$0.t0);
	
	                                case 10:
	                                case "end":
	                                    return context$4$0.stop();
	                            }
	                        }, null, this, [[0, 7]]);
	                    }
	                });
	            };
	
	            for (var _iterator = this.objectProperties[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                _loop();
	            }
	        } catch (err) {
	            _didIteratorError = true;
	            _iteratorError = err;
	        } finally {
	            try {
	                if (!_iteratorNormalCompletion && _iterator["return"]) {
	                    _iterator["return"]();
	                }
	            } finally {
	                if (_didIteratorError) {
	                    throw _iteratorError;
	                }
	            }
	        }
	
	        if (object.id) {
	            this.onUpdate(function (object) {
	                var _iteratorNormalCompletion2 = true;
	                var _didIteratorError2 = false;
	                var _iteratorError2 = undefined;
	
	                try {
	                    for (var _iterator2 = _this.objectProperties[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                        var key = _step2.value;
	
	                        _this["_" + key] = object[key]
	                        //console.log("UPDATED:", this)
	                        ;
	                    }
	                } catch (err) {
	                    _didIteratorError2 = true;
	                    _iteratorError2 = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion2 && _iterator2["return"]) {
	                            _iterator2["return"]();
	                        }
	                    } finally {
	                        if (_didIteratorError2) {
	                            throw _iteratorError2;
	                        }
	                    }
	                }
	            });
	        }
	    }
	
	    var _Model = Model;
	
	    _createClass(_Model, [{
	        key: "collection",
	        get: function () {
	            return this.constructor.collection;
	        }
	    }, {
	        key: "upload",
	
	        // Method used to upload an object to both local and remote databases.
	        value: function upload() {
	            var object, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, prop;
	
	            return regeneratorRuntime.async(function upload$(context$2$0) {
	                while (1) switch (context$2$0.prev = context$2$0.next) {
	                    case 0:
	                        object = {};
	                        _iteratorNormalCompletion3 = true;
	                        _didIteratorError3 = false;
	                        _iteratorError3 = undefined;
	                        context$2$0.prev = 4;
	
	                        for (_iterator3 = this.objectProperties[Symbol.iterator](); !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
	                            prop = _step3.value;
	
	                            object[prop] = this[prop];
	                        }context$2$0.next = 12;
	                        break;
	
	                    case 8:
	                        context$2$0.prev = 8;
	                        context$2$0.t0 = context$2$0["catch"](4);
	                        _didIteratorError3 = true;
	                        _iteratorError3 = context$2$0.t0;
	
	                    case 12:
	                        context$2$0.prev = 12;
	                        context$2$0.prev = 13;
	
	                        if (!_iteratorNormalCompletion3 && _iterator3["return"]) {
	                            _iterator3["return"]();
	                        }
	
	                    case 15:
	                        context$2$0.prev = 15;
	
	                        if (!_didIteratorError3) {
	                            context$2$0.next = 18;
	                            break;
	                        }
	
	                        throw _iteratorError3;
	
	                    case 18:
	                        return context$2$0.finish(15);
	
	                    case 19:
	                        return context$2$0.finish(12);
	
	                    case 20:
	                        context$2$0.next = 22;
	                        return this.backendAdapter.store.add(this.collection, object);
	
	                    case 22:
	                        return context$2$0.abrupt("return", context$2$0.sent);
	
	                    case 23:
	                    case "end":
	                        return context$2$0.stop();
	                }
	            }, null, this, [[4, 8, 12, 20], [13,, 15, 19]]);
	        }
	    }, {
	        key: "update",
	        value: function update(changes) {
	            return this.backendAdapter.store.update(this.collection, this.id, changes);
	        }
	    }, {
	        key: "delete",
	        value: function _delete() {
	            return this.backendAdapter.store.remove(this.collection, this.id);
	        }
	    }, {
	        key: "onUpdate",
	        value: function onUpdate(callback) {
	            return this.backendAdapter.store.onItemUpdated(this.collection, this.id, callback);
	        }
	    }, {
	        key: "onRemove",
	        value: function onRemove(callback) {
	            return this.backendAdapter.store.onItemRemoved(this.collection, this.id, callback);
	        }
	    }], [{
	        key: "collection",
	
	        // Initializing the collection name with the name of the class.
	        get: function () {
	            return this.name.toLowerCase();
	        }
	    }, {
	        key: "validateProperties",
	        value: function validateProperties(object) {
	            var _iteratorNormalCompletion4 = true;
	            var _didIteratorError4 = false;
	            var _iteratorError4 = undefined;
	
	            try {
	                for (var _iterator4 = this.requiredProperties[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
	                    var prop = _step4.value;
	
	                    if (!object.hasOwnProperty(prop)) throw new Error("Property " + prop + " missing on creating an instance of " + this.collection);
	                }
	            } catch (err) {
	                _didIteratorError4 = true;
	                _iteratorError4 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion4 && _iterator4["return"]) {
	                        _iterator4["return"]();
	                    }
	                } finally {
	                    if (_didIteratorError4) {
	                        throw _iteratorError4;
	                    }
	                }
	            }
	        }
	    }, {
	        key: "find",
	        value: function find(backendAdapter, id) {
	            var result;
	            return regeneratorRuntime.async(function find$(context$2$0) {
	                while (1) switch (context$2$0.prev = context$2$0.next) {
	                    case 0:
	                        context$2$0.next = 2;
	                        return backendAdapter.store.find(this.collection, id);
	
	                    case 2:
	                        result = context$2$0.sent;
	                        return context$2$0.abrupt("return", new this(backendAdapter, result));
	
	                    case 4:
	                    case "end":
	                        return context$2$0.stop();
	                }
	            }, null, this);
	        }
	    }, {
	        key: "findAll",
	        value: function findAll(backendAdapter, queryFunction) {
	            var results;
	            return regeneratorRuntime.async(function findAll$(context$2$0) {
	                var _this2 = this;
	
	                while (1) switch (context$2$0.prev = context$2$0.next) {
	                    case 0:
	                        context$2$0.next = 2;
	                        return backendAdapter.store.findAll(this.collection, queryFunction);
	
	                    case 2:
	                        results = context$2$0.sent;
	                        return context$2$0.abrupt("return", (function () {
	                            var _ref = [];
	                            var _iteratorNormalCompletion5 = true;
	                            var _didIteratorError5 = false;
	                            var _iteratorError5 = undefined;
	
	                            try {
	                                for (var _iterator5 = results[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
	                                    var result = _step5.value;
	
	                                    _ref.push(new _this2(backendAdapter, result));
	                                }
	                            } catch (err) {
	                                _didIteratorError5 = true;
	                                _iteratorError5 = err;
	                            } finally {
	                                try {
	                                    if (!_iteratorNormalCompletion5 && _iterator5["return"]) {
	                                        _iterator5["return"]();
	                                    }
	                                } finally {
	                                    if (_didIteratorError5) {
	                                        throw _iteratorError5;
	                                    }
	                                }
	                            }
	
	                            return _ref;
	                        })());
	
	                    case 4:
	                    case "end":
	                        return context$2$0.stop();
	                }
	            }, null, this);
	        }
	    }, {
	        key: "updateAll",
	        value: function updateAll(backendAdapter, changes) {
	            return backendAdapter.store.updateAll(this.collection, changes);
	        }
	    }, {
	        key: "removeAll",
	        value: function removeAll(backendAdapter) {
	            return backendAdapter.store.removeAll(this.collection);
	        }
	    }, {
	        key: "onAddToCollection",
	
	        //Hoodie database events
	        value: function onAddToCollection(backendAdapter, callback) {
	            var _this3 = this;
	
	            return backendAdapter.store.onAdd(this.collection, function (result) {
	                return callback(new _this3(backendAdapter, result));
	            });
	        }
	    }, {
	        key: "onUpdateInCollection",
	        value: function onUpdateInCollection(backendAdapter, callback) {
	            return backendAdapter.store.onUpdate(this.collection, callback);
	        }
	    }, {
	        key: "onRemoveFromCollection",
	        value: function onRemoveFromCollection(backendAdapter, callback) {
	            return backendAdapter.store.onRemove(this.collection, callback);
	        }
	    }, {
	        key: "cast",
	        value: function cast(backendAdapter, object) {
	            var castedObject = {};
	            var _iteratorNormalCompletion6 = true;
	            var _didIteratorError6 = false;
	            var _iteratorError6 = undefined;
	
	            try {
	                for (var _iterator6 = this.requiredProperties[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
	                    var prop = _step6.value;
	
	                    if (object.hasOwnProperty(prop)) castedObject[prop] = object[prop];else throw new Error("Property " + prop + " missing on casting an object to " + this.name);
	                }
	            } catch (err) {
	                _didIteratorError6 = true;
	                _iteratorError6 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion6 && _iterator6["return"]) {
	                        _iterator6["return"]();
	                    }
	                } finally {
	                    if (_didIteratorError6) {
	                        throw _iteratorError6;
	                    }
	                }
	            }
	
	            if (object.id) castedObject.id = object.id;
	
	            return new this(backendAdapter, castedObject);
	        }
	    }]);
	
	    Model = Collection()(Model) || Model;
	    Model = Required()(Model) || Model;
	    return Model;
	})();
	
	Model.Required = Required;
	Model.Collection = Collection;
	
	exports["default"] = Model;
	module.exports = exports["default"];
	/* required properties */

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map