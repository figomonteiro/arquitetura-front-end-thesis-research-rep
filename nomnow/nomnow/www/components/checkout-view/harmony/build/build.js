/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer checkout-view custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	function parseLocation(addressComponents) {
	    var address = {
	        floor: ''
	    };
	    var _iteratorNormalCompletion = true;
	    var _didIteratorError = false;
	    var _iteratorError = undefined;
	
	    try {
	        for (var _iterator = addressComponents[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var param = _step.value;
	
	            if (/postal_code/.test(param.types[0])) {
	                address.zipCode = param['long_name'];
	            } else if (/route/.test(param.types[0])) {
	                address.street = param['long_name'];
	            } else if (/street_number/.test(param.types[0])) {
	                address.doorNumber = param['long_name'];
	            }
	        }
	    } catch (err) {
	        _didIteratorError = true;
	        _iteratorError = err;
	    } finally {
	        try {
	            if (!_iteratorNormalCompletion && _iterator['return']) {
	                _iterator['return']();
	            }
	        } finally {
	            if (_didIteratorError) {
	                throw _iteratorError;
	            }
	        }
	    }
	
	    return address;
	}
	Polymer('checkout-view', {
	    loading: false,
	    selectLocation: function selectLocation() {
	        var _this = this;
	
	        if (!this.checkBounds()) {
	            return this.asyncFire('show-popup-message', {
	                message: 'You must choose a location within the given radius.'
	            });
	        }
	
	        var position = this.$.deliveryMarker.marker.getPosition();
	        this.geocoder.geocode({ 'latLng': position }, function (results, status) {
	            if (status == google.maps.GeocoderStatus.OK) {
	                if (results[1]) {
	                    _this.selectedAddress = parseLocation(results[0].address_components);
	                } else {
	                    alert('No results found');
	                }
	            } else {
	                alert('Geocoder failed due to: ' + status);
	            }
	        });
	
	        this.$.pagerTile.selectedPage = 1;
	    },
	    confirmOrder: function confirmOrder() {
	        this.observations = this.$.observationsInput.value;
	        this.clientLocation = {
	            latitude: this.$.deliveryMarker.marker.getPosition().A,
	            longitude: this.$.deliveryMarker.marker.getPosition().F
	        };
	        // Check valid location
	        if (this.centerPosition.A == this.clientLocation.latitude && this.centerPosition.F == this.clientLocation.longitude) {
	            return this.asyncFire('show-popup-message', {
	                message: 'The delivery location must be different than the restaurant location.'
	            });
	        }
	        // Fire event to get the estimated order detail values for price, waiting time, etc
	        this.asyncFire('checkout-location-set', {
	            restaurant: this.restaurant.objectId,
	            clientLocation: this.clientLocation
	        }, this);
	        this.loading = true;
	
	        this.totalPrice = 0;
	        var _iteratorNormalCompletion2 = true;
	        var _didIteratorError2 = false;
	        var _iteratorError2 = undefined;
	
	        try {
	            for (var _iterator2 = this.items[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                var item = _step2.value;
	
	                this.totalPrice += item.product.price;
	            }
	        } catch (err) {
	            _didIteratorError2 = true;
	            _iteratorError2 = err;
	        } finally {
	            try {
	                if (!_iteratorNormalCompletion2 && _iterator2['return']) {
	                    _iterator2['return']();
	                }
	            } finally {
	                if (_didIteratorError2) {
	                    throw _iteratorError2;
	                }
	            }
	        }
	    },
	    checkout: function checkout() {
	        this.asyncFire('order-checkout', {
	            restaurant: this.restaurant,
	            address: this.selectedAddress,
	            location: this.clientLocation,
	            observations: this.observations,
	            orderItems: this.items
	        });
	        this.loading = true;
	    },
	    showConfirmationPage: function showConfirmationPage(minDeliveryCost, maxDeliveryCost, minDeliveryTime, maxDeliveryTime) {
	        this.loading = false;
	        this.minDeliveryCost = minDeliveryCost;
	        this.maxDeliveryCost = maxDeliveryCost;
	        this.minDeliveryTime = Math.ceil(minDeliveryTime);
	        this.maxDeliveryTime = Math.ceil(maxDeliveryTime);
	        this.minPriceWithDelivery = this.totalPrice + minDeliveryCost;
	        this.maxPriceWithDelivery = this.totalPrice + maxDeliveryCost;
	        // Only show minimum values when they differ from the max values
	        if (minDeliveryCost == maxDeliveryCost) {
	            this.minDeliveryCost = false;
	            this.minPriceWithDelivery = false;
	        }
	        if (minDeliveryTime == maxDeliveryTime) {
	            this.minDeliveryTime = false;
	        }
	        // Show confirmation page
	        this.$.pagerTile.selectedPage = 2;
	    },
	    setMapsApiReady: function setMapsApiReady() {
	        this.mapReady = true;
	        if (this.restaurantReady) {
	            this.initializeMap();
	        }
	    },
	    checkBounds: function checkBounds() {
	        return google.maps.geometry.spherical.computeDistanceBetween(this.$.deliveryMarker.marker.getPosition(), this.centerPosition) < 5000;
	    },
	    restaurantChanged: function restaurantChanged() {
	        this.restaurantReady = true;
	        if (this.mapReady) {
	            this.initializeMap();
	        }
	    },
	    initializeMap: function initializeMap() {
	        this.centerPosition = new google.maps.LatLng(this.restaurant.location.latitude, this.restaurant.location.longitude);
	
	        google.maps.event.trigger(this.$.mapElement.map, 'resize');
	        this.$.mapElement.map.setCenter(this.centerPosition);
	        this.deliveryArea = new google.maps.Circle({
	            map: this.$.mapElement.map,
	            strokeColor: '#f8af61',
	            strokeOpastate: 0.8,
	            strokeWeight: 2,
	            fillColor: '#ffffff',
	            fillOpastate: 0.15,
	            center: this.centerPosition,
	            radius: 5000
	        });
	        this.geocoder = new google.maps.Geocoder();
	    },
	    nextPageHandler: function nextPageHandler(event) {
	        switch (this.$.pagerTile.selectedPage) {
	            case 0:
	                this.selectLocation();
	                break;
	            case 1:
	                this.confirmOrder();
	                break;
	            case 2:
	                this.checkout();
	                break;
	        }
	        event.stopImmediatePropagation();
	    },
	    closePopupClicked: function closePopupClicked(event) {
	        this.asyncFire('close-popup-clicked');
	        event.stopImmediatePropagation();
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map