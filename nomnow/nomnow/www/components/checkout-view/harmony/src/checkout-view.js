/**
 *
 * Registration of polymer checkout-view custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
function parseLocation(addressComponents) {
    let address = {
        floor: ''
    }
    for (let param of addressComponents) {
        if (/postal_code/.test(param.types[0])) {
            address.zipCode = param['long_name']
        } else if (/route/.test(param.types[0])) {
            address.street = param['long_name']
        } else if (/street_number/.test(param.types[0])) {
            address.doorNumber = param['long_name']
        }
    }
    return address
}
Polymer('checkout-view', {
    loading: false,
    selectLocation() {
        if (!this.checkBounds()) {
            return this.asyncFire('show-popup-message', {
                message: 'You must choose a location within the given radius.'
            })
        }

        let position = this.$.deliveryMarker.marker.getPosition()
        this.geocoder.geocode({'latLng': position}, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    this.selectedAddress = parseLocation(results[0].address_components)
                } else {
                    alert('No results found')
                }
            } else {
                alert('Geocoder failed due to: ' + status)
            }
        })

        this.$.pagerTile.selectedPage = 1
    },
    confirmOrder() {
        this.observations = this.$.observationsInput.value
        this.clientLocation = {
            latitude: this.$.deliveryMarker.marker.getPosition().A,
            longitude: this.$.deliveryMarker.marker.getPosition().F
        }
        // Check valid location
        if (this.centerPosition.A == this.clientLocation.latitude
            && this.centerPosition.F == this.clientLocation.longitude) {
            return this.asyncFire('show-popup-message', {
                message: 'The delivery location must be different than the restaurant location.'
            })
        }
        // Fire event to get the estimated order detail values for price, waiting time, etc
        this.asyncFire('checkout-location-set', {
            restaurant: this.restaurant.objectId,
            clientLocation: this.clientLocation
        }, this)
        this.loading = true

        this.totalPrice = 0
        for (let item of this.items) {
            this.totalPrice += item.product.price
        }
    },
    checkout() {
        this.asyncFire('order-checkout', {
            restaurant: this.restaurant,
            address: this.selectedAddress,
            location: this.clientLocation,
            observations: this.observations,
            orderItems: this.items
        })
        this.loading = true
    },
    showConfirmationPage(minDeliveryCost, maxDeliveryCost, minDeliveryTime, maxDeliveryTime) {
        this.loading = false
        this.minDeliveryCost = minDeliveryCost
        this.maxDeliveryCost = maxDeliveryCost
        this.minDeliveryTime = Math.ceil(minDeliveryTime)
        this.maxDeliveryTime = Math.ceil(maxDeliveryTime)
        this.minPriceWithDelivery = this.totalPrice + minDeliveryCost
        this.maxPriceWithDelivery = this.totalPrice + maxDeliveryCost
        // Only show minimum values when they differ from the max values
        if (minDeliveryCost == maxDeliveryCost) {
            this.minDeliveryCost = false
            this.minPriceWithDelivery = false
        }
        if (minDeliveryTime == maxDeliveryTime) {
            this.minDeliveryTime = false
        }
        // Show confirmation page
        this.$.pagerTile.selectedPage = 2
    },
    setMapsApiReady() {
        this.mapReady = true
        if (this.restaurantReady) {
            this.initializeMap()
        }
    },
    checkBounds() {
        return google.maps.geometry.spherical.computeDistanceBetween(
                this.$.deliveryMarker.marker.getPosition(),
                this.centerPosition
            ) < 5000
    },
    restaurantChanged() {
        this.restaurantReady = true
        if (this.mapReady) {
            this.initializeMap()
        }
    },
    initializeMap() {
        this.centerPosition = new google.maps.LatLng(
            this.restaurant.location.latitude,
            this.restaurant.location.longitude
        )

        google.maps.event.trigger(this.$.mapElement.map, 'resize')
        this.$.mapElement.map.setCenter(this.centerPosition)
        this.deliveryArea = new google.maps.Circle({
            map: this.$.mapElement.map,
            strokeColor: '#f8af61',
            strokeOpastate: 0.8,
            strokeWeight: 2,
            fillColor: '#ffffff',
            fillOpastate: 0.15,
            center: this.centerPosition,
            radius: 5000
        })
        this.geocoder = new google.maps.Geocoder();
    },
    nextPageHandler(event) {
        switch(this.$.pagerTile.selectedPage) {
            case 0:
                this.selectLocation()
                break
            case 1:
                this.confirmOrder()
                break
            case 2:
                this.checkout()
                break
        }
        event.stopImmediatePropagation()
    },
    closePopupClicked(event) {
        this.asyncFire('close-popup-clicked')
        event.stopImmediatePropagation()
    }
})