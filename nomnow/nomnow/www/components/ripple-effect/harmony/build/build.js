/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer ripple-effect custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('ripple-effect', {
	    maxScale: 15,
	    publish: {
	        recenteringTouch: false
	    },
	    bubbleAnimationPlayer: null,
	    touchingHandler: function touchingHandler(event) {
	        var bubble, elementRectangle, resizeRatio;
	        return regeneratorRuntime.async(function touchingHandler$(context$1$0) {
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    bubble = document.createElement('div');
	
	                    bubble.setAttribute('class', 'bubble');
	                    this.$.background.appendChild(bubble);
	
	                    elementRectangle = this.getBoundingClientRect(), resizeRatio = (elementRectangle.width > elementRectangle.height ? elementRectangle.width : elementRectangle.height) / 20;
	
	                    this.bubbleAnimationPlayer = this.bubbleAnimation(bubble, event.x - elementRectangle.left, event.y - elementRectangle.top, resizeRatio > this.maxScale ? this.maxScale : resizeRatio);
	                    context$1$0.next = 7;
	                    return this.bubbleAnimationPlayer.finishedPromise;
	
	                case 7:
	                    context$1$0.next = 9;
	                    return this.bubbleDisappearAnimation().finishedPromise;
	
	                case 9:
	                    this.$.background.removeChild(bubble);
	
	                case 10:
	                case 'end':
	                    return context$1$0.stop();
	            }
	        }, null, this);
	    },
	    stopTouchingHandler: function stopTouchingHandler() {
	        this.bubbleAnimationPlayer.finish();
	    },
	    bubbleAnimation: function bubbleAnimation(bubble, startX, startY, resizeRatio) {
	        var endX = this.recenteringTouch ? this.offsetWidth / 2 : startX,
	            endY = this.recenteringTouch ? this.offsetHeight / 2 : startY;
	        var bubbleAnimParams = {
	            initialState: {
	                transform: 'translate3d(' + startX + 'px,' + startY + 'px, 0) ' + 'scale3d(1, 1, 1) ',
	                opacity: 1
	            },
	            finalState: {
	                transform: 'translate3d(' + endX + 'px,' + endY + 'px, 0) ' + 'scale3d(' + resizeRatio + ', ' + resizeRatio + ', 1)',
	                opacity: 0.2
	            }
	        },
	            backgroundAnimParams = {
	            initialState: {
	                opacity: 0
	            },
	            finalState: {
	                opacity: 1
	            }
	        };
	
	        var bubbling = new KeyframeEffect(bubble, [bubbleAnimParams.initialState, bubbleAnimParams.finalState], { duration: 600, fill: 'forwards', easing: 'ease' });
	        var backgroundFade = new KeyframeEffect(this.$.background, [backgroundAnimParams.initialState, backgroundAnimParams.finalState], { duration: 400, fill: 'forwards', easing: 'ease' });
	        var player = document.timeline.play(new GroupEffect([bubbling, backgroundFade]));
	        player.finishedPromise = new Promise(function (resolve) {
	            player.onfinish = function (playerEvent) {
	                resolve(playerEvent);
	            };
	        });
	        return player;
	    },
	    bubbleDisappearAnimation: function bubbleDisappearAnimation() {
	        var player = this.$.background.animate([{ opacity: 1 }, { opacity: 0 }], { duration: 500, fill: 'forwards', easing: 'ease-out' });
	        player.finishedPromise = new Promise(function (resolve) {
	            player.onfinish = function (playerEvent) {
	                return resolve(playerEvent);
	            };
	        });
	        return player;
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map