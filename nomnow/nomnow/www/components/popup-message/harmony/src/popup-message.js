/**
 *
 * Registration of polymer popup-message custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('popup-message', {
	close() {
        let _this = this
		this.fire('message-closed', { success: _this.success });
	},
	keypressHandler(event) {
        console.log("asd")
		if (event.keyCode == 13) {
			this.close()
		}
	}
})