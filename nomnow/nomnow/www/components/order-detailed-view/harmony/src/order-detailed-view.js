/**
 *
 * Registration of polymer order-detailed-view custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('order-detailed-view', {
    setPricePrecision: function(value) {
        if (value) {
            return parseFloat(value).toFixed(2)
        }
    },
    viewDetailsButtonClicked() {
        this.asyncFire('view-products-button-clicked', this.order)
    }
})