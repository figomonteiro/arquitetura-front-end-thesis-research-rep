/**
 *
 * Registration of polymer animated-delete-button custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('animated-delete-button', {
	animated: false,
    eventDelegates: {
        tap: "handleTap"
    },
    handleTap() {
        if (!this.animated) {
            console.log("cenas")
            this.$.pathOne.classList.add('animated')
            this.$.pathTwo.classList.add('animated')
        }
    }
})