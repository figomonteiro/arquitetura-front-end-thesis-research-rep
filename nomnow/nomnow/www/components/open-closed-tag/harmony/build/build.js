/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer open-closed-tag custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	Polymer("open-closed-tag", {
	    openingDateISOChanged: function openingDateISOChanged() {
	        this.openingDate = new Date(this.openingDateISO);
	        if (this.closingDate) {
	            this.updateStatusText();
	        }
	    },
	    closingDateISOChanged: function closingDateISOChanged() {
	        this.closingDate = new Date(this.closingDateISO);
	        if (this.openingDate) {
	            this.updateStatusText();
	        }
	    },
	    updateStatusText: function updateStatusText() {
	        var openingInMinutes = 60 * this.openingDate.getHours() + this.openingDate.getMinutes(),
	            closingInMinutes = 60 * this.closingDate.getHours() + this.closingDate.getMinutes(),
	            now = new Date(),
	            nowInMinutes = 60 * now.getHours() + now.getMinutes();
	
	        if (openingInMinutes < closingInMinutes) {
	            this.status = nowInMinutes < closingInMinutes && nowInMinutes > openingInMinutes ? "Now open" : "Closed now";
	        } else {
	            this.status = nowInMinutes <= openingInMinutes && nowInMinutes >= closingInMinutes ? "Closed now" : "Now open";
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map