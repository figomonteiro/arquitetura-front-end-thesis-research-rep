/**
 *
 * Registration of polymer open-closed-tag custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('open-closed-tag', {
    openingDateISOChanged() {
        this.openingDate = new Date(this.openingDateISO)
        if (this.closingDate) {
            this.updateStatusText()
        }
    },
    closingDateISOChanged() {
        this.closingDate = new Date(this.closingDateISO)
        if (this.openingDate) {
            this.updateStatusText()
        }
    },
    updateStatusText() {
        let openingInMinutes = 60 * this.openingDate.getHours() + this.openingDate.getMinutes(),
            closingInMinutes = 60 * this.closingDate.getHours() + this.closingDate.getMinutes(),
            now = new Date(),
            nowInMinutes = 60 * now.getHours() + now.getMinutes()

        if (openingInMinutes < closingInMinutes) {
            this.status = (nowInMinutes < closingInMinutes && nowInMinutes > openingInMinutes) ?
                "Now open" : "Closed now"
        } else {
            this.status = (nowInMinutes <= openingInMinutes && nowInMinutes >= closingInMinutes) ?
                "Closed now" : "Now open"
        }
    }
})