/**
 * Registration of polymer hoodie-backend-adapter custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let adapter, session, store
Polymer('hoodie-backend-adapter', {
    attached() {
        // Implements Singleton design pattern
        if (!adapter) {
            adapter = new Hoodie(this.url)
            session = {
                username: adapter.account.username,
                signUp: adapter.account.signUp,
                signIn: adapter.account.signIn,
                signOut: adapter.account.signOut,
                changePassword: adapter.account.changePassword,
                changeUsername: adapter.account.changeUsername,
                resetPassword: adapter.account.resetPassword,
                destroy: adapter.account.destroy,

                onSignUpOrSignIn(callback) {
                    adapter.account.on('signin signup', callback)
                },
                onSignOut(callback) {
                    adapter.account.on('signout', callback)
                },
                onUnauthenticatedError(callback) {
                    adapter.account.on('error:unauthenticated', callback)
                }
            }
            store = {
                add: adapter.store.add,
                find: adapter.store.find,
                findOrAdd: adapter.store.findOrAdd,
                findAll(collection, queryFunction) {
                    return adapter.store.findAll(object => {
                            return object.type === collection
                                && (!queryFunction || queryFunction(object))
                        }
                    )
                },
                update: adapter.store.update,
                updateAll: adapter.store.updateAll,
                remove: adapter.store.remove,
                removeAll: adapter.store.removeAll,

                onAdd(collection, callback) {
                    adapter.store.on(collection + ':add', callback)
                    // BUG HOODIE: add event sometimes activated as an update event
                    adapter.store.on(collection + ':update', result => {
                        if (result.createdAt == result.updatedAt || !result.updatedAt)
                            callback(result)
                    })
                },
                onItemUpdated(collection, id, callback) {
                    adapter.store.on(collection + ':' + id + ':update', callback)
                },
                onUpdate(collection, callback) {
                    adapter.store.on(collection + ':update', callback)
                },
                onItemRemoved(collection, id, callback) {
                    adapter.store.on(collection + ':' + id + ':remove', callback)
                },
                onRemove(collection, callback) {
                    adapter.store.on(collection + ':remove', callback)
                }
            }
            session.onSignUpOrSignIn(user => {
                session.username = user
            })
            session.onSignOut(result => {
                session.username = null
            })
        }
        this.adapter = adapter
        this.session = session
        this.store = store

        // Element event handlers
        let _this = this
        this.onLoginHandler = (event) => {
            _this.session.signIn(event.detail.username, event.detail.password)
                .catch(error => _this.asyncFire('hoodie-message', {message: error.message}))
        }
        this.onRegisterHandler = (event) => {
            _this.session.signUp(event.detail.username, event.detail.password)
                .catch(error => _this.asyncFire('hoodie-message', {message: error.message}))
        }
        this.onChangePasswordHandler = async (event) => {
            try {
                await _this.session.changePassword(event.detail.password, event.detail.newPassword)
                _this.asyncFire('hoodie-message', {
                    success: true,
                    message: 'Password changed successfully'
                })
            } catch (error) {
                _this.asyncFire('hoodie-message', {message: error.message})
            }
        }
    }
})