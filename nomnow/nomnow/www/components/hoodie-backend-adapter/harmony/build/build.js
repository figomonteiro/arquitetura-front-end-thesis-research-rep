/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Registration of polymer hoodie-backend-adapter custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	var adapter = undefined,
	    session = undefined,
	    store = undefined;
	Polymer('hoodie-backend-adapter', {
	    attached: function attached() {
	        var _this2 = this;
	
	        // Implements Singleton design pattern
	        if (!adapter) {
	            adapter = new Hoodie(this.url);
	            session = {
	                username: adapter.account.username,
	                signUp: adapter.account.signUp,
	                signIn: adapter.account.signIn,
	                signOut: adapter.account.signOut,
	                changePassword: adapter.account.changePassword,
	                changeUsername: adapter.account.changeUsername,
	                resetPassword: adapter.account.resetPassword,
	                destroy: adapter.account.destroy,
	
	                onSignUpOrSignIn: function onSignUpOrSignIn(callback) {
	                    adapter.account.on('signin signup', callback);
	                },
	                onSignOut: function onSignOut(callback) {
	                    adapter.account.on('signout', callback);
	                },
	                onUnauthenticatedError: function onUnauthenticatedError(callback) {
	                    adapter.account.on('error:unauthenticated', callback);
	                }
	            };
	            store = {
	                add: adapter.store.add,
	                find: adapter.store.find,
	                findOrAdd: adapter.store.findOrAdd,
	                findAll: function findAll(collection, queryFunction) {
	                    return adapter.store.findAll(function (object) {
	                        return object.type === collection && (!queryFunction || queryFunction(object));
	                    });
	                },
	                update: adapter.store.update,
	                updateAll: adapter.store.updateAll,
	                remove: adapter.store.remove,
	                removeAll: adapter.store.removeAll,
	
	                onAdd: function onAdd(collection, callback) {
	                    adapter.store.on(collection + ':add', callback);
	                    // BUG HOODIE: add event sometimes activated as an update event
	                    adapter.store.on(collection + ':update', function (result) {
	                        if (result.createdAt == result.updatedAt || !result.updatedAt) callback(result);
	                    });
	                },
	                onItemUpdated: function onItemUpdated(collection, id, callback) {
	                    adapter.store.on(collection + ':' + id + ':update', callback);
	                },
	                onUpdate: function onUpdate(collection, callback) {
	                    adapter.store.on(collection + ':update', callback);
	                },
	                onItemRemoved: function onItemRemoved(collection, id, callback) {
	                    adapter.store.on(collection + ':' + id + ':remove', callback);
	                },
	                onRemove: function onRemove(collection, callback) {
	                    adapter.store.on(collection + ':remove', callback);
	                }
	            };
	            session.onSignUpOrSignIn(function (user) {
	                session.username = user;
	            });
	            session.onSignOut(function (result) {
	                session.username = null;
	            });
	        }
	        this.adapter = adapter;
	        this.session = session;
	        this.store = store;
	
	        // Element event handlers
	        var _this = this;
	        this.onLoginHandler = function (event) {
	            _this.session.signIn(event.detail.username, event.detail.password)['catch'](function (error) {
	                return _this.asyncFire('hoodie-message', { message: error.message });
	            });
	        };
	        this.onRegisterHandler = function (event) {
	            _this.session.signUp(event.detail.username, event.detail.password)['catch'](function (error) {
	                return _this.asyncFire('hoodie-message', { message: error.message });
	            });
	        };
	        this.onChangePasswordHandler = function callee$1$0(event) {
	            return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
	                while (1) switch (context$2$0.prev = context$2$0.next) {
	                    case 0:
	                        context$2$0.prev = 0;
	                        context$2$0.next = 3;
	                        return _this.session.changePassword(event.detail.password, event.detail.newPassword);
	
	                    case 3:
	                        _this.asyncFire('hoodie-message', {
	                            success: true,
	                            message: 'Password changed successfully'
	                        });
	                        context$2$0.next = 9;
	                        break;
	
	                    case 6:
	                        context$2$0.prev = 6;
	                        context$2$0.t0 = context$2$0['catch'](0);
	
	                        _this.asyncFire('hoodie-message', { message: context$2$0.t0.message });
	
	                    case 9:
	                    case 'end':
	                        return context$2$0.stop();
	                }
	            }, null, _this2, [[0, 6]]);
	        };
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map