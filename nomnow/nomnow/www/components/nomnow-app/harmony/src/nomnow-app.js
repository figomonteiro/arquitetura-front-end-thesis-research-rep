/**
 *
 * Registration of polymer nomnow-app custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('nomnow-app', {
    attached() {
        this.signOut = () => {
            if (!this.signInSkipped) {
                this.adapter.session.signOut()
            } else {
                this.signInSkipped = false
            }
        }
        this.skipSignIn = () => {
            this.signInSkipped = true
        }
    }
})