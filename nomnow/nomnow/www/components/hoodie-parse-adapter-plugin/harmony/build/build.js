/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer hoodie-parse-adapter-plugin custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	var storePlaceholder = {
	    add: function add() {
	        throw new Error("Read-only collection.");
	    },
	    findOrAdd: function findOrAdd() {
	        throw new Error("Read-only collection.");
	    },
	    update: function update() {
	        throw new Error("Read-only collection.");
	    },
	    updateAll: function updateAll() {
	        throw new Error("Read-only collection.");
	    },
	    remove: function remove() {
	        throw new Error("Read-only collection.");
	    },
	    removeAll: function removeAll() {
	        throw new Error("Read-only collection.");
	    }
	};
	var syncCollections = [];
	Polymer("hoodie-parse-adapter-plugin", {
	    attached: function attached() {
	        var _this = this;
	
	        this.hoodieAdapter = this.parentElement;
	        if (!this.hoodieAdapter && this.hoodieAdapter.tagName == "HOODIE-BACKEND-ADAPTER") {
	            throw new Error("hoodie-static-resources-plugin element must be " + "used nested with a hoodie-backend-adapter element");
	        }
	        if (!this.hoodieAdapter.adapter.parse) {
	            throw new Error("Hoodie plugin 'parse' is not installed.");
	        }
	        if (this.syncCollections) {
	            syncCollections = this.syncCollections;
	        }
	
	        // Synchronize private collections on the element attribute syncCollections
	        var syncPrivateCollections = function syncPrivateCollections() {
	            if (syncCollections) {
	                var collectionsToSync = syncCollections.split(" ");
	                _this.hoodieAdapter.adapter.parse.syncPrivateData(collectionsToSync);
	            }
	        };
	        // Mapping between adapter methods and Parse plugin session methods
	        this.session = {
	            signIn: this.hoodieAdapter.adapter.parse.authenticate,
	            signOut: this.hoodieAdapter.adapter.parse.deauthenticate,
	            username: this.hoodieAdapter.session.username
	        };
	        this.session.signInWithFacebook = function () {
	            _this.hoodieAdapter.adapter.parse.authenticateWithFacebook().then(syncPrivateCollections);
	        };
	        // Mapping between adapter methods and Parse plugin store methods
	        this.store = storePlaceholder;
	        this.store.find = this.hoodieAdapter.adapter.parse.store.findFromCollectionByParseId;
	        this.store.findAll = this.hoodieAdapter.adapter.parse.store.findAllFromCollection;
	        this.store.onAdd = this.hoodieAdapter.adapter.parse.store.onAdd;
	        this.store.onItemUpdated = this.hoodieAdapter.adapter.parse.store.onItemUpdated;
	        this.store.onUpdate = this.hoodieAdapter.adapter.parse.store.onUpdate;
	        this.store.onItemRemoved = this.hoodieAdapter.adapter.parse.store.onItemRemoved;
	        this.store.onRemove = this.hoodieAdapter.adapter.parse.store.onRemove;
	    },
	    observe: {
	        "hoodieAdapter.session.username": "updateSession"
	    },
	    updateSession: function updateSession() {
	        this.session.username = this.hoodieAdapter.session.username;
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map