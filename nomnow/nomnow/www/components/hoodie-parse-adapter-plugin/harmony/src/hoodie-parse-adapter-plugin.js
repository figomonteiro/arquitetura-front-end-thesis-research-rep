/**
 *
 * Registration of polymer hoodie-parse-adapter-plugin custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let storePlaceholder = {
    add() {throw new Error("Read-only collection.")},
    findOrAdd() {throw new Error("Read-only collection.")},
    update() {throw new Error("Read-only collection.")},
    updateAll() {throw new Error("Read-only collection.")},
    remove() {throw new Error("Read-only collection.")},
    removeAll() {throw new Error("Read-only collection.")}
}
let syncCollections = []
Polymer('hoodie-parse-adapter-plugin', {
    attached() {
        this.hoodieAdapter = this.parentElement
        if (!this.hoodieAdapter && this.hoodieAdapter.tagName == 'HOODIE-BACKEND-ADAPTER') {
            throw new Error('hoodie-static-resources-plugin element must be ' +
                'used nested with a hoodie-backend-adapter element')
        }
        if (!this.hoodieAdapter.adapter.parse) {
            throw new Error('Hoodie plugin \'parse\' is not installed.')
        }
        if (this.syncCollections) {
            syncCollections = this.syncCollections
        }

        // Synchronize private collections on the element attribute syncCollections
        let syncPrivateCollections = () => {
            if (syncCollections) {
                let collectionsToSync = syncCollections.split(' ')
                this.hoodieAdapter.adapter.parse.syncPrivateData(collectionsToSync)
            }
        }
        // Mapping between adapter methods and Parse plugin session methods
        this.session = {
            signIn: this.hoodieAdapter.adapter.parse.authenticate,
            signOut: this.hoodieAdapter.adapter.parse.deauthenticate,
            username: this.hoodieAdapter.session.username
        }
        this.session.signInWithFacebook = () => {
            this.hoodieAdapter.adapter.parse.authenticateWithFacebook().then(syncPrivateCollections)
        }
        // Mapping between adapter methods and Parse plugin store methods
        this.store = storePlaceholder
        this.store.find = this.hoodieAdapter.adapter.parse.store.findFromCollectionByParseId
        this.store.findAll = this.hoodieAdapter.adapter.parse.store.findAllFromCollection
        this.store.onAdd = this.hoodieAdapter.adapter.parse.store.onAdd
        this.store.onItemUpdated = this.hoodieAdapter.adapter.parse.store.onItemUpdated
        this.store.onUpdate = this.hoodieAdapter.adapter.parse.store.onUpdate
        this.store.onItemRemoved = this.hoodieAdapter.adapter.parse.store.onItemRemoved
        this.store.onRemove = this.hoodieAdapter.adapter.parse.store.onRemove
    },
    observe: {
        'hoodieAdapter.session.username': 'updateSession'
    },
    updateSession() {
        this.session.username = this.hoodieAdapter.session.username
    }
})