/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _Map = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/core-js/map\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	var _regeneratorRuntime = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/regenerator\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	var _Promise = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/core-js/promise\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	var _interopRequireDefault = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/helpers/interop-require-default\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	var _this = this;
	
	var _cacheManifest = __webpack_require__(1);
	
	var _cacheManifest2 = _interopRequireDefault(_cacheManifest);
	
	var caches = __webpack_require__(2);
	
	var DEFAULT_VERSION = 1;
	
	var deserializeUrlParams = function deserializeUrlParams(queryString) {
	    return new _Map(queryString.split('&').map(function (keyValuePair) {
	        return keyValuePair.split('=').map(decodeURIComponent);
	    }));
	},
	    init = (function () {
	    var params = deserializeUrlParams(location.search.substring(1));
	    // Allow some defaults to be overridden via URL parameters.
	    var version = params.has('version') ? params.get('version') : DEFAULT_VERSION,
	        preCacheUrls = params.has('cacheurls') ? params.get('cacheurls').split(',') : [];
	
	    _cacheManifest2['default'].appSourceCache += version;
	    _cacheManifest2['default'].cacheFilesArray = _cacheManifest2['default'].cacheFilesArray.concat(preCacheUrls);
	})();
	
	self.oninstall = function (event) {
	    event.waitUntil((function callee$1$0() {
	        var cache;
	        return _regeneratorRuntime.async(function callee$1$0$(context$2$0) {
	            while (1) switch (context$2$0.prev = context$2$0.next) {
	                case 0:
	                    context$2$0.next = 2;
	                    return caches.open(_cacheManifest2['default'].appSourceCache);
	
	                case 2:
	                    cache = context$2$0.sent;
	                    context$2$0.next = 5;
	                    return cache.addAll(_cacheManifest2['default'].cacheFilesArray);
	
	                case 5:
	                    return context$2$0.abrupt('return', context$2$0.sent);
	
	                case 6:
	                case 'end':
	                    return context$2$0.stop();
	            }
	        }, null, _this);
	    })());
	};
	
	self.onactivate = function (event) {
	    event.waitUntil((function callee$1$0() {
	        var cacheNames;
	        return _regeneratorRuntime.async(function callee$1$0$(context$2$0) {
	            while (1) switch (context$2$0.prev = context$2$0.next) {
	                case 0:
	                    context$2$0.next = 2;
	                    return caches.keys();
	
	                case 2:
	                    cacheNames = context$2$0.sent;
	                    return context$2$0.abrupt('return', _Promise.all(cacheNames.map(function (cacheName) {
	                        if (_cacheManifest2['default'].expectedCaches.indexOf(cacheName) == -1) {
	                            return caches['delete'](cacheName);
	                        }
	                    })));
	
	                case 4:
	                case 'end':
	                    return context$2$0.stop();
	            }
	        }, null, _this);
	    })());
	};
	
	self.onfetch = function callee$0$0(event) {
	    var requestURL;
	    return _regeneratorRuntime.async(function callee$0$0$(context$1$0) {
	        while (1) switch (context$1$0.prev = context$1$0.next) {
	            case 0:
	                requestURL = new URL(event.request.url);
	
	                if (!(requestURL.hostname != location.hostname)) {
	                    context$1$0.next = 5;
	                    break;
	                }
	
	                event.respondWith(dynamicResourceResponse(event.request, _cacheManifest2['default'].appDynamicCache));
	                context$1$0.next = 16;
	                break;
	
	            case 5:
	                if (!/\/static_resources\//.test(requestURL.pathname)) {
	                    context$1$0.next = 9;
	                    break;
	                }
	
	                event.respondWith(staticResourceResponse(event.request, _cacheManifest2['default'].appStaticCache));
	                context$1$0.next = 16;
	                break;
	
	            case 9:
	                if (!/\/_api\/(?!_files\/)/.test(requestURL.pathname)) {
	                    context$1$0.next = 15;
	                    break;
	                }
	
	                context$1$0.next = 12;
	                return fetch(event.request);
	
	            case 12:
	                return context$1$0.abrupt('return', context$1$0.sent);
	
	            case 15:
	                if (/components\//.test(requestURL.pathname)) {
	                    event.respondWith(staticResourceResponse(event.request, _cacheManifest2['default'].appSourceCache));
	                } else {
	                    event.respondWith(cacheResponse(event.request));
	                }
	
	            case 16:
	            case 'end':
	                return context$1$0.stop();
	        }
	    }, null, _this);
	};
	
	var cacheResponse = function cacheResponse(request) {
	    return _regeneratorRuntime.async(function cacheResponse$(context$1$0) {
	        while (1) switch (context$1$0.prev = context$1$0.next) {
	            case 0:
	                context$1$0.next = 2;
	                return caches.match(request, {
	                    ignoreVary: true
	                });
	
	            case 2:
	                return context$1$0.abrupt('return', context$1$0.sent);
	
	            case 3:
	            case 'end':
	                return context$1$0.stop();
	        }
	    }, null, _this);
	};
	
	var dynamicResourceResponse = function dynamicResourceResponse(request, cacheName) {
	    var response, cache;
	    return _regeneratorRuntime.async(function dynamicResourceResponse$(context$1$0) {
	        while (1) switch (context$1$0.prev = context$1$0.next) {
	            case 0:
	                response = null;
	
	                if (!(request.headers.get('Accept') == 'x-cache/only')) {
	                    context$1$0.next = 8;
	                    break;
	                }
	
	                context$1$0.next = 4;
	                return caches.match(request);
	
	            case 4:
	                response = context$1$0.sent;
	
	                if (!response) {
	                    context$1$0.next = 8;
	                    break;
	                }
	
	                // LOG
	                logCacheRequest(request.url);
	                return context$1$0.abrupt('return', response);
	
	            case 8:
	                context$1$0.next = 10;
	                return fetch(request.clone());
	
	            case 10:
	                response = context$1$0.sent;
	
	                // LOG
	                logServerRequest(request.url);
	                context$1$0.next = 14;
	                return caches.open(cacheName);
	
	            case 14:
	                cache = context$1$0.sent;
	                context$1$0.next = 17;
	                return cache.put(request, response.clone());
	
	            case 17:
	                return context$1$0.abrupt('return', response.clone());
	
	            case 18:
	            case 'end':
	                return context$1$0.stop();
	        }
	    }, null, _this);
	};
	
	var staticResourceResponse = function staticResourceResponse(request, cacheName) {
	    var response, cache;
	    return _regeneratorRuntime.async(function staticResourceResponse$(context$1$0) {
	        while (1) switch (context$1$0.prev = context$1$0.next) {
	            case 0:
	                context$1$0.next = 2;
	                return caches.match(request);
	
	            case 2:
	                response = context$1$0.sent;
	
	                if (!response) {
	                    context$1$0.next = 6;
	                    break;
	                }
	
	                // LOG
	                logCacheRequest(request.url);
	                return context$1$0.abrupt('return', response);
	
	            case 6:
	                context$1$0.next = 8;
	                return fetch(request.clone());
	
	            case 8:
	                response = context$1$0.sent;
	
	                // LOG
	                logServerRequest(request.url);
	                context$1$0.next = 12;
	                return caches.open(cacheName);
	
	            case 12:
	                cache = context$1$0.sent;
	                context$1$0.next = 15;
	                return cache.put(request, response.clone());
	
	            case 15:
	                return context$1$0.abrupt('return', response.clone());
	
	            case 16:
	            case 'end':
	                return context$1$0.stop();
	        }
	    }, null, _this);
	};
	
	var logCacheRequest = function logCacheRequest(url, cacheName) {
	    console.log('%cCache %c%s\n%c%s', 'font-weight: bold; color: #333', 'font-weight: normal; color: #777', cacheName, 'font-style: italic', url);
	},
	    logServerRequest = function logServerRequest(url) {
	    console.log('%cServer\n%c%s', 'font-weight: bold; color: #d35400', 'font-style: italic', url);
	};

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _Object$defineProperty = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/core-js/object/define-property\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	var _Object$defineProperties = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/core-js/object/define-properties\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	_Object$defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var CacheManifest = _Object$defineProperties({
	    appSourceCache: 'proto-app-cache-v',
	    appStaticCache: 'proto-app-static',
	    appDynamicCache: 'proto-app-dynamic',
	    cacheFilesArray: ['./bower_components/core-ajax/core-ajax.html', './bower_components/core-ajax/core-xhr.html', './bower_components/core-animated-pages/core-animated-pages.css', './bower_components/core-animated-pages/core-animated-pages.html', './bower_components/core-component-page/core-component-page.html', './bower_components/core-field/core-field.css', './bower_components/core-field/core-field.html', './bower_components/core-icon/core-icon.css', './bower_components/core-icon/core-icon.html', './bower_components/core-icons/av-icons.html', './bower_components/core-icons/communication-icons.html', './bower_components/core-icons/core-icons.html', './bower_components/core-icons/device-icons.html', './bower_components/core-icons/editor-icons.html', './bower_components/core-icons/hardware-icons.html', './bower_components/core-icons/image-icons.html', './bower_components/core-icons/maps-icons.html', './bower_components/core-icons/notification-icons.html', './bower_components/core-icons/social-icons.html', './bower_components/core-iconset-svg/core-iconset-svg.html', './bower_components/core-iconset-svg/svg-sample-icons.html', './bower_components/core-iconset/core-iconset.html', './bower_components/core-media-query/core-media-query.html', './bower_components/core-meta/core-meta.html', './bower_components/core-resizable/core-resizable.html', './bower_components/core-selection/core-selection.html', './bower_components/core-selector/core-selector.html', './bower_components/core-style/core-style.html', './bower_components/core-style/elements.html', './bower_components/core-style/my-theme.html', './bower_components/core-transition/core-transition-css.html', './bower_components/core-transition/core-transition-overlay.css', './bower_components/core-transition/core-transition.html', './bower_components/font-roboto/roboto.html', './bower_components/paper-shadow/paper-shadow.css', './bower_components/paper-shadow/paper-shadow.html', './bower_components/platform/platform.js', './bower_components/polymer/layout.html', './bower_components/polymer/polymer.html', './bower_components/polymer/polymer.js', './bower_components/polymer/polymer.min.js', './bower_components/swipe-pages/swipe-page.css', './bower_components/swipe-pages/swipe-page.html', './bower_components/swipe-pages/swipe-pages.css', './bower_components/swipe-pages/swipe-pages.html', './bower_components/webcomponentsjs/CustomElements.js', './bower_components/webcomponentsjs/CustomElements.min.js', './bower_components/webcomponentsjs/HTMLImports.js', './bower_components/webcomponentsjs/HTMLImports.min.js', './bower_components/webcomponentsjs/MutationObserver.js', './bower_components/webcomponentsjs/MutationObserver.min.js', './bower_components/webcomponentsjs/ShadowDOM.js', './bower_components/webcomponentsjs/ShadowDOM.min.js', './bower_components/webcomponentsjs/webcomponents-lite.js', './bower_components/webcomponentsjs/webcomponents-lite.min.js', './bower_components/webcomponentsjs/webcomponents.js', './bower_components/webcomponentsjs/webcomponents.min.js', './components/babel-polyfill/browser-polyfill.js', './components/backend-adapter-interface/backend-adapter-interface.html', './components/backend-adapter-interface/harmony/build/build.js', './components/backend-collection/backend-collection.html', './components/backend-collection/harmony/build/build.js', './components/chart-view/chart-view.html', './components/chart-view/harmony/build/build.js', './components/clickable-list-item/clickable-list-item.html', './components/clickable-list-item/harmony/build/build.js', './components/dom-selector/dom-selector.html', './components/dom-selector/harmony/build/build.js', './components/event-dispatcher/event-dispatcher.html', './components/event-dispatcher/harmony/build/build.js', './components/global-theme-values/global-theme-values.html', './components/global-theme-values/harmony/build/build.js', './components/hoodie-backend-adapter/harmony/build/build.js', './components/hoodie-backend-adapter/hoodie-backend-adapter.html', './components/hoodie-parse-adapter-plugin/harmony/build/build.js', './components/hoodie-parse-adapter-plugin/hoodie-parse-adapter-plugin.html', './components/iso-date-formatter/harmony/build/build.js', './components/iso-date-formatter/iso-date-formatter.html', './components/nomnow-app/harmony/build/build.js', './components/nomnow-app/nomnow-app.html', './components/nomnow-content-container/harmony/build/build.js', './components/nomnow-content-container/nomnow-content-container.html', './components/nomnow-desktop-content/harmony/build/build.js', './components/nomnow-desktop-content/nomnow-desktop-content.html', './components/nomnow-header/harmony/build/build.js', './components/nomnow-header/nomnow-header.html', './components/nomnow-icon-button/harmony/build/build.js', './components/nomnow-icon-button/nomnow-icon-button.html', './components/nomnow-logged-window/harmony/build/build.js', './components/nomnow-logged-window/nomnow-logged-window.html', './components/nomnow-login-form/harmony/build/build.js', './components/nomnow-login-form/nomnow-login-form.html', './components/nomnow-login-window/harmony/build/build.js', './components/nomnow-login-window/nomnow-login-window.html', './components/nomnow-phone-content/harmony/build/build.js', './components/nomnow-phone-content/nomnow-phone-content.html', './components/nomnow-scaffold/harmony/build/build.js', './components/nomnow-scaffold/nomnow-scaffold.html', './components/nomnow-tablet-content/harmony/build/build.js', './components/nomnow-tablet-content/nomnow-tablet-content.html', './components/nomnow-text-button/harmony/build/build.js', './components/nomnow-text-button/nomnow-text-button.html', './components/nomnow-theme-bright/nomnow-theme-bright.html', './components/open-closed-tag/harmony/build/build.js', './components/open-closed-tag/open-closed-tag.html', './components/order-detailed-view/harmony/build/build.js', './components/order-detailed-view/order-detailed-view.html', './components/order-explorer-view/harmony/build/build.js', './components/order-explorer-view/order-explorer-view.html', './components/pager-tile/harmony/build/build.js', './components/pager-tile/pager-tile.html', './components/popup-message/harmony/build/build.js', './components/popup-message/popup-message.html', './components/proto-service-worker/harmony/build/build.js', './components/proto-service-worker/proto-service-worker.html', './components/register-form/harmony/build/build.js', './components/register-form/register-form.html', './components/restaurant-explorer-view/harmony/build/build.js', './components/restaurant-explorer-view/restaurant-explorer-view.html', './components/ripple-button/harmony/build/build.js', './components/ripple-button/ripple-button.html', './components/ripple-effect/harmony/build/build.js', './components/ripple-effect/ripple-effect.html', './components/start-rating-bar/harmony/build/build.js', './components/start-rating-bar/start-rating-bar.html', './components/theme-style/harmony/build/build.js', './components/theme-style/theme-style.html', './components/web-animations-api/web-animations-api.html', './index.html']
	}, {
	    expectedCaches: {
	        get: function () {
	            return [this.appSourceCache, this.appStaticCache, this.appDynamicCache];
	        },
	        configurable: true,
	        enumerable: true
	    }
	});
	exports['default'] = CacheManifest;
	module.exports = exports['default'];

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	// Cannot feature-detect, as we have these implemented but they reject
	
	'use strict';
	
	var _Object$create = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/core-js/object/create\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	var _Promise = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"babel-runtime/core-js/promise\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))['default'];
	
	if (!Cache.prototype.add) {
	  Cache.prototype.add = function add(request) {
	    return this.addAll([request]);
	  };
	}
	
	if (!Cache.prototype.addAll) {
	  Cache.prototype.addAll = function addAll(requests) {
	    var cache = this;
	
	    // Since DOMExceptions are not constructable:
	    function NetworkError(message) {
	      this.name = 'NetworkError';
	      this.code = 19;
	      this.message = message;
	    }
	    NetworkError.prototype = _Object$create(Error.prototype);
	
	    return _Promise.resolve().then(function () {
	      if (arguments.length < 1) throw new TypeError();
	
	      // Simulate sequence<(Request or USVString)> binding:
	      var sequence = [];
	
	      requests = requests.map(function (request) {
	        if (request instanceof Request) {
	          return request;
	        } else {
	          return String(request); // may throw TypeError
	        }
	      });
	
	      return _Promise.all(requests.map(function (request) {
	        if (typeof request === 'string') {
	          request = new Request(request);
	        }
	
	        var scheme = new URL(request.url).protocol;
	
	        if (scheme !== 'http:' && scheme !== 'https:') {
	          throw new NetworkError('Invalid scheme');
	        }
	
	        return fetch(request.clone());
	      }));
	    }).then(function (responses) {
	      // TODO: check that requests don't overwrite one another
	      // (don't think this is possible to polyfill due to opaque responses)
	      return _Promise.all(responses.map(function (response, i) {
	        return cache.put(requests[i], response);
	      }));
	    }).then(function () {
	      return undefined;
	    });
	  };
	}
	
	if (!CacheStorage.prototype.match) {
	  // This is probably vulnerable to race conditions (removing caches etc)
	  CacheStorage.prototype.match = function match(request, opts) {
	    var caches = this;
	
	    return this.keys().then(function (cacheNames) {
	      var match;
	
	      return cacheNames.reduce(function (chain, cacheName) {
	        return chain.then(function () {
	          return match || caches.open(cacheName).then(function (cache) {
	            return cache.match(request, opts);
	          }).then(function (response) {
	            match = response;
	            return match;
	          });
	        });
	      }, _Promise.resolve());
	    });
	  };
	}
	
	module.exports = self.caches;

/***/ }
/******/ ]);
//# sourceMappingURL=service-worker.js.map