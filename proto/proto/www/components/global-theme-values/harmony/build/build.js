/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer global-theme-values custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	var valueTypesArray = ['color', 'url', 'src'];
	var themeValues = {};
	
	function getNameAttribute(element) {
	    if (!element.attributes || !element.attributes.name) {
	        throw new Error('global-theme-color must have a name attribute.');
	    }
	    return element.attributes.name.value;
	}
	
	Polymer('global-theme-values', {
	    created: function created() {
	        var children = undefined,
	            nameAttribute = undefined;
	
	        var _iteratorNormalCompletion = true;
	        var _didIteratorError = false;
	        var _iteratorError = undefined;
	
	        try {
	            for (var _iterator = valueTypesArray[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                var valueType = _step.value;
	
	                if ((children = this.getElementsByTagName('global-theme-' + valueType)).length > 0) {
	                    // Parse child nodes for style values
	                    themeValues[valueType] = {};
	
	                    for (var index = 0; index < children.length; index++) {
	                        if (themeValues.hasOwnProperty(nameAttribute = getNameAttribute(children[index]))) {
	                            throw new Error('Duplicated value ' + nameAttribute + ' in global-theme-values.');
	                        }
	                        themeValues[valueType][nameAttribute] = valueType == 'url' ? 'url(' + children[index].textContent + ')' : children[index].textContent;
	                    }
	                }
	            }
	        } catch (err) {
	            _didIteratorError = true;
	            _iteratorError = err;
	        } finally {
	            try {
	                if (!_iteratorNormalCompletion && _iterator['return']) {
	                    _iterator['return']();
	                }
	            } finally {
	                if (_didIteratorError) {
	                    throw _iteratorError;
	                }
	            }
	        }
	
	        this.themeValues = themeValues;
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map