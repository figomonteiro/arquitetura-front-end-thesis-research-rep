/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer proto-service-worker custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	var serializeUrlParams = function serializeUrlParams(params) {
	    return Object.keys(params).map(function (key) {
	        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
	    }).join('&');
	};
	Polymer('proto-service-worker', {
	    workerUrl: '/service-worker.js',
	    scope: '/',
	    preCacheUrls: ['/'],
	    unsupported: false,
	    ready: function ready() {
	        var serviceWorkerWithParams, registration;
	        return regeneratorRuntime.async(function ready$(context$1$0) {
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    if (!('serviceWorker' in navigator)) {
	                        context$1$0.next = 8;
	                        break;
	                    }
	
	                    serviceWorkerWithParams = this.workerUrl + '?' + serializeUrlParams({
	                        version: this.version,
	                        cacheurls: this.preCacheUrls
	                    });
	                    context$1$0.next = 4;
	                    return navigator.serviceWorker.register(serviceWorkerWithParams, {
	                        scope: this.scope
	                    });
	
	                case 4:
	                    registration = context$1$0.sent;
	
	                    if (registration) {
	                        console.log(registration);
	                    }
	                    context$1$0.next = 9;
	                    break;
	
	                case 8:
	                    this.unsupported = true;
	
	                case 9:
	                case 'end':
	                    return context$1$0.stop();
	            }
	        }, null, this);
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map