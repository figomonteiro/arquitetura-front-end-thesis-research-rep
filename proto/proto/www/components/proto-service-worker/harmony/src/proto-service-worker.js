/**
 *
 * Registration of polymer proto-service-worker custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let serializeUrlParams = (params) => {
    return Object.keys(params).map(function (key) {
        return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
    }).join('&');
}
Polymer('proto-service-worker', {
    workerUrl: "/service-worker.js",
    scope: "/",
    preCacheUrls: ['/'],
    unsupported: false,
    async ready() {
        if ('serviceWorker' in navigator) {
            let serviceWorkerWithParams = this.workerUrl + '?' + serializeUrlParams({
                    version: this.version,
                    cacheurls: this.preCacheUrls
                })
            let registration = await navigator.serviceWorker.register(serviceWorkerWithParams, {
                scope: this.scope
            })
            if (registration) {
                console.log(registration)
            }
        } else {
            this.unsupported = true;
        }
    }
})