import Model from "./models/Model"
/**
 * Registration of polymer backend-collection custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let checkRequiredAttributes = (name, backendAdapter, backendAdapterInterface) => {
    if (!name)
        throw new Error("backend-collection must receive a name attribute.")
    if (!backendAdapter)
        throw new Error("backend-collection must receive a backendAdapter attribute.")
    else if (backendAdapter.__proto__.__proto__ != backendAdapterInterface.__proto__)
        throw new Error("backendAdapter must implement BackendAdapterInterface")
}

// backend-collection element registration
Polymer('backend-collection', {
    collection: "model",
    requiredProperties: [],
    modelClass: {},
    domReady() {
        checkRequiredAttributes(this.name, this.backendAdapter, this.$.backendAdapterInterface)
        this.collection = this.name
        this.requiredProperties = this.requiredProperties.split(' ');

        let _this = this

        @Model.Required(_this.requiredProperties)
        @Model.Collection(_this.collection)
        class GenericModel extends Model {
            constructor(backendAdapter, object) {
                super(backendAdapter, object);
            }
        }
        this.modelClass = GenericModel
        this.onItemAddedHandler = event => _this.createObject(event.detail).upload()
        this.onItemDeletedHandler = event => event.detail.delete()
    },
    createObject(object) {
        return new this.modelClass(this.backendAdapter, object)
    }
})

// backend-collection-query element registration
Polymer('backend-collection-query', {
    result: [],
    domReady() {
        if (!this.collection)
            throw new Error("backend-collection-query must receive a collection attribute.")

        let _this = this

        this.collection.modelClass.findAll(_this.collection.backendAdapter)
            .then(result => _this.result = result)

        if (_this.noPersistence == undefined) {
            this.collection.modelClass.onAddToCollection(
                _this.collection.backendAdapter,
                    todo => {
                    if (!_this.getTodoFromList(todo))
                        _this.result.push(todo)
                    console.log("ADDED:", todo)
                })

            this.collection.modelClass.onRemoveFromCollection(
                _this.collection.backendAdapter,
                    result => {
                    _this.removeTodoFromList(result);
                    console.log("REMOVED", result)
                })
        }
    },
    removeTodoFromList(object) {
        for (let index = 0; index < this.result.length; index++)
            if (this.result[index].id == object.id)
                this.result.splice(index, 1)
    },
    getTodoFromList(object) {
        for (let instance of this.result)
            if (instance.id == object.id)
                return instance;
        return null;
    }
})
