/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer change-password-popup custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	var _this = null;
	
	Polymer("change-password-popup", {
	    password: "",
	    newPassword: "",
	    passwordConfirmation: "",
	    attached: function attached() {
	        _this = this;
	    },
	    changePassword: function changePassword() {
	        var messageObject = {
	            detail: {
	                message: "success"
	            }
	        };
	        if (_this.password == "" || _this.newPassword == "" || _this.passwordConfirmation == "") {
	            messageObject.detail.message = "write something";
	            _this.$.popup.messageHandler(messageObject);
	            return;
	        } else if (_this.newPassword != _this.passwordConfirmation) {
	            messageObject.detail.message = "passwords mismatch";
	            _this.$.popup.messageHandler(messageObject);
	            return;
	        }
	
	        _this.asyncFire("change-password", { password: _this.password, newPassword: _this.newPassword });
	    },
	    messageHandler: function messageHandler(event) {
	        _this.$.popup.messageHandler(event);
	    },
	    keypressHandler: function keypressHandler(event) {
	        if (event.keyCode == 13 && this.username != "" && this.password != "") {
	            this.changePassword();
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map