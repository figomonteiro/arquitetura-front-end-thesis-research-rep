/**
 *
 * Registration of polymer change-password-popup custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let _this = null

Polymer('change-password-popup', {
    password: "",
    newPassword: "",
    passwordConfirmation: "",
    attached() {
        _this = this
    },
    changePassword() {
        let messageObject = {
            detail: {
                message: "success"
            }
        }
        if (_this.password == "" || _this.newPassword == "" || _this.passwordConfirmation == "") {
            messageObject.detail.message = "write something"
            _this.$.popup.messageHandler(messageObject)
            return
        }
        else if (_this.newPassword != _this.passwordConfirmation) {
            messageObject.detail.message = "passwords mismatch"
            _this.$.popup.messageHandler(messageObject)
            return
        }

        _this.asyncFire('change-password', { password: _this.password, newPassword: _this.newPassword})
    },
    messageHandler(event) {
        _this.$.popup.messageHandler(event)
    },
    keypressHandler(event) {
        if (event.keyCode == 13 && this.username != "" && this.password != "") {
            this.changePassword()
        }
    }
})