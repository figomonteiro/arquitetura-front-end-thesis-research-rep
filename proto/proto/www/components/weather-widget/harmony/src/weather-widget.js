/**
 *
 * Registration of polymer weather-widget custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let getGeopositionCoords = () => {
    return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
                    geoposition => resolve(geoposition.coords),
                    error => reject(error),
                {
                    maximumAge: 60000,
                    timeout: 5000,
                    enableHighAccuracy: true
                }
            )
        }
    );
}
let saveLocationOnCache = (coords) => {
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem(
                "__last_geoposition_coords",
                JSON.stringify({
                    latitude: coords.latitude,
                    longitude: coords.longitude
                }));
        }
    },
    getCachedLocation = () => {
        if (typeof(Storage) !== "undefined") {
            return JSON.parse(localStorage.getItem("__last_geoposition_coords"))
        }
        else return null
    }
let buildOpenWeatherApiLink = (coords) => {
    return `http://api.openweathermap.org/data/2.5/weather?lat=${coords.latitude}&lon=${coords.longitude}`
}
Polymer('weather-widget', {
    hasResponse: false,
    description: "",
    minTemperature: 0,
    maxTemperature: 0,
    waitingLiveResults: true,
    error: false,
    loadingLabelMessage: "loading weather",
    connectionToOpenWeather: true,
    location: {},
    async created() {
        try {
            this.location = await getGeopositionCoords()
            this.apiUrl = buildOpenWeatherApiLink(this.location)
        } catch (exception) {
            this.error = true
            this.loadingLabelMessage = "couldn't get your location"
            this.errorHandler(null, exception)
        }
    },
    responseHandler(event) {
        this.hasResponse = true
        if (this.connectionToOpenWeather) {
            saveLocationOnCache(this.location)
        }

        if (event.type == "live-response") {
            this.waitingLiveResults = false
        }

        let response = event.detail.response
        this.location = response.name
        this.description = response.weather[0].main
        this.minTemperature = parseInt(response.main.temp_min - 273.15)
        this.maxTemperature = parseInt(response.main.temp_max - 273.15)
    },
    errorHandler(event, detail) {
        if (event && event.type == "live-request-error") {
            this.waitingLiveResults = false

            if (!this.hasResponse) {
                if (this.connectionToOpenWeather) {
                    this.connectionToOpenWeather = false

                    let cachedLocation = getCachedLocation()
                    if (cachedLocation) {
                        this.apiUrl = buildOpenWeatherApiLink(cachedLocation)
                    }
                } else {
                    this.loadingLabelMessage = "couldn't get weather information"
                    this.error = true
                }
            }
        }
    }
})