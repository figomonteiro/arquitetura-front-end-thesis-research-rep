/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer weather-widget custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	var getGeopositionCoords = function getGeopositionCoords() {
	    return new Promise(function (resolve, reject) {
	        navigator.geolocation.getCurrentPosition(function (geoposition) {
	            return resolve(geoposition.coords);
	        }, function (error) {
	            return reject(error);
	        }, {
	            maximumAge: 60000,
	            timeout: 5000,
	            enableHighAccuracy: true
	        });
	    });
	};
	var saveLocationOnCache = function saveLocationOnCache(coords) {
	    if (typeof Storage !== "undefined") {
	        localStorage.setItem("__last_geoposition_coords", JSON.stringify({
	            latitude: coords.latitude,
	            longitude: coords.longitude
	        }));
	    }
	},
	    getCachedLocation = function getCachedLocation() {
	    if (typeof Storage !== "undefined") {
	        return JSON.parse(localStorage.getItem("__last_geoposition_coords"));
	    } else {
	        return null;
	    }
	};
	var buildOpenWeatherApiLink = function buildOpenWeatherApiLink(coords) {
	    return "http://api.openweathermap.org/data/2.5/weather?lat=" + coords.latitude + "&lon=" + coords.longitude;
	};
	Polymer("weather-widget", {
	    hasResponse: false,
	    description: "",
	    minTemperature: 0,
	    maxTemperature: 0,
	    waitingLiveResults: true,
	    error: false,
	    loadingLabelMessage: "loading weather",
	    connectionToOpenWeather: true,
	    location: {},
	    created: function created() {
	        return regeneratorRuntime.async(function created$(context$1$0) {
	            while (1) switch (context$1$0.prev = context$1$0.next) {
	                case 0:
	                    context$1$0.prev = 0;
	                    context$1$0.next = 3;
	                    return getGeopositionCoords();
	
	                case 3:
	                    this.location = context$1$0.sent;
	
	                    this.apiUrl = buildOpenWeatherApiLink(this.location);
	                    context$1$0.next = 12;
	                    break;
	
	                case 7:
	                    context$1$0.prev = 7;
	                    context$1$0.t1 = context$1$0["catch"](0);
	
	                    this.error = true;
	                    this.loadingLabelMessage = "couldn't get your location";
	                    this.errorHandler(null, context$1$0.t1);
	
	                case 12:
	                case "end":
	                    return context$1$0.stop();
	            }
	        }, null, this, [[0, 7]]);
	    },
	    responseHandler: function responseHandler(event) {
	        this.hasResponse = true;
	        if (this.connectionToOpenWeather) {
	            saveLocationOnCache(this.location);
	        }
	
	        if (event.type == "live-response") {
	            this.waitingLiveResults = false;
	        }
	
	        var response = event.detail.response;
	        this.location = response.name;
	        this.description = response.weather[0].main;
	        this.minTemperature = parseInt(response.main.temp_min - 273.15);
	        this.maxTemperature = parseInt(response.main.temp_max - 273.15);
	    },
	    errorHandler: function errorHandler(event, detail) {
	        if (event && event.type == "live-request-error") {
	            this.waitingLiveResults = false;
	
	            if (!this.hasResponse) {
	                if (this.connectionToOpenWeather) {
	                    this.connectionToOpenWeather = false;
	
	                    var cachedLocation = getCachedLocation();
	                    if (cachedLocation) {
	                        this.apiUrl = buildOpenWeatherApiLink(cachedLocation);
	                    }
	                } else {
	                    this.loadingLabelMessage = "couldn't get weather information";
	                    this.error = true;
	                }
	            }
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map