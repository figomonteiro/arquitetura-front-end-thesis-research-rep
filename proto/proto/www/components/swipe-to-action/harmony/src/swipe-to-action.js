import {chan, go, alts, putAsync, filterFrom} from 'js-csp'
/**
 *
 * Registration of polymer swipe-to-action custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let listen = (element, type) => {
    let channel = chan();
    element.addEventListener(type, (event) => {
        putAsync(channel, event);
    });
    return channel;
}
Polymer('swipe-to-action', {
    dragElement: null,
    attached() {
        this.dragElement = this.parentNode.host || this.parentElement

        this.dragElement.__currentPosition__ = [0, 0]
        this.offsetLimit = this.offsetLimit || Infinity

        this.initDragEvents()
    },
    initDragEvents() {
        go(function*() {
            let touchMoveChannel = listen(this.dragElement, 'touchmove'),
                touchStartChannel = listen(this.dragElement, 'touchstart'),
                touchEndChannel = listen(this.dragElement, 'touchend'),
                dragging = false,
                touchPos = [0, 0],
                initialPosition = [0, 0],
                player = null,
                isFirstDragMove

            let updateDrag = () => {
                if (dragging) {
                    requestAnimationFrame(updateDrag);
                }
                this.dragElement.__currentPosition__[0] = touchPos[0] - initialPosition[0]
                this.dragElement.style.webkitTransform = 'translateX(' + this.dragElement.__currentPosition__[0] + 'px)'
            }

            while (true) {
                let v = yield alts([touchMoveChannel, touchStartChannel, touchEndChannel])
                let e = v.value

                if (v.channel === touchStartChannel) {
                    if (player) {
                        player.finish()
                        this.dragElement.__currentPosition__ = [0, 0]
                    }
                    initialPosition = [
                        e.targetTouches[0].clientX - this.dragElement.__currentPosition__[0],
                        e.targetTouches[0].clientY
                    ]
                    isFirstDragMove = true
                } else if (v.channel === touchMoveChannel) {
                    touchPos = [
                        e.targetTouches[0].clientX,
                        e.targetTouches[0].clientY
                    ]
                    if (isFirstDragMove) {
                        let diff = [touchPos[0] - initialPosition[0], touchPos[1] - initialPosition[1]]
                        diff = diff[1] / diff[0]

                        if ((diff >= 0 && diff < 1) || (diff < 0 && diff > -1)) {
                            dragging = true
                            requestAnimationFrame(updateDrag)
                        }
                        isFirstDragMove = false
                    }
                    if (dragging) {
                        e.preventDefault()
                    }
                } else if (v.channel === touchEndChannel && dragging) {
                    e.preventDefault()
                    dragging = false

                    if (((this.dragElement.__currentPosition__[0] < 0)
                            ? -this.dragElement.__currentPosition__[0]
                            : this.dragElement.__currentPosition__[0])
                        > (this.offsetLimit * this.dragElement.offsetWidth)) {
                        return this.asyncFire('swipe-result', this.dragElement.__currentPosition__)
                    }
                    (player = this.dragElement.animate(
                        [
                            {transform: 'translateX(' + this.dragElement.__currentPosition__[0] + 'px)'},
                            {transform: 'translateX(0)'}
                        ],
                        {
                            duration: 500,
                            easing: 'ease'
                        }
                    )).onfinish = () => {
                        this.dragElement.style.webkitTransform = 'translateX(0px)'
                    }
                }
            }
        }.bind(this))
    }
})