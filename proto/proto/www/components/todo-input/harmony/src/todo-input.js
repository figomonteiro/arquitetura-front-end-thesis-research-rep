Polymer('todo-input', {
    ready() {
        this.inputValue = "";
    },
    publish: {
        placeholder: "What needs to be done?"
    },
    keypressHandler(event) {
        if (event.keyCode == 13 && this.inputValue != "") {
            this.asyncFire('todo-created', {
                body: this.inputValue,
                checked: false
            });
            this.inputValue = "";
        }
    }
});