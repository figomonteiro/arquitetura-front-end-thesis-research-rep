/**
 * Registration of polymer proto-app custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('proto-app', {
    ready() {
        this.appTitle = "Prototype"
    }
})
