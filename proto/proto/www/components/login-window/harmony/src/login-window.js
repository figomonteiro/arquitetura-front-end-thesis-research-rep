Polymer("login-window", {
    page: 0,
    previousSelection: 0,
    domReady() {
        let _this = this

        this.registerButtonHandler = (event) => {
            _this.page = 1
        }
        this.loginButtonHandler = (event) => {
            _this.page = 0
        }
        this.errorHandler = (event) => {
            _this.errorMessage = event.detail.message
            _this.previousSelection = _this.page
            _this.page = 2
        }
        this.closeErrorHandler = () => {
            _this.page = _this.previousSelection
        }
    }
});