/**
 * Registration of polymer hoodie-backend-adapter custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('hoodie-backend-adapter', {
    // Static singleton adapter
    get adapter() {
        return this.constructor.adapter
    },
    set adapter(adapter) {
        if (!this.constructor.adapter)
            this.constructor.adapter = adapter
    },

    attached() {
        this.adapter = new Hoodie(this.url)

        this.session = {
            username: this.adapter.account.username,
            signUp: this.adapter.account.signUp,
            signIn: this.adapter.account.signIn,
            signOut: this.adapter.account.signOut,
            changePassword: this.adapter.account.changePassword,
            changeUsername: this.adapter.account.changeUsername,
            resetPassword: this.adapter.account.resetPassword,
            destroy: this.adapter.account.destroy,

            onSignUpOrSignIn: (callback) => {
                this.adapter.account.on('signin signup', callback)
            },
            onSignOut: (callback) => {
                this.adapter.account.on('signout', callback)
            },
            onUnauthenticatedError: (callback) => {
                this.adapter.account.on('error:unauthenticated', callback)
            }
        }
        this.store = {
            add: this.adapter.store.add,
            find: this.adapter.store.find,
            findOrAdd: this.adapter.store.findOrAdd,
            findAll: this.adapter.store.findAll,
            update: this.adapter.store.update,
            updateAll: this.adapter.store.updateAll,
            remove: this.adapter.store.remove,
            removeAll: this.adapter.store.removeAll,

            onAdd: (collection, callback) => {
                this.adapter.store.on(collection + ':add', callback)
                // BUG HOODIE: add event sometimes activated as an update event
                this.adapter.store.on(collection + ':update', result => {
                    if (result.createdAt == result.updatedAt)
                        callback(result)
                })
            },
            onItemUpdated: (collection, id, callback) => {
                this.adapter.store.on(collection + ':' + id + ':update', callback)
            },
            onUpdate: (collection, callback) => {
                this.adapter.store.on(collection + ':update', callback)
            },
            onItemRemoved: (collection, id, callback) => {
                this.adapter.store.on(collection + ':' + id + ':remove', callback)
            },
            onRemove: (collection, callback) => {
                this.adapter.store.on(collection + ':remove', callback)
            }
        }

        this.session.onSignUpOrSignIn(user => {
            this.session.username = user
        })

        this.session.onSignOut(result => {
            this.session.username = null
        })

        let _this = this
        this.onLoginHandler = (event) => {
            _this.session.signIn(event.detail.username, event.detail.password)
                .catch(error => _this.asyncFire('hoodie-message', { message: error.message }))
        }
        this.onRegisterHandler = (event) => {
            _this.session.signUp(event.detail.username, event.detail.password)
                .catch(error => _this.asyncFire('hoodie-message', { message: error.message }))
        }
        this.onChangePasswordHandler = async (event) => {
            try {
                await _this.session.changePassword(event.detail.password, event.detail.newPassword)
                _this.asyncFire('hoodie-message', {
                    success: true,
                    message: 'Password changed successfully'
                })
            } catch (error) {
                _this.asyncFire('hoodie-message', { message: error.message })
            }
        }
    }
})