/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Registration of polymer hoodie-backend-adapter custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('hoodie-backend-adapter', Object.defineProperties({
	
	    attached: function attached() {
	        var _this2 = this;
	
	        this.adapter = new Hoodie(this.url);
	
	        this.session = {
	            username: this.adapter.account.username,
	            signUp: this.adapter.account.signUp,
	            signIn: this.adapter.account.signIn,
	            signOut: this.adapter.account.signOut,
	            changePassword: this.adapter.account.changePassword,
	            changeUsername: this.adapter.account.changeUsername,
	            resetPassword: this.adapter.account.resetPassword,
	            destroy: this.adapter.account.destroy,
	
	            onSignUpOrSignIn: function onSignUpOrSignIn(callback) {
	                _this2.adapter.account.on('signin signup', callback);
	            },
	            onSignOut: function onSignOut(callback) {
	                _this2.adapter.account.on('signout', callback);
	            },
	            onUnauthenticatedError: function onUnauthenticatedError(callback) {
	                _this2.adapter.account.on('error:unauthenticated', callback);
	            }
	        };
	        this.store = {
	            add: this.adapter.store.add,
	            find: this.adapter.store.find,
	            findOrAdd: this.adapter.store.findOrAdd,
	            findAll: this.adapter.store.findAll,
	            update: this.adapter.store.update,
	            updateAll: this.adapter.store.updateAll,
	            remove: this.adapter.store.remove,
	            removeAll: this.adapter.store.removeAll,
	
	            onAdd: function onAdd(collection, callback) {
	                _this2.adapter.store.on(collection + ':add', callback);
	                // BUG HOODIE: add event sometimes activated as an update event
	                _this2.adapter.store.on(collection + ':update', function (result) {
	                    if (result.createdAt == result.updatedAt) callback(result);
	                });
	            },
	            onItemUpdated: function onItemUpdated(collection, id, callback) {
	                _this2.adapter.store.on(collection + ':' + id + ':update', callback);
	            },
	            onUpdate: function onUpdate(collection, callback) {
	                _this2.adapter.store.on(collection + ':update', callback);
	            },
	            onItemRemoved: function onItemRemoved(collection, id, callback) {
	                _this2.adapter.store.on(collection + ':' + id + ':remove', callback);
	            },
	            onRemove: function onRemove(collection, callback) {
	                _this2.adapter.store.on(collection + ':remove', callback);
	            }
	        };
	
	        this.session.onSignUpOrSignIn(function (user) {
	            _this2.session.username = user;
	        });
	
	        this.session.onSignOut(function (result) {
	            _this2.session.username = null;
	        });
	
	        var _this = this;
	        this.onLoginHandler = function (event) {
	            _this.session.signIn(event.detail.username, event.detail.password)['catch'](function (error) {
	                return _this.asyncFire('hoodie-message', { message: error.message });
	            });
	        };
	        this.onRegisterHandler = function (event) {
	            _this.session.signUp(event.detail.username, event.detail.password)['catch'](function (error) {
	                return _this.asyncFire('hoodie-message', { message: error.message });
	            });
	        };
	        this.onChangePasswordHandler = function callee$1$0(event) {
	            return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
	                while (1) switch (context$2$0.prev = context$2$0.next) {
	                    case 0:
	                        context$2$0.prev = 0;
	                        context$2$0.next = 3;
	                        return _this.session.changePassword(event.detail.password, event.detail.newPassword);
	
	                    case 3:
	                        _this.asyncFire('hoodie-message', {
	                            success: true,
	                            message: 'Password changed successfully'
	                        });
	                        context$2$0.next = 9;
	                        break;
	
	                    case 6:
	                        context$2$0.prev = 6;
	                        context$2$0.t0 = context$2$0['catch'](0);
	
	                        _this.asyncFire('hoodie-message', { message: context$2$0.t0.message });
	
	                    case 9:
	                    case 'end':
	                        return context$2$0.stop();
	                }
	            }, null, _this2, [[0, 6]]);
	        };
	    }
	}, {
	    adapter: {
	        // Static singleton adapter
	
	        get: function () {
	            return this.constructor.adapter;
	        },
	        set: function (adapter) {
	            if (!this.constructor.adapter) this.constructor.adapter = adapter;
	        },
	        configurable: true,
	        enumerable: true
	    }
	}));

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map