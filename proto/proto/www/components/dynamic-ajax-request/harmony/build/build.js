/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer dynamic-ajax-request custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('dynamic-ajax-request', {
	    response: null,
	    error: null,
	    liveResponseHandler: function liveResponseHandler(event) {
	        event.stopImmediatePropagation();
	        this.response = event.detail;
	        this.asyncFire('live-response', event.detail);
	    },
	    cacheResponseHandler: function cacheResponseHandler(event) {
	        event.stopImmediatePropagation();
	        if (!this.response) {
	            this.response = event.detail;
	            this.asyncFire('cache-response', event.detail);
	        }
	    },
	    liveErrorHandler: function liveErrorHandler(event) {
	        event.stopImmediatePropagation();
	        this.error = event.detail;
	        if (!this.response && this.error != null) {
	            this.error = event.detail;
	            this.fire('live-request-error', this.error);
	        }
	    },
	    cacheErrorHandler: function cacheErrorHandler(event) {
	        event.stopImmediatePropagation();
	        if (!this.response) {
	            this.error = event.detail;
	            this.fire('cache-request-error', this.error);
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map