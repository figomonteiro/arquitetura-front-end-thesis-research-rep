/**
 *
 * Registration of polymer dynamic-ajax-request custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('dynamic-ajax-request', {
    response: null,
    error: null,
	liveResponseHandler(event) {
        event.stopImmediatePropagation()
        this.response = event.detail
        this.asyncFire('live-response', event.detail)

    },
    cacheResponseHandler(event) {
        event.stopImmediatePropagation()
        if (!this.response) {
            this.response = event.detail
            this.asyncFire('cache-response', event.detail)
        }
    },
    liveErrorHandler(event) {
        event.stopImmediatePropagation()
        this.error = event.detail
        if (!this.response && this.error != null) {
            this.error = event.detail
            this.fire("live-request-error", this.error)
        }
    },
    cacheErrorHandler(event) {
        event.stopImmediatePropagation()
        if (!this.response) {
            this.error = event.detail
            this.fire("cache-request-error", this.error)
        }
    }
})