/**
 *
 * Registration of polymer proto-button-upload custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('proto-button-upload', {
    fireUploadEvent(event) {
        this.asyncFire('file-selected', {
            file: event.target.files[0]
        })
    }
})