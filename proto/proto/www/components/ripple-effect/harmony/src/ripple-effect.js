/**
 *
 * Registration of polymer ripple-effect custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('ripple-effect', {
    maxScale: 15,
    publish: {
        recenteringTouch: false
    },
    bubbleAnimationPlayer: null,
    async touchingHandler(event) {
        let bubble = document.createElement('div');
        bubble.setAttribute('class', 'bubble');
        this.$.background.appendChild(bubble);

        let elementRectangle = this.getBoundingClientRect(),
            resizeRatio = ((elementRectangle.width > elementRectangle.height)
                    ? elementRectangle.width : elementRectangle.height) / 20;
        this.bubbleAnimationPlayer = this.bubbleAnimation(
            bubble,
            event.x - elementRectangle.left,
            event.y - elementRectangle.top,
            (resizeRatio > this.maxScale) ? this.maxScale : resizeRatio
        )
        await this.bubbleAnimationPlayer.finishedPromise
        await this.bubbleDisappearAnimation().finishedPromise
        this.$.background.removeChild(bubble)
    },
    stopTouchingHandler() {
        this.bubbleAnimationPlayer.finish()
    },
    bubbleAnimation(bubble, startX, startY, resizeRatio) {
        let endX = (this.recenteringTouch) ? this.offsetWidth / 2 : startX,
            endY = (this.recenteringTouch) ? this.offsetHeight / 2 : startY
        let bubbleAnimParams = {
                initialState: {
                    transform: 'translate3d(' + startX + 'px,' + startY + 'px, 0) ' +
                    'scale3d(1, 1, 1) ',
                    opacity: 1
                },
                finalState: {
                    transform: 'translate3d(' + endX + 'px,' + endY + 'px, 0) ' +
                    'scale3d(' + resizeRatio + ', ' + resizeRatio + ', 1)',
                    opacity: 0.2
                }
            },
            backgroundAnimParams = {
                initialState: {
                    opacity: 0
                },
                finalState: {
                    opacity: 1
                }
            }

        let bubbling = new KeyframeEffect(
            bubble,
            [bubbleAnimParams.initialState, bubbleAnimParams.finalState],
            {duration: 600, fill: 'forwards', easing: 'ease'}
        )
        let backgroundFade = new KeyframeEffect(
            this.$.background,
            [backgroundAnimParams.initialState, backgroundAnimParams.finalState],
            {duration: 400, fill: 'forwards', easing: 'ease'}
        )
        let player = document.timeline.play(new GroupEffect([bubbling, backgroundFade]))
        player.finishedPromise = new Promise(function(resolve) {
            player.onfinish = (playerEvent) => {
                resolve(playerEvent)
            }
        })
        return player
    },
    bubbleDisappearAnimation() {
        let player = this.$.background.animate(
            [{opacity: 1}, {opacity: 0}],
            {duration: 500, fill: 'forwards', easing: 'ease-out'}
        )
        player.finishedPromise = new Promise(function(resolve) {
            player.onfinish = (playerEvent) => resolve(playerEvent)
        })
        return player
    }
})