/**
 *
 * Registration of polymer main-panel custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('main-panel', {
	attached() {
        this.$.headerBg.appendChild(this.$.headerBgPic)
        this.$.headerBgPic.removeAttribute('hidden')
    }
})