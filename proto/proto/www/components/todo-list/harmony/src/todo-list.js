// TODO DEAL WITH THE this THING

let _this = null
let findModelInDom = (elem, list) => {
    for (let index = 1; index < list.length; index++) {
        if (elem === list[index].model) {
            return list[index]
        }
    } return undefined
}
let runningAnimationsCounter = 0,
    todosBeingDeleted = []
Polymer('todo-list', {
    created() {
        _this = this
        this.bgColor = "#efefef"
    },
    listChanged(oldValue, newValue) {
        if (newValue) {
            return this.todos = this.list.slice(0)
        } else {
            let removedTrigger = false

            for (let change of oldValue) {
                if (change.removed.length > 0) {
                    removedTrigger = true

                    for (let removed of change.removed) {
                        let deletedElement = findModelInDom(removed, _this.$.todoWrapper.children)
                        if (deletedElement) {
                            runningAnimationsCounter++
                            let beingDeletedIndex = todosBeingDeleted.indexOf(removed)
                            if (beingDeletedIndex == -1) {
                                deletedElement.deleteAnimation().onfinish = () => {
                                    runningAnimationsCounter--
                                    if (runningAnimationsCounter == 0) {
                                        _this.todos = _this.list.slice(0)
                                    }
                                }
                            } else {
                                runningAnimationsCounter--
                                todosBeingDeleted.slice(beingDeletedIndex, 1)
                                _this.todos = _this.list.slice(0)
                            }
                        }
                    }
                }
            }

            if (!removedTrigger) {
                _this.todos = _this.list.slice(0)
            }
        }
    },
    todoDeletedHandler(event) {
        if (findModelInDom(event.detail, _this.$.todoWrapper.children).currentPosition[0] != 0) {
            todosBeingDeleted.push(event.detail)
        }
    }
});