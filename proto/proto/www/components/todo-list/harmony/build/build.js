/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	// TODO DEAL WITH THE this THING
	
	"use strict";
	
	var _this = null;
	var findModelInDom = function findModelInDom(elem, list) {
	    for (var index = 1; index < list.length; index++) {
	        if (elem === list[index].model) {
	            return list[index];
	        }
	    }return undefined;
	};
	var runningAnimationsCounter = 0,
	    todosBeingDeleted = [];
	Polymer("todo-list", {
	    created: function created() {
	        _this = this;
	        this.bgColor = "#efefef";
	    },
	    listChanged: function listChanged(oldValue, newValue) {
	        if (newValue) {
	            return this.todos = this.list.slice(0);
	        } else {
	            var removedTrigger = false;
	
	            var _iteratorNormalCompletion = true;
	            var _didIteratorError = false;
	            var _iteratorError = undefined;
	
	            try {
	                for (var _iterator = oldValue[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                    var change = _step.value;
	
	                    if (change.removed.length > 0) {
	                        removedTrigger = true;
	
	                        var _iteratorNormalCompletion2 = true;
	                        var _didIteratorError2 = false;
	                        var _iteratorError2 = undefined;
	
	                        try {
	                            for (var _iterator2 = change.removed[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                                var removed = _step2.value;
	
	                                var deletedElement = findModelInDom(removed, _this.$.todoWrapper.children);
	                                if (deletedElement) {
	                                    runningAnimationsCounter++;
	                                    var beingDeletedIndex = todosBeingDeleted.indexOf(removed);
	                                    if (beingDeletedIndex == -1) {
	                                        deletedElement.deleteAnimation().onfinish = function () {
	                                            runningAnimationsCounter--;
	                                            if (runningAnimationsCounter == 0) {
	                                                _this.todos = _this.list.slice(0);
	                                            }
	                                        };
	                                    } else {
	                                        runningAnimationsCounter--;
	                                        todosBeingDeleted.slice(beingDeletedIndex, 1);
	                                        _this.todos = _this.list.slice(0);
	                                    }
	                                }
	                            }
	                        } catch (err) {
	                            _didIteratorError2 = true;
	                            _iteratorError2 = err;
	                        } finally {
	                            try {
	                                if (!_iteratorNormalCompletion2 && _iterator2["return"]) {
	                                    _iterator2["return"]();
	                                }
	                            } finally {
	                                if (_didIteratorError2) {
	                                    throw _iteratorError2;
	                                }
	                            }
	                        }
	                    }
	                }
	            } catch (err) {
	                _didIteratorError = true;
	                _iteratorError = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion && _iterator["return"]) {
	                        _iterator["return"]();
	                    }
	                } finally {
	                    if (_didIteratorError) {
	                        throw _iteratorError;
	                    }
	                }
	            }
	
	            if (!removedTrigger) {
	                _this.todos = _this.list.slice(0);
	            }
	        }
	    },
	    todoDeletedHandler: function todoDeletedHandler(event) {
	        if (findModelInDom(event.detail, _this.$.todoWrapper.children).currentPosition[0] != 0) {
	            todosBeingDeleted.push(event.detail);
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map