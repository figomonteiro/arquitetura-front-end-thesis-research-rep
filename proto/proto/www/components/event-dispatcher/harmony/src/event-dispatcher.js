/**
 *
 * Registration of polymer event-dispatcher custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let parentNode = null,
    callback = null
Polymer('event-dispatcher', {
    attached() {
        let _this = this
        parentNode = this.parentNode
        callback = (event) => {
            try {
                _this.handler(event)
            } catch (exception) {
                console.log(exception)
            } finally {
                if (_this.keepAlive === undefined) {
                    event.stopImmediatePropagation()
                }
            }
        }
        parentNode.addEventListener(this.event, callback)
    },
    detached() {
        parentNode.removeEventListener(this.event, callback)
    }
})