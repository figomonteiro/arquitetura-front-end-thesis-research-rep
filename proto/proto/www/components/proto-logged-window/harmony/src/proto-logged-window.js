/**
 *
 * Registration of polymer proto-logged-window custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('proto-logged-window', {
    showChangePasswordPopup: false,
    domReady() {
        this.toggleChangePasswordPopup = () => {
            this.showChangePasswordPopup = !this.showChangePasswordPopup
        }
    }
})