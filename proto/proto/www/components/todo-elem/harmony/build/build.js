/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Registration of polymer proto-app custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	Polymer("todo-elem", {
	    currentPosition: [0, 0],
	    model: {
	        body: "default",
	        checked: false
	    },
	    observe: {
	        "model.checked": "updateChecked",
	        "model.body": "updateBody"
	    },
	    changeHandler: function changeHandler(event) {
	        this.model.checked = event.target.checked;
	    },
	    swipeDeleteHandler: function swipeDeleteHandler(event) {
	        var _this = this;
	
	        this.currentPosition[0] = event.detail[0];
	        return this.deleteAnimation().onfinish = function () {
	            _this.fire("todo-deleted", _this.model);
	        };
	    },
	    deleteHandler: function deleteHandler() {
	        return this.fire("todo-deleted", this.model);
	    },
	    updateChecked: function updateChecked(oldValue, newValue) {
	        this.$.checkbox.checked = newValue;
	    },
	    updateBody: function updateBody(oldValue, newValue) {
	        this.$.body.textContent = newValue;
	    },
	    deleteAnimation: function deleteAnimation() {
	        var _this = this;
	        var slide = new KeyframeEffect(_this, [{ transform: "translateX(" + _this.currentPosition[0] + "px)", opacity: 1 }, { transform: "translateX(" + (_this.currentPosition[0] < 0 ? "-" : "") + "110%)", opacity: 0.5 }], {
	            duration: 400,
	            fill: "forwards",
	            easing: "ease-out"
	        });
	        var scale = new KeyframeEffect(_this, [{ height: _this.offsetHeight + "px" }, { height: 0 }], {
	            duration: 400,
	            fill: "forwards",
	            easing: "ease-out"
	        });
	        return document.timeline.play(new SequenceEffect([slide, scale]));
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map