/**
 * Registration of polymer proto-app custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer("todo-elem", {
    currentPosition: [0, 0],
    model: {
        body: "default",
        checked: false
    },
    observe: {
        "model.checked": "updateChecked",
        "model.body": "updateBody"
    },
    changeHandler(event) {
        this.model.checked = event.target.checked
    },
    swipeDeleteHandler(event) {
        this.currentPosition[0] = event.detail[0]
        return this.deleteAnimation().onfinish = () => {
            this.fire("todo-deleted", this.model);
        }
    },
    deleteHandler() {
        return this.fire("todo-deleted", this.model);
    },
    updateChecked(oldValue, newValue) {
        this.$.checkbox.checked = newValue
    },
    updateBody(oldValue, newValue) {
        this.$.body.textContent = newValue
    },
    deleteAnimation() {
        let _this = this
        let slide = new KeyframeEffect(
            _this,
            [
                {transform: 'translateX(' + _this.currentPosition[0] + 'px)', opacity: 1},
                {transform: 'translateX(' + ((_this.currentPosition[0] < 0) ? '-' : '') + '110%)', opacity: 0.5}
            ],
            {
                duration: 400,
                fill: 'forwards',
                easing: 'ease-out'
            }
        )
        let scale = new KeyframeEffect(
            _this,
            [
                {height: _this.offsetHeight + 'px'},
                {height: 0}
            ],
            {
                duration: 400,
                fill: 'forwards',
                easing: 'ease-out'
            }
        )
        return document.timeline.play(new SequenceEffect([slide, scale]))
    }
});