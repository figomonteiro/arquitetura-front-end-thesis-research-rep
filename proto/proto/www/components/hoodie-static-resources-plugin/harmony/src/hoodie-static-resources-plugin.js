/**
 *
 * Registration of polymer hoodie-static-resources-plugin custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('hoodie-static-resources-plugin', {
	attached() {
        let _this = this

        this.hoodieAdapter = this.parentElement
        if (!this.hoodieAdapter && this.hoodieAdapter.tagName == 'HOODIE-BACKEND-ADAPTER') {
            throw new Error('hoodie-static-resources-plugin element must be ' +
                'used nested with a hoodie-backend-adapter element')
        }
        this.hoodieAdapter.__proto__.static = {
            // Receives the File object to upload as argument
            publish: this.hoodieAdapter.adapter.static.publish,
            // Receives the File object to upload as argument
            uploadToMyCollection: this.hoodieAdapter.adapter.static.uploadToMyCollection,
            // Receives the asset's url as argument
            deleteAsset: this.hoodieAdapter.adapter.static.deleteAsset,
            // Event handlers for interacting with other custom elements
            onPublishAssetHandler(event) {
                _this.hoodieAdapter.static.publish(event.detail.file)
                    .then(result => {
                        _this.asyncFire('asset-published', {url: result})
                    })
                    .catch(exception => _this.asyncFire('asset-upload-error', {error: exception}))
            }
        }
    }
})