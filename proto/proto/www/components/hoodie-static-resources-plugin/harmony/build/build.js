/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer hoodie-static-resources-plugin custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	Polymer('hoodie-static-resources-plugin', {
	    attached: function attached() {
	        var _this = this;
	
	        this.hoodieAdapter = this.parentElement;
	        if (!this.hoodieAdapter && this.hoodieAdapter.tagName == 'HOODIE-BACKEND-ADAPTER') {
	            throw new Error('hoodie-static-resources-plugin element must be ' + 'used nested with a hoodie-backend-adapter element');
	        }
	        this.hoodieAdapter.__proto__['static'] = {
	            // Receives the File object to upload as argument
	            publish: this.hoodieAdapter.adapter['static'].publish,
	            // Receives the File object to upload as argument
	            uploadToMyCollection: this.hoodieAdapter.adapter['static'].uploadToMyCollection,
	            // Receives the asset's url as argument
	            deleteAsset: this.hoodieAdapter.adapter['static'].deleteAsset,
	            // Event handlers for interacting with other custom elements
	            onPublishAssetHandler: function onPublishAssetHandler(event) {
	                _this.hoodieAdapter['static'].publish(event.detail.file).then(function (result) {
	                    _this.asyncFire('asset-published', { url: result });
	                })['catch'](function (exception) {
	                    return _this.asyncFire('asset-upload-error', { error: exception });
	                });
	            }
	        };
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map