Polymer("login-form", {
    ready() {
        this.username = "";
        this.password = "";
    },
    login() {
        this.fire('login', {
            username: this.username,
            password: this.password
        });
        this.username = "";
        this.password = "";
    },
    register() {
        this.fire('register-button-tapped', {})
    },
    keypressHandler(event) {
        if (event.keyCode == 13 && this.username != "" && this.password != "") {
            this.login()
        }
    }
});