/**
 *
 * Registration of polymer generic-popup custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
let _this = {}
Polymer('generic-popup', {
    page: 0,
    previousSelection: 0,
    domReady() {
        _this = this

        this.close = () => {
            _this.asyncFire('popup-close')
            event.stopImmediatePropagation()
        }
        this.ignoreTap = (event) => {
            event.stopImmediatePropagation()
        }
        this.closeMessageHandler = (event) => {
            if (event.detail.success) {
                _this.asyncFire('popup-close')
                return
            }
            _this.page = _this.previousSelection
        }
        this.messageHandler = (event) => {
            _this.message = event.detail.message
            _this.success = event.detail.success

            _this.previousSelection = _this.page
            _this.page = 1
        }
    }
})