/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer generic-popup custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	'use strict';
	
	var _this = {};
	Polymer('generic-popup', {
	    page: 0,
	    previousSelection: 0,
	    domReady: function domReady() {
	        _this = this;
	
	        this.close = function () {
	            _this.asyncFire('popup-close');
	            event.stopImmediatePropagation();
	        };
	        this.ignoreTap = function (event) {
	            event.stopImmediatePropagation();
	        };
	        this.closeMessageHandler = function (event) {
	            if (event.detail.success) {
	                _this.asyncFire('popup-close');
	                return;
	            }
	            _this.page = _this.previousSelection;
	        };
	        this.messageHandler = function (event) {
	            _this.message = event.detail.message;
	            _this.success = event.detail.success;
	
	            _this.previousSelection = _this.page;
	            _this.page = 1;
	        };
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map