/**
 *
 * Registration of polymer register-form custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('register-form', {
	ready() {
        this.username = ""
        this.password = ""
        this.passwordConfirmation = ""
    },
    login() {
    	this.fire('login-button-tapped', {})
    },
    register() {
    	if (this.password != this.passwordConfirmation) {
    		this.fire('error-message', { message: "passwords mismatch" })
    		return
    	} else if (this.password.length < 6) {
    		this.fire('error-message', { message: "password must contain at least 6 characters" })
    		return
    	}

        this.fire('register', {
            username: this.username,
            password: this.password,
        });
        this.username = ""
        this.password = ""
        this.passwordConfirmation = ""
    },
    keypressHandler(event) {
        if (event.keyCode == 13 && this.username != "" && this.password != "") {
            this.register()
        }
    }
})