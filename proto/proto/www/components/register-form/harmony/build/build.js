/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * Registration of polymer register-form custom element.
	 * ECMAScript 6 - using BabelJS traspiler.
	 */
	"use strict";
	
	Polymer("register-form", {
	    ready: function ready() {
	        this.username = "";
	        this.password = "";
	        this.passwordConfirmation = "";
	    },
	    login: function login() {
	        this.fire("login-button-tapped", {});
	    },
	    register: function register() {
	        if (this.password != this.passwordConfirmation) {
	            this.fire("error-message", { message: "passwords mismatch" });
	            return;
	        } else if (this.password.length < 6) {
	            this.fire("error-message", { message: "password must contain at least 6 characters" });
	            return;
	        }
	
	        this.fire("register", {
	            username: this.username,
	            password: this.password });
	        this.username = "";
	        this.password = "";
	        this.passwordConfirmation = "";
	    },
	    keypressHandler: function keypressHandler(event) {
	        if (event.keyCode == 13 && this.username != "" && this.password != "") {
	            this.register();
	        }
	    }
	});

/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map