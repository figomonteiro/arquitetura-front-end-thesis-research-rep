/**
 *
 * Registration of polymer proto-button custom element.
 * ECMAScript 6 - using BabelJS traspiler.
 */
Polymer('proto-button', {
    publish: {
        text: "BUTTON"
    }
})