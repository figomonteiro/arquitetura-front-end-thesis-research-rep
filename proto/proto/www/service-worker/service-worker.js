import CacheManifest from './cache-manifest'
let caches = require('./cache-polyfill')

let DEFAULT_VERSION = 1

let deserializeUrlParams = (queryString) => {
        return new Map(queryString.split('&').map(function (keyValuePair) {
            return keyValuePair.split('=').map(decodeURIComponent)
        }))
    },
    init = (() => {
        let params = deserializeUrlParams(location.search.substring(1))
        // Allow some defaults to be overridden via URL parameters.
        let version = params.has('version') ? params.get('version') : DEFAULT_VERSION,
            preCacheUrls = params.has('cacheurls') ? params.get('cacheurls').split(',') : []

        CacheManifest.appSourceCache += version
        CacheManifest.cacheFilesArray = CacheManifest.cacheFilesArray.concat(preCacheUrls)
    })()

self.oninstall = (event) => {
    event.waitUntil((async() => {
        let cache = await caches.open(CacheManifest.appSourceCache)
        return await cache.addAll(CacheManifest.cacheFilesArray)
    })())
}

self.onactivate = (event) => {
    event.waitUntil((async() => {
        let cacheNames = await caches.keys()
        return Promise.all(
            cacheNames.map(function (cacheName) {
                if (CacheManifest.expectedCaches.indexOf(cacheName) == -1) {
                    return caches.delete(cacheName);
                }
            })
        )
    })())
}

self.onfetch = async(event) => {
    let requestURL = new URL(event.request.url)

    if (requestURL.hostname != location.hostname) {
        event.respondWith(dynamicResourceResponse(event.request, CacheManifest.appDynamicCache))
    }
    else if (/\/static_resources\//.test(requestURL.pathname)) {
        event.respondWith(staticResourceResponse(event.request, CacheManifest.appStaticCache))
    }
    else if (/\/_api\/(?!_files\/)/.test(requestURL.pathname)) {
        return await fetch(event.request)
    }
    else if (/components\//.test(requestURL.pathname)) {
        event.respondWith(staticResourceResponse(event.request, CacheManifest.appSourceCache))
    }
    else {
        event.respondWith(cacheResponse(event.request))
    }
};

let cacheResponse = async(request) => {
    return await caches.match(request, {
        ignoreVary: true
    })
}

let dynamicResourceResponse = async(request, cacheName) => {
    let response = null

    if (request.headers.get('Accept') == 'x-cache/only') {
        response = await caches.match(request)
        if (response) {
            // LOG
            logCacheRequest(request.url)
            return response
        }
    }

    response = await fetch(request.clone())
    // LOG
    logServerRequest(request.url)
    let cache = await caches.open(cacheName)
    await cache.put(request, response.clone())
    return response.clone()
}

let staticResourceResponse = async(request, cacheName) => {
    let response = await caches.match(request)
    if (response) {
        // LOG
        logCacheRequest(request.url)
        return response
    }

    response = await fetch(request.clone())
    // LOG
    logServerRequest(request.url)
    let cache = await caches.open(cacheName)
    await cache.put(request, response.clone())
    return response.clone()
}

let logCacheRequest = (url, cacheName) => {
        console.log('%cCache %c%s\n%c%s',
            'font-weight: bold; color: #333',
            'font-weight: normal; color: #777', cacheName,
            'font-style: italic', url)
    },
    logServerRequest = (url) => {
        console.log('%cServer\n%c%s',
            'font-weight: bold; color: #d35400', 'font-style: italic', url)
    }
