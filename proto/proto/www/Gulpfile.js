var gulp = require("gulp"),
    glob = require("glob"),
    browserify = require("browserify"),
    babelify = require("babelify"),
    babel = require("gulp-babel"),
    uglify = require("gulp-uglify"),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    sourcemaps = require("gulp-sourcemaps"),
    concat = require("gulp-concat"),
    clean = require('gulp-clean'),
    srcDir = './components/**/harmony/src/**/*',
    buildDir = './components/**/harmony/build/',
    webpack = require('webpack'),
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    gutil = require('gulp-util'),
    template = require('gulp-template')

gulp.task('clean', function () {
    return gulp.src(buildDir, {read: false})
        .pipe(clean({
            force: true
        }))
})

gulp.task('generate-sw-cachelist-file', function () {
    var globPattern = './{{components,bower_components}/*/'
        + '{harmony/build/build.js,!(demo|index|metadata).{html,css,js}}'
        + ',cache-array.js,index.html,service-service-worker.js}'

    gulp.src('./service-worker/cache-manifest._template')
        .pipe(template({
            fileList: JSON.stringify(glob.sync(globPattern))
        }))
        .pipe(rename("cache-manifest.js"))
        //.pipe(babel())
        //.pipe(uglify())
        .pipe(gulp.dest('./service-worker/'))
})

gulp.task('browserify-watch', function () {
    gulp.watch([srcDir], ['browserify-build'])
})

gulp.task('browserify-build', function () {
    var entryFiles = glob.sync('./components/**/harmony/src/*.js')
    var bundler = null, path = null

    for (var file of entryFiles) {
        path = file.split("src")[0] + "build/"
        bundler = browserify({
            entries: file,
            debug: true
        })
            .transform(babelify.configure({
                stage: 0
            }))
            .bundle()
            .pipe(source('build.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            //.pipe(uglify())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(path))
    }
})

// Webpack bundling test
var babelLoader = 'babel?stage=0'

function onBuild(err, stats) {
    if (err) {
        throw new Error(err);
    }
    if (stats.compilation.errors.length > 0) {
        for (var error of stats.compilation.errors) {
            gutil.log(error.message)
        }
        gutil.log(
            gutil.colors.red.bold('Compilation failed'),
            gutil.colors.dim('after', stats.endTime - stats.startTime, 'ms')
        )
        return
    }
    gutil.log(
        gutil.colors.cyan.bold('Compiled with success'),
        gutil.colors.dim('after', stats.endTime - stats.startTime, 'ms')
    )
}

var webpackConfig = {
    entry: {},
    output: {
        filename: "./components/[name]/harmony/build/build.js",
        sourceMapFilename: "./components/[name]/harmony/build/build.js.map"
    },
    devtool: 'source-map',
    debug: true,
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules\/(?!js-csp).+/,
                loaders: [babelLoader]
            }
        ]
    },
    profile: true
}

var webpackSWConfig = {
    entry: {
        'service-worker': './service-worker/service-worker.js'
    },
    output: {
        filename: "./[name].js",
        sourceMapFilename: "./[name].js.map"
    },
    devtool: 'source-map',
    debug: true,
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules\/.+/,
                loaders: [babelLoader + '&optional=runtime']
            }
        ]
    },
    profile: true
}

var computeEntryFiles = function () {
    var entryFiles = glob.sync('./components/**/harmony/src/*.js')
    for (var file of entryFiles) {
        webpackConfig.entry[file.match("[a-z]+(-[a-z]+)+")[0]] = file
    }
}

gulp.task('webpack-sw-build', ['generate-sw-cachelist-file'], function (done) {
    webpack(webpackSWConfig).run(function (err, stats) {
        onBuild(err, stats);
        done();
    });
});

gulp.task('webpack-sw-watch', ['generate-sw-cachelist-file'], function (done) {
    var firedDone = false;
    webpack(webpackSWConfig).watch(100, function (err, stats) {
        if (!firedDone) {
            done();
            firedDone = true;
        }
        onBuild(err, stats);
    });
});

gulp.task('webpack-build', function (done) {
    computeEntryFiles()
    webpack(webpackConfig).run(function (err, stats) {
        onBuild(err, stats);
        done();
    });
});

gulp.task('webpack-watch', function (done) {
    computeEntryFiles()
    var firedDone = false;
    webpack(webpackConfig).watch(100, function (err, stats) {
        if (!firedDone) {
            done();
            firedDone = true;
        }
        onBuild(err, stats);
    });
});

gulp.task('default', ['webpack-watch'])