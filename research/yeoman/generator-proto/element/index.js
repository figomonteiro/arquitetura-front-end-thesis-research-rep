'use strict';
var yeoman = require('yeoman-generator');
var util = require('util');

var ElementGenerator = yeoman.generators.Base.extend({
  promptUser: function() {
    var done = this.async();

    // have Yeoman greet the user
    console.log(this.yeoman);

    var prompts = [{
      name: 'elementName',
      message: 'Custom element\'s name ?',
      required: true,
      type: String
    }];

    this.prompt(prompts, function (props) {
      this.elementName = props.elementName;
      done();
    }.bind(this));
  },
  generateElement: function(){
    var context = { 
        element_name: this.elementName 
    }

    var htmlFile = "components/" + this.elementName + "/" + this.elementName + ".html"
    var jsFile = "components/" + this.elementName + "/harmony/src/" + this.elementName + ".js"
 
    this.template("_element.html", htmlFile, context)
    this.template("_element.js", jsFile, context)
  }
})

module.exports = ElementGenerator