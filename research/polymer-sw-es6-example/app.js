var compass = require('compass'),
express = require('express');

app = express();
app.use(compass({ cwd: __dirname + 'starter' }));
app.use(express.static(__dirname + 'starter'));
app.listen(3000).then(function() { console.log("Server running..."); });