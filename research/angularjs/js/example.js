// initialized app
var app = angular.module('example-app', []);

// share data between controllers
app.factory('Data', function() {
	return {message: "placeholder"};
});

// apply filters to binded html content
app.filter('reverse', function() {
	return function(text) {
		return text.split("").reverse().join("");
	}
});


// defining the controllers business logic
var ExampleCtrl = function($scope, Data) {
	$scope.data = Data;
}

var DuplicatedCtrl = function($scope, Data) {
	$scope.data = Data;

	$scope.outOfScopeMessage = function(source) {
		alert("Out of scope message from " + source);
	}

	$scope.bidirmessage = "bidirectional message";	
}

app.controller('ExampleCtrl', ExampleCtrl);
app.controller('DuplicatedCtrl', DuplicatedCtrl);

// custom html element
app.directive("directiveExample", function() {
	//return {
	//	restrict: "E",
	//	template: "<div>This is an example of an angularjs directive</div>"
	//}
	return function($scope, element, attrs) {
		// assigning content dynamically to the new element
		element.html($scope.data.message + " " + attrs.suffix);
	}
});

// directive custom events
app.directive("mouseover", function() {
	return {
		restrict: "A",
		link: function(scope, element, attrs) {
			element.bind('mouseenter', function() {
				element.addClass(attrs.mouseover);
			})
		}
	}
});

app.directive('mouseleave', function() {
	return {
		restrict: "A",
		link: function(scope, element, attrs) {
			element.bind('mouseleave', function() {
				element.removeClass(attrs.mouseover);
			})
		}
	}
})

// isolated scopes
app.directive('isolated', function() {
	return {
		restrict: "E",
		scope: {},
		template: '<p><input type="text" ng-model="task" /> {{task}}</p>'
	}
})

//callig a controller method from an isolated scope element
app.directive('isolatedCommunication', function() {
	return {
		restrict: "E",
		scope: {
			done: "&"
		},
		template: '<p><input type="text" ng-model="task" /> {{task}}'
			+ '<button ng-click="done({task:task})">Finish</button></p>'
	}
})

app.directive('nonIsolated', function() {
	return {
		restrict: "E",
		template: '<p><input type="text" ng-model="task" /> {{task}}</p>'
	}
})

// binding values between the controller scope and an isolated scope
app.directive('isolatedBidirectional', function() {
	return {
		restrict: "E",
		scope: {
			message: "="
		},
		template: '<p><input type="text" ng-model="message" /></p>'
	}
})

// binding attribute values within an isolated scope
app.directive('isolatedAttrValue', function() {
	return {
		restrict: "E",
		scope: {
			message: "@"
		},
		template: '<div>{{message}}</div>'
	}
})